//
//  AnnotationView.swift
//  Pickitup
//
//  Created by Bala on 18/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class AnnotationView: UIView {
    
    var labelDistance : UILabel!
    var imageviewAnnotationPin : UIImageView!


    
    func CustomizeAnnotation (annot : BMAnnotation){
        imageviewAnnotationPin = self.viewWithTag(1) as! UIImageView
        labelDistance = self.viewWithTag(2) as! UILabel
        labelDistance.text = annot.distance
        imageviewAnnotationPin.image = UIImage(named: annot.imageName)
    }
}
