//
//  AnnotationPin.swift
//  Pickitup
//
//  Created by Bala on 18/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
import MapKit

class AnnotationPin: MKAnnotationView {

     override init(annotation: MKAnnotation!, reuseIdentifier: String!) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        fatalError("init(coder:) has not been implemented")
    }

}
