//
//  BMOrderDropdown.swift
//  Pickitup
//
//  Created by Bala on 28/03/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
protocol bmOrderDropDownDelegate  {
    func selectedValueFromBMDropDowm (index : Int)
}

class BMOrderDropdown: UIView,UITableViewDelegate,UITableViewDataSource {
    var tableViewObj : UITableView!
    var arrayToLoad : NSMutableArray = NSMutableArray()
    var delegate : bmOrderDropDownDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    func showDropDown(dropDownFrame dropDownFrame : CGRect ,heightOfDropDown : CGFloat){
        self.frame = UIScreen.mainScreen().bounds
        self.backgroundColor = UIColor.clearColor()
        tableViewObj = UITableView(frame:  dropDownFrame, style: UITableViewStyle.Plain)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        tableViewObj.backgroundColor = UIColor(red: 38.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        tableViewObj.showsVerticalScrollIndicator = false
        tableViewObj.layer.cornerRadius = 5.0
        self .addSubview(tableViewObj)
        let appde : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appde.window!.addSubview(self)
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), heightOfDropDown)
        }) { (finished) -> Void in
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayToLoad.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cel")
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None;
            cell?.backgroundColor = UIColor(red: 38.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 1.0)
            cell?.contentView.backgroundColor = UIColor(red: 38.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha:1.0)
        }
        let driver : DriverDetails = arrayToLoad[indexPath.row] as! DriverDetails
        cell?.textLabel?.text = driver.driverStoreName
        cell?.textLabel?.font = UIFont.systemFontOfSize(15)
        cell?.textLabel?.textColor = UIColor.whiteColor()
        cell?.textLabel?.textAlignment = NSTextAlignment.Center
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
            
        }) { (finished) -> Void in
            self.delegate.selectedValueFromBMDropDowm(indexPath.row)
            self.removeFromSuperview()
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = (touches as Set).first
        let location = touch!.locationInView(touch!.view)
        if(!CGRectContainsPoint(self.tableViewObj.frame, location)){
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
            }) { (finished) -> Void in
                self.removeFromSuperview()
            }
        }
    }
}
