//
//  BMDropDownView.swift
//  Pickitup
//
//  Created by Bala on 22/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

protocol BMDropdownDelegate{
    func selectedStore(selectedstore : Stores)
}

class BMDropDownView: UIView,UITableViewDelegate,UITableViewDataSource{

    var tableViewObj : UITableView!
    var arrayToLoad : NSMutableArray = NSMutableArray()
    var delegate : BMDropdownDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clearColor()
        tableViewObj = UITableView(frame:  CGRectMake(0, 0, self.bounds.width, 0), style: UITableViewStyle.Plain)
        tableViewObj.delegate = self
        tableViewObj.dataSource = self
        tableViewObj.backgroundColor = UIColor(red: 37.0/255.0, green: 37.0/255.0, blue: 36.0/255.0, alpha: 0.9)
        tableViewObj.showsVerticalScrollIndicator = false
//        tableViewObj.layer.borderColor = UIColor.blackColor().CGColor
//        tableViewObj.layer.borderWidth = 1.0
       // tableViewObj.layer.cornerRadius = 3.0
        self .addSubview(tableViewObj)
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), frame.size.height)
            }) { (finished) -> Void in
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK:- RELOAD PAGE
    func reloadPlaces(arrayPlaces : NSArray){
        self.arrayToLoad.removeAllObjects()
        self.arrayToLoad.addObjectsFromArray(arrayPlaces as [AnyObject])
        self.tableViewObj.reloadData()
    }
    
    func hideMenu(){
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
            }) { (finished) -> Void in
                self.removeFromSuperview()
        }
    }
    //MARK:- TABLEVIEW DELEGATES
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayToLoad.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if(self.respondsToSelector(Selector("setSeparatorInset:"))){
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
        if(cell.respondsToSelector(Selector("setLayoutMargins:"))){
            cell.layoutMargins =  UIEdgeInsetsMake(0, 0, 0, 0)
            ;
        }
        if(cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:"))){
            cell.preservesSuperviewLayoutMargins = false;
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell  = tableView.dequeueReusableCellWithIdentifier("cel")
        if (cell == nil){
            cell  = BMDropDownTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let store : Stores = self.arrayToLoad[indexPath.row] as! Stores
        
        (cell as! BMDropDownTableViewCell).labelShopName.text = store.storeName
        (cell as! BMDropDownTableViewCell).imageViewShop.sd_setImageWithURL(NSURL(string:store_ImagePath + store.storeImageName), placeholderImage: UIImage(named: "dummy5"))
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.tableViewObj.frame = CGRectMake(CGRectGetMinX(self.tableViewObj.frame), CGRectGetMinY(self.tableViewObj.frame), CGRectGetWidth(self.tableViewObj.frame), 0)
            }) { (finished) -> Void in
                let store : Stores = self.arrayToLoad[indexPath.row] as! Stores
                self.delegate.selectedStore(store)
                self.removeFromSuperview()
        }
    }

}
