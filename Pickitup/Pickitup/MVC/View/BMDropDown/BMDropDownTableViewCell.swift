//
//  BMDropDownTableViewCell.swift
//  Pickitup
//
//  Created by Bala on 22/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class BMDropDownTableViewCell: UITableViewCell {

    var labelShopName : UILabel!
    var imageViewShop : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        imageViewShop = UIImageView(frame: CGRectMake(5, 5, 30, 30))
        imageViewShop.layer.cornerRadius = 15.0
        imageViewShop.clipsToBounds = true
        self.contentView.addSubview(imageViewShop)
        
        labelShopName = UILabel(frame: CGRectMake(40, 0, screenSize.width - 90, 40))
        labelShopName.textColor = UIColor.whiteColor()
        labelShopName.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        self.contentView.addSubview(labelShopName)
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
