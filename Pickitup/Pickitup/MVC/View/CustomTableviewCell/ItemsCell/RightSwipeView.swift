//
//  RightSwipeView.swift
//  Pickitup
//
//  Created by Bala on 04/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

protocol RightMenuViewDelegate{
    func onCartItemsModified (Amnt : Float)
}
class RightSwipeView: UIView {
    
    var cellItems : ItemsTableViewCell!
    var cellDiscover : DiscoverTableViewCell!
    var delegate : RightMenuViewDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- PLUS BUTTON ACTION
    @IBAction func onPlusButtonPressed (sender : UIButton){
        if(cellDiscover == nil){
            if(canModifyData(cellItems.storeId)){
                var quantity : Int = cellItems.labelQuantity.text == "" ? 0 : Int((cellItems.labelQuantity.text!.stringByReplacingOccurrencesOfString("x", withString: "") as NSString).floatValue)
                quantity = quantity + 1
                cellItems.labelQuantity.text = NSString(format: "x%d", quantity) as String
                cellItems.viewQuantiy.hidden = quantity == 0
                delegate.onCartItemsModified(Float(cellItems.itemObj.itemRate)!)
                addToCart("\(quantity)", storeId: cellItems.storeId, itemid: cellItems.itemObj.itemId, subitemid: "", subQuantity: "")
                
            }else{
                SVProgressHUD.showErrorWithStatus("You can add items from one store at a time")
            }
        }else{
            if(canModifyData(cellDiscover.cartObj.cartStoreId)){
                var quantity : Int = cellDiscover.labelQuantity.text == "" ? 0 : Int((cellDiscover.labelQuantity.text!.stringByReplacingOccurrencesOfString("x", withString: "") as NSString).floatValue)
                quantity = quantity + 1
                cellDiscover.labelQuantity.text = NSString(format: "x%d", quantity) as String
                cellDiscover.viewQuantiy.hidden = quantity == 0
                if cellDiscover.cartObj.cartItemType == "maindish" {
                    addToCart("\(quantity)", storeId: cellDiscover.cartObj.cartStoreId, itemid:  cellDiscover.cartObj.cartItemId, subitemid: "", subQuantity: "")
                }else{
                    addToCart("", storeId:"", itemid:"", subitemid: cellDiscover.cartObj.cartSubItemId, subQuantity: "\(quantity)")
                }
                delegate.onCartItemsModified(cellDiscover.cartObj.cartItemType == "maindish" ? Float(cellDiscover.cartObj.cartItemPrice)! : Float(cellDiscover.cartObj.cartSubItemPrice)! )
                
            }else{
                SVProgressHUD.showErrorWithStatus("You can add items from one store at a time")
                
            }
        }
    }
    
    //MARK:- CHECKING ITEMS IN CART
    func canModifyData (storeid : String) -> Bool{
        var status : Bool = false
        if(Cart.SharedInstance().arrayCartItems.count > 0){
            let cart : Cart = Cart.SharedInstance().arrayCartItems[0] as! Cart
            let storeId = cellItems == nil ? cellDiscover.cartObj.cartStoreId : cellItems.storeId
            if(cart.cartStoreId == storeId){
                status = true
            }
        }else if (Cart.SharedInstance().arrayCartItems.count == 0){
            status = true
        }
        
        return status
    }
    
    //MARK:- MINUS BUTTON ACTION
    @IBAction func onMinusButtonPressed (sender : UIButton){
        if(cellDiscover == nil){
            if(canModifyData(cellItems.storeId)){
                var quantity : Int = cellItems.labelQuantity.text == "" ? 0 : Int((cellItems.labelQuantity.text!.stringByReplacingOccurrencesOfString("x", withString: "") as NSString).floatValue)
                if(quantity != 0){
                    quantity = quantity - 1
                    cellItems.labelQuantity.text = NSString(format: "x%d", quantity) as String
                    delegate.onCartItemsModified(Float(cellItems.itemObj.itemRate)! * -1)
                    addToCart("\(quantity)", storeId: cellItems.storeId, itemid: cellItems.itemObj.itemId, subitemid: "", subQuantity: "")
                }
                cellItems.viewQuantiy.hidden = quantity == 0
                
            }else{
                SVProgressHUD.showErrorWithStatus("You can add items from one store at a time")
            }
        }else{
            if(canModifyData(cellDiscover.cartObj.cartStoreId)){
                var quantity : Int = cellDiscover.labelQuantity.text == "" ? 0 : Int((cellDiscover.labelQuantity.text!.stringByReplacingOccurrencesOfString("x", withString: "") as NSString).floatValue)
                if(quantity != 0){
                    quantity = quantity - 1
                    cellDiscover.labelQuantity.text = NSString(format: "x%d", quantity) as String
                    delegate.onCartItemsModified(Float(cellDiscover.cartObj.cartItemPrice)! * -1)
                    if cellDiscover.cartObj.cartItemType == "maindish" {
                        addToCart("\(quantity)", storeId: cellDiscover.cartObj.cartStoreId, itemid:  cellDiscover.cartObj.cartItemId, subitemid: "", subQuantity: "")
                    }else{
                        addToCart("", storeId:"", itemid:"", subitemid: cellDiscover.cartObj.cartSubItemId, subQuantity: "\(quantity)")
                    }
                }
                cellDiscover.viewQuantiy.hidden = quantity == 0
            }else{
                SVProgressHUD.showErrorWithStatus("You can add items from one store at a time")
            }
        }
    }
    
    //MARK:- TRASH BUTTON ACTION
    @IBAction func onTrashButtonPressed (sender : UIButton){
        if(cellDiscover == nil){
            let quantity : Int = cellItems.labelQuantity.text == "" ? 0 : Int((cellItems.labelQuantity.text!.stringByReplacingOccurrencesOfString("x", withString: "") as NSString).floatValue)
            if(quantity == 0) {return}
            delegate.onCartItemsModified(Float(cellItems.itemObj.itemRate)! * Float(quantity) * -1)
            addToCart("0", storeId: cellItems.storeId, itemid: cellItems.itemObj.itemId, subitemid: "", subQuantity: "")
            cellItems.labelQuantity.text = "x0"
            cellItems.viewQuantiy.hidden = true
        }else{
            let quantity : Int = cellDiscover.labelQuantity.text == "" ? 0 : Int((cellDiscover.labelQuantity.text!.stringByReplacingOccurrencesOfString("x", withString: "") as NSString).floatValue)
            if(quantity == 0) {return}
            delegate.onCartItemsModified(Float(cellDiscover.cartObj.cartItemPrice)! * Float(quantity) * -1)
            if cellDiscover.cartObj.cartItemType == "maindish" {
                addToCart("0", storeId: cellDiscover.cartObj.cartStoreId, itemid:  cellDiscover.cartObj.cartItemId, subitemid: "", subQuantity: "")
            }else{
                addToCart("", storeId:"", itemid:"", subitemid: cellDiscover.cartObj.cartSubItemId, subQuantity: "0")
            }
            cellDiscover.labelQuantity.text = "x0"
            cellDiscover.viewQuantiy.hidden = true
        }
    }
    
    
    //MARK:- ADD TO CART WITH JSON
    func addToCart (quantity : String ,storeId : String , itemid : String , subitemid : String , subQuantity : String){
        SVProgressHUD.showWithStatus("Processing...")
        Cart.addToCart(storeId, itemId: itemid, quantity: quantity, subItemId: subitemid, subItemQuantity: subQuantity) { (success, result, error) in
            if(success){
                SVProgressHUD.dismiss()
            }else{
                if(error?.localizedDescription == "Please try again"){
                    Cart.SharedInstance().totalPrice = "0"
                    Cart.SharedInstance().shippingCost = "0"
                    Cart.SharedInstance().arrayCartItems.removeAllObjects()
                    SVProgressHUD.dismiss()
                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            }
        }
    }
}
