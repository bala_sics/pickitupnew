//
//  ItemsTableViewCell.swift
//  Pickitup
//
//  Created by Bala on 04/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

protocol itemCelldelegate {
    func selectedRowatIndex(index : Int , itemObj : Items)
    func updateCartAmount (amount : Float)
}
class ItemsTableViewCell: UITableViewCell {

    var labelItemName : UILabel!
    var labelPrice : UILabel!
    var labelDescription : UILabel!
    var labelQuantity : UILabel!
    var viewQuantiy : UIView!
    var delegate : itemCelldelegate!
    var itemObj : Items!
    var storeId : String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        let scrolll : UIScrollView = UIScrollView(frame: CGRectMake(0, 0, screenSize.width, 70))
        scrolll.backgroundColor = UIColor.whiteColor()
        scrolll.showsHorizontalScrollIndicator = false
        scrolll.showsVerticalScrollIndicator = false
        scrolll.bounces = false
        self.contentView.addSubview(scrolll)
        
        labelItemName = UILabel(frame: CGRectMake(15, 0, screenSize.width - 125, 30))
        labelItemName.textColor = UIColor.blackColor()
        labelItemName.font = UIFont(name: "Myriad Hebrew", size: 13.0)
//        labelItemName.text = "Club Pita Fallafel & Eau"
        scrolll.addSubview(labelItemName)
        
        labelPrice = UILabel(frame: CGRectMake(screenSize.width - 110, 0, 50, 30))
        labelPrice.textColor = UIColor.blackColor()
        labelPrice.font = UIFont(name: "Myriad Hebrew", size: 13.0)
//        labelPrice.text = "10.00$"
        labelPrice.textAlignment = NSTextAlignment.Right
        scrolll.addSubview(labelPrice)
        
        labelDescription = UILabel(frame: CGRectMake(15, 20, screenSize.width - 75, 50))
        labelDescription.textColor = UIColor.lightGrayColor()
        labelDescription.font = UIFont(name: "Myriad Hebrew", size: 12.0)
        labelDescription.lineBreakMode = NSLineBreakMode.ByWordWrapping
        labelDescription.numberOfLines = 3
//        labelDescription.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        scrolll.addSubview(labelDescription)
        
        viewQuantiy  = UIView(frame: CGRectMake(screenSize.width - 60, 0, 60, 70))
        viewQuantiy.backgroundColor = UIColor.whiteColor()
        scrolll.addSubview(viewQuantiy)
        
        let viewBorder: UIView = UIView(frame: CGRectMake(55, 0, 5, 70))
        viewBorder.backgroundColor = UIColor(red: 11.0/255.0, green: 186.0/255.0, blue: 191.0/255.0, alpha: 1.0)
        viewQuantiy.addSubview(viewBorder)
        
        labelQuantity = UILabel(frame: CGRectMake(0, 0, 50, 70))
        labelQuantity.textColor = UIColor(red: 11.0/255.0, green: 186.0/255.0, blue: 191.0/255.0, alpha: 1.0)
        labelQuantity.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        labelQuantity.text = ""
        labelQuantity.textAlignment = NSTextAlignment.Right
        viewQuantiy.addSubview(labelQuantity)

        viewQuantiy.hidden = true
        
        let menuView : RightSwipeView = RightSwipeView.initWithNib("RightSwipeView", bundle: nil)
        menuView.frame = CGRectMake(screenSize.width, 0, 100, 70)
        menuView.cellItems = self
        menuView.delegate = self
        scrolll.addSubview(menuView)
        scrolll.contentSize = CGSizeMake(screenSize.width + 100, 0)
        
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ItemsTableViewCell.onTapGestureTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        scrolll.addGestureRecognizer(tapGesture)
        
    }
    
    func onTapGestureTapped (gesture : UITapGestureRecognizer){
        delegate.selectedRowatIndex(gesture.view!.tag, itemObj: self.itemObj)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ItemsTableViewCell : RightMenuViewDelegate{
    
    func onCartItemsModified(Amnt: Float) {
        self.delegate.updateCartAmount(Amnt)
    }
}
