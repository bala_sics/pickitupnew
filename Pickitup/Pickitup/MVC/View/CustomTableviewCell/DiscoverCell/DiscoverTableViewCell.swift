//
//  DiscoverTableViewCell.swift
//  Pickitup
//
//  Created by Bala on 05/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

protocol discoverCelldelegate {
    func updateCartAmount (amount : Float)
}

class DiscoverTableViewCell: UITableViewCell {

    var labelItemName : UILabel!
    var imageViewItem : UIImageView!
    var labelItemPrice : UILabel!
    var labelItemType : UILabel!
    var labelDot : UILabel!
    var viewQuantiy : UIView!
    var labelQuantity : UILabel!
    var delegate : discoverCelldelegate!
    var cartObj : Cart!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        
        labelItemType = UILabel(frame: CGRectMake(0, 0, screenSize.width , 35))
        labelItemType.textColor = UIColor.lightGrayColor()
        labelItemType.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        labelItemType.layer.borderColor = UIColor.lightGrayColor().CGColor
        labelItemType.layer.borderWidth = 1.5
        self.contentView.addSubview(labelItemType)
        
        imageViewItem = UIImageView(frame: CGRectMake(0, 34, screenSize.width, 200))
         self.contentView.addSubview(imageViewItem)
        
        let scrolll : UIScrollView = UIScrollView(frame: CGRectMake(0, 234, screenSize.width, 70))
        scrolll.backgroundColor = UIColor.whiteColor()
        scrolll.showsHorizontalScrollIndicator = false
        scrolll.showsVerticalScrollIndicator = false
        scrolll.bounces = false
        self.contentView.addSubview(scrolll)


        labelItemName = UILabel(frame: CGRectMake(10, 0, screenSize.width, 35))
        labelItemName.textColor = UIColor.blackColor()
        labelItemName.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        scrolll.addSubview(labelItemName)

        
        labelItemPrice = UILabel(frame: CGRectMake(10, CGRectGetMaxY(labelItemName.frame) - 10, 60, 30))
        labelItemPrice.textColor = UIColor.blackColor()
        labelItemPrice.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        scrolll.addSubview(labelItemPrice)
        
        labelDot = UILabel(frame: CGRectMake(70, CGRectGetMaxY(labelItemName.frame) - 17, 10, 30))
        labelDot.textColor = UIColor.darkGrayColor()
        labelDot.font = UIFont.boldSystemFontOfSize(30)
        labelDot.text = "."
        scrolll.addSubview(labelDot)
        
        viewQuantiy  = UIView(frame: CGRectMake(screenSize.width - 60, -2, 60, 74))
        viewQuantiy.backgroundColor = UIColor.whiteColor()
        scrolll.addSubview(viewQuantiy)
        
        let viewBorder: UIView = UIView(frame: CGRectMake(55, 0, 5, 72))
        viewBorder.backgroundColor = UIColor(red: 11.0/255.0, green: 186.0/255.0, blue: 191.0/255.0, alpha: 1.0)
        viewQuantiy.addSubview(viewBorder)
        
        labelQuantity = UILabel(frame: CGRectMake(0, 0, 50, 72))
        labelQuantity.textColor = UIColor(red: 11.0/255.0, green: 186.0/255.0, blue: 191.0/255.0, alpha: 1.0)
        labelQuantity.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        labelQuantity.text = "x1"
        labelQuantity.textAlignment = NSTextAlignment.Right
        viewQuantiy.addSubview(labelQuantity)
        
        let menuView : RightSwipeView = RightSwipeView.initWithNib("RightSwipeView", bundle: nil)
        menuView.frame = CGRectMake(screenSize.width, 0, 100, 70)
        menuView.cellDiscover = self
        menuView.delegate = self
        scrolll.addSubview(menuView)
        scrolll.contentSize = CGSizeMake(screenSize.width + 100, 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension DiscoverTableViewCell : RightMenuViewDelegate{
    
    func onCartItemsModified(Amnt: Float) {
        self.delegate.updateCartAmount(Amnt)
    }
}
