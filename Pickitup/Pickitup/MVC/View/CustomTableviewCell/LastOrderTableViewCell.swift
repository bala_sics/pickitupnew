//
//  LastOrderTableViewCell.swift
//  Pickitup
//
//  Created by Bala Murugann on 4/13/16.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class LastOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTip : UILabel!
    @IBOutlet weak var labelRace : UILabel!
    @IBOutlet weak var labelTime : UILabel!
    @IBOutlet weak var labelDistance1 : UILabel!
    @IBOutlet weak var labelDistance2 : UILabel!
    @IBOutlet weak var imageviewStore : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
   
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func configureCell(dict : NSDictionary) {
//        print(dict)
        imageviewStore.layer.cornerRadius = imageviewStore.frame.height / 2
        imageviewStore.clipsToBounds = true
        labelTip.text = "Tip:\(dict["tip"]!)$"
        labelTime.text = "Time:\(dict["duration"]!)m"
        labelDistance1.text = "\(dict["distance_from_driver_store"]!) km"
        labelDistance2.text = "\(dict["distance_from_store_customer"]!) km"
        let storeImage : String = dict["store_image"] as! String
        imageviewStore.sd_setImageWithURL(NSURL(string:store_ImagePath + storeImage), placeholderImage: UIImage(named: "dummy3"))


    }
}
