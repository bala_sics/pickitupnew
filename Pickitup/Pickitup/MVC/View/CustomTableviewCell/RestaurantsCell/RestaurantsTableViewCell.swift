//
//  RestaurantsTableViewCell.swift
//  Pickitup
//
//  Created by Bala on 01/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class RestaurantsTableViewCell: UITableViewCell {

    var imageViewItemType : UIImageView!
    var labelItemName : UILabel!
    var buttonWant : UIButton!
    var imageViewItem : UIImageView!
    var labelItemType : UILabel!
    var labelItemPrice : UILabel!
    var labelDistance : UILabel!
    var labelDot : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        let backgroundView : UIView = UIView(frame: CGRectMake(0, 0, screenSize.width, 150))
        backgroundView.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(backgroundView)
        
        imageViewItemType = UIImageView(frame: CGRectMake(10, 10, 40, 40))
        imageViewItemType.layer.cornerRadius = 20.0
        imageViewItemType.clipsToBounds = true
        backgroundView.addSubview(imageViewItemType)
        
        labelItemType = UILabel(frame: CGRectMake(55, 10, screenSize.width - 150, 40))
        labelItemType.textColor = UIColor.blackColor()
        labelItemType.font = UIFont(name: "Myriad Hebrew", size: 16.0)
        backgroundView.addSubview(labelItemType)
        
        buttonWant = UIButton(type: UIButtonType.Custom)
        buttonWant.frame = CGRectMake(screenSize.width - 90, 15, 80, 30)
        buttonWant.layer.cornerRadius = 5.0
        buttonWant.backgroundColor = UIColor(red: 37.0/255.0, green: 191.0/255.0, blue: 197.0/255.0, alpha: 1.0)
        buttonWant.setTitle("Pick it !", forState: UIControlState.Normal)
        buttonWant.titleLabel?.font = UIFont(name: "Myriad Hebrew", size: 17.0)
        backgroundView.addSubview(buttonWant)
        buttonWant.userInteractionEnabled = false
        
        imageViewItem = UIImageView(frame: CGRectMake(0, 60, screenSize.width, 200))
        backgroundView.addSubview(imageViewItem)
        
//        labelItemName = UILabel(frame: CGRectMake(10, 265, screenSize.width - 20, 30))
//        labelItemName.textColor = UIColor.blackColor()
//        labelItemName.font = UIFont(name: "Myriad Hebrew", size: 15.0)
//        backgroundView.addSubview(labelItemName)
        
//        labelItemPrice = UILabel(frame: CGRectMake(10, 295, 40, 30))
//        labelItemPrice.textColor = UIColor.blackColor()
//        labelItemPrice.font = UIFont(name: "Myriad Hebrew", size: 15.0)
//        backgroundView.addSubview(labelItemPrice)
        
        labelDot =  UILabel(frame: CGRectMake(10, 257, 10, 30))//UILabel(frame: CGRectMake(10, 287, 10, 30))
        labelDot.textColor = UIColor.darkGrayColor()
        labelDot.font = UIFont.boldSystemFontOfSize(30)
        labelDot.text = "."
        backgroundView.addSubview(labelDot)
        
        labelDistance =  UILabel(frame: CGRectMake(20, 265, screenSize.width - 100, 30)) // UILabel(frame: CGRectMake(20, 295, screenSize.width - 100, 30))
        labelDistance.textColor = UIColor.lightGrayColor()
        labelDistance.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        backgroundView.addSubview(labelDistance)
        
        let separatorView : UIView = UIView(frame: CGRectMake(0,300, screenSize.width , 10))
        separatorView.backgroundColor = UIColor(red: 241.0/255.0, green: 239.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.contentView.addSubview(separatorView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func configureCell (dict : Stores){
        
        imageViewItemType.sd_setImageWithURL(NSURL(string:store_ImagePath + dict.storeLogo), placeholderImage: UIImage(named: "dummy5"))
        imageViewItem.sd_setImageWithURL(NSURL(string:store_ImagePath + dict.storeImageName), placeholderImage: UIImage(named: "dummy5"))
        labelItemType.text = dict.storeName
//        labelItemName.text = dict.storeFirstName
//        labelItemPrice.text = dict["Price"] as? String
        labelDistance.text = "\(dict.storeDistance) km away"
        
//        let attrStr : NSMutableAttributedString = NSMutableAttributedString(string: dict["Distance"] as! String)
//        attrStr.addAttributes([NSForegroundColorAttributeName : UIColor.darkGrayColor() , NSFontAttributeName : UIFont.boldSystemFontOfSize(20)], range: NSMakeRange(0, 1))
//        labelDistance.attributedText = attrStr
        
    }
}
