//
//  PersonalizedTableViewCell.swift
//  Pickitup
//
//  Created by Bala on 11/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class PersonalizedTableViewCell: UITableViewCell {

    var viewBackground : UIView!
    var labelItemName : UILabel!
    var imageViewBackground : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.whiteColor()
        self.contentView.backgroundColor = UIColor.whiteColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        viewBackground  = UIView(frame: CGRectMake(0, 0, screenSize.width, 44))
        viewBackground.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(viewBackground)
        
        imageViewBackground = UIImageView(frame: CGRectMake(0, -0.7, screenSize.width, 45))
        viewBackground.addSubview(imageViewBackground)

        labelItemName = UILabel(frame: CGRectMake(20, 0, screenSize.width - 40, 44))
        labelItemName.textColor = UIColor.blackColor()
        labelItemName.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        viewBackground.addSubview(labelItemName)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
