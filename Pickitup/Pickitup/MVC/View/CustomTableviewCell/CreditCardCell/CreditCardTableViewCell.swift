//
//  CreditCardTableViewCell.swift
//  Pickitup
//
//  Created by Bala on 08/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class CreditCardTableViewCell: UITableViewCell {

    var buttonDelete : UIButton!
    var labelCard : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        

        /////BUTTON DELETE
        buttonDelete = UIButton(type: UIButtonType.Custom)
        buttonDelete.frame = CGRectMake(20, 7, 26, 26)
        buttonDelete.layer.cornerRadius = 13.0
        buttonDelete.backgroundColor = UIColor.darkGrayColor()
        self.contentView.addSubview(buttonDelete)
        
        ///LABEL CARD
        labelCard = UILabel(frame: CGRectMake(60, 5, screenSize.width - 90, 30))
        labelCard.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        labelCard.textColor = UIColor.darkGrayColor()
        labelCard.backgroundColor = UIColor.whiteColor()
        labelCard.text = "  Samo*** y*** **********231"
        self.contentView.addSubview(labelCard)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
