//
//  AnnotationPopUpView.swift
//  Pickitup
//
//  Created by Bala on 20/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

protocol annotationPopUpDelegate {
    func goButtonPressed(order : Orders)
}

class AnnotationPopUpView: UIView {

    @IBOutlet var labelReadyTime : UILabel!
    @IBOutlet var labelDeliveryTime : UILabel!
    @IBOutlet var buttonGo : UIButton!
    @IBOutlet var imageviewProfile : UIImageView!
    var delegate : annotationPopUpDelegate!
    @IBOutlet var labelStoreName : UILabel!
    @IBOutlet var labelItems : UILabel!
    var orderObj : Orders!

    func customizeView (order : Orders){
        orderObj = order
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
        self.backgroundColor = UIColor(red: 48.0/255.0, green: 47.0/255.0, blue: 46.0/255.0, alpha: 0.9)
        labelReadyTime.layer.cornerRadius = 5.0
        labelReadyTime.clipsToBounds = true
        labelDeliveryTime.layer.cornerRadius = 5.0
        labelDeliveryTime.clipsToBounds = true
        buttonGo.layer.cornerRadius = 5.0
        imageviewProfile.layer.borderWidth = 2.0
        imageviewProfile.layer.borderColor = UIColor.whiteColor().CGColor
        imageviewProfile.layer.cornerRadius = 20.0
        imageviewProfile.clipsToBounds = true
        
        imageviewProfile.sd_setImageWithURL(NSURL(string: store_ImagePath + order.orderStoreLogoImage))
        labelStoreName.text = order.orderStoreName.uppercaseString
        labelItems.text = "\(order.orderTotalItems) items"
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AnnotationPopUpView.stopBackgroundTask), name: "KAnnotationBackgroundTask", object: nil)
        calculateTime()

    }
    
    func stopBackgroundTask(){
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "KAnnotationBackgroundTask", object: nil)
        NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(AnnotationPopUpView.calculateTime), object: nil)
    }
    func calculateTime (){
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        let date : NSDate = dateFormatter.dateFromString("\(self.orderObj.orderDeliveredDate) \(self.orderObj.orderDeliveryTime)")!
        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
       let diff = NSCalendar.currentCalendar().components(.Minute, fromDate: NSDate(), toDate: date, options: []).minute
        labelReadyTime.text = diff < 1 ? "0" : "\(diff)"
        
        let date1 : NSDate = dateFormatter.dateFromString("\(self.orderObj.orderedDate) \(self.orderObj.orderedTime)")!
        let diff1 = NSCalendar.currentCalendar().components(.Minute, fromDate: NSDate(), toDate: date1, options: []).minute
        labelDeliveryTime.text = diff1 < 1 ? "0" : "\(diff1)"
        
        self.performSelector(#selector(AnnotationPopUpView.calculateTime), withObject: nil, afterDelay: 5.0)
    }
    
    @IBAction func onButtonGoPressed (sender : UIButton){
        delegate.goButtonPressed(orderObj)
    }
    
}
