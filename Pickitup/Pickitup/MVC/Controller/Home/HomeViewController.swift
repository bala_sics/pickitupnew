//
//  HomeViewController.swift
//  Pickitup
//
//  Created by Bala on 30/12/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class HomeViewController: UIViewController {
    
    var locationManager : CLLocationManager!
    @IBOutlet var mapViewObj : MKMapView!
    @IBOutlet weak var buttonRestaurants : UIButton!
    @IBOutlet weak var buttonStores : UIButton!
    @IBOutlet weak var textfieldSearch : UITextField!
    @IBOutlet weak var viewSearchContainer : UIView!
    
    var arrayCoordinates : [CLLocationCoordinate2D]!
    var isFirstTime : Bool!
    var tapGestureSearch : BMTapGestureRecognizer!
    var arraySearchStores : NSMutableArray = NSMutableArray()
    var arrayNearByStores : NSMutableArray = NSMutableArray()
    var dropdown : BMDropDownView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ////UPDATE USER LOCATION
        locationManager = locationManager == nil ? CLLocationManager() : locationManager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        mapViewObj.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
        
        isFirstTime = true
        
        self.view.sendSubviewToBack(mapViewObj)
        
        let sidemenu : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "SideMenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(HomeViewController.onSideMenuButtonPressed))
        self.navigationItem.leftBarButtonItem = sidemenu
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = false
    }
    
    func onSideMenuButtonPressed (){
         textfieldSearch.text = ""
        textfieldSearch.resignFirstResponder()
        if(dropdown != nil){
            dropdown?.hideMenu()
            dropdown = nil
        }
        if(tapGestureSearch != nil) { self.navigationController?.view.removeGestureRecognizer(tapGestureSearch)}
        self.sideMenuController()?.sideMenu?.toggleMenu()
    }
    
    
    func onTapGestureSearchTapped(){
        textfieldSearch.text = ""
        textfieldSearch.resignFirstResponder()
        if(dropdown != nil){
            dropdown?.hideMenu()
            dropdown = nil
        }
        if(tapGestureSearch != nil) { self.navigationController?.view.removeGestureRecognizer(tapGestureSearch)}
    }
    
    //MARK:- GPS BUTTON ACTION
    @IBAction func onGpsbuttonPressed (sender : UIButton){
        mapViewObj.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);

    }
    
    //MARK:- BUTTON ACTIONS
    //MARK:- RESTAURANT BUTTON ACTION
    @IBAction func onRestaurantsButtonPressed (sender : UIButton){
        let restaurant : RestaurantsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantsVC") as! RestaurantsViewController
        self.navigationController?.pushViewController(restaurant, animated: true)
    }
    
    //MARK:- STORE SERVICE BUTTON ACTION
    @IBAction func onStoreServicesButtonPressed (sender : UIButton){
        let store : StoreServiceViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreServiceVC") as! StoreServiceViewController
        self.navigationController?.pushViewController(store, animated: true)
    }
    
    //MARK:- FETCH ITEMS IN CART
    func fetchCartItems (){
        SVProgressHUD.showWithStatus("Processing...")
        Cart.viewCart { (success, result, error) -> Void in
            if(error != nil) {
                self.fetchCartItems()
            }else{
                SVProgressHUD.dismiss()
            }
        }
    }
    
    //MARK:- FETCH NEARBY STORES
    func fetchNearByStores (lat : String , lon : String){
        SVProgressHUD.showWithStatus("Processing...")
        Stores.fetchNearbyStores(lat, long: lon) { (success, result, error) -> Void in
            if(success){
                self.arrayNearByStores.removeAllObjects()
                self.arrayNearByStores.addObjectsFromArray(result as! [AnyObject])
                self.arrayNearByStores.enumerateObjectsUsingBlock({ (obj, idx, stop) -> Void in
                    let store : Stores = obj as! Stores
                    let info1 = BMAnnotation()
                    info1.coordinate = CLLocationCoordinate2DMake(Double(store.storeLatitude)!, Double(store.storeLongitude)!)
                    info1.title = store.storeName
                    info1.imageName = store.type
                    self.mapViewObj.addAnnotation(info1)
                })
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
           // self.fetchCartItems()
        }
    }
    
    func searchStores(text : String){
        Stores.searchStore(text) { (success, result, error) -> Void in
            if(success){
                self.arraySearchStores.removeAllObjects()
                self.arraySearchStores.addObjectsFromArray(result as! [AnyObject])
                if(self.dropdown == nil){
                    self.dropdown = BMDropDownView(frame: CGRectMake(20, 110, screenSize.width - 40, self.arraySearchStores.count > 4 ? 160 : CGFloat(self.arraySearchStores.count * 40)))
                    self.dropdown?.delegate = self
                    self.dropdown!.arrayToLoad = self.arraySearchStores.mutableCopy() as! NSMutableArray
                    AppDelegate.getAppdelegateInstance().window!.addSubview(self.dropdown!)
                }else{
                    self.dropdown?.reloadPlaces(self.arraySearchStores)
                }
            }else{
                
            }
        }
    }
}

//MARK:- LOCATION MANAGER DELEGATES
extension HomeViewController : CLLocationManagerDelegate{
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc : CLLocation = (locations as NSArray).firstObject as! CLLocation
        locationManager.stopUpdatingLocation()
        if(isFirstTime == true){
            fetchNearByStores("\(loc.coordinate.latitude)", lon: "\(loc.coordinate.longitude)")
            isFirstTime = false
        }
        print("loc \(loc)")
    }
}

//MARK:- TEXTFIELD DELEGATE
extension HomeViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        tapGestureSearch = BMTapGestureRecognizer(target: self, action: #selector(HomeViewController.onTapGestureSearchTapped))
        self.navigationController?.view.addGestureRecognizer(tapGestureSearch)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(tapGestureSearch != nil) { self.navigationController?.view.removeGestureRecognizer(tapGestureSearch)}
        textfieldSearch.text = ""
        textField.resignFirstResponder()
        if(dropdown != nil){
            dropdown?.hideMenu()
            dropdown = nil
        }

        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        searchStores(textField.text! + string)
        return true
    }
    
}

extension HomeViewController : MKMapViewDelegate{
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is BMAnnotation) {
            return nil
        }
        
        let reuseId = "Annotation"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = true
        }
        else {
            anView!.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let cpa = annotation as! BMAnnotation
        anView!.image = UIImage(named:cpa.imageName)
        
        return anView
    }
}


extension HomeViewController : BMDropdownDelegate{
    func selectedStore(selectedstore: Stores) {
        textfieldSearch.text = selectedstore.storeName
        dropdown = nil
        textfieldSearch.resignFirstResponder()
        if(tapGestureSearch != nil) { self.navigationController?.view.removeGestureRecognizer(tapGestureSearch)}
        
        let predicate : NSPredicate = NSPredicate(format: "storeId == %@",selectedstore.storeId)
        let array : NSArray = arrayNearByStores.filteredArrayUsingPredicate(predicate)
        var latitude:CLLocationDegrees!
        var longitude:CLLocationDegrees!
        if(array.count > 0){
            let store : Stores = array[0] as! Stores
            latitude = Double(store.storeLatitude)
            longitude = Double(store.storeLongitude)
        }else{
            arrayNearByStores.addObject(selectedstore)
            let info1 = BMAnnotation()
            info1.coordinate = CLLocationCoordinate2DMake(Double(selectedstore.storeLatitude)!, Double(selectedstore.storeLongitude)!)
            info1.title = selectedstore.storeName
            info1.imageName = selectedstore.type
            self.mapViewObj.addAnnotation(info1)
            latitude = Double(selectedstore.storeLatitude)
            longitude = Double(selectedstore.storeLongitude)
        }
        let latDelta:CLLocationDegrees = 0.05
        let lonDelta:CLLocationDegrees = 0.05
        let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        mapViewObj.setRegion(region, animated: true)
    }
}