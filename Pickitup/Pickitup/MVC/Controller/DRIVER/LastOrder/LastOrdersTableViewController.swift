//
//  LastOrdersTableViewController.swift
//  Pickitup
//
//  Created by Bala on 20/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class LastOrdersTableViewController: UITableViewController {

    var dictDetails : NSDictionary = NSDictionary()
    var arraySorted : NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Driver.currentPerson().userId == nil ?  fetchLastOrders() : fetchDriverLastOrder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchDriverLastOrder (){
        SVProgressHUD.showWithStatus("Processing...")
        Orders.driverLastOrders { (success, result, error) in
            if(success){
                self.dictDetails = result as! NSDictionary
                let array : NSArray = self.dictDetails.allKeys
                let sortDescriptor : NSSortDescriptor = NSSortDescriptor(key: nil, ascending: false, selector: #selector(NSString.localizedCompare(_:)))
                self.arraySorted = array.sortedArrayUsingDescriptors([sortDescriptor])
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    func fetchLastOrders (){
        SVProgressHUD.showWithStatus("Processing...")
        Orders.myLastOrders { (success, result, error) in
            if(success){
                self.dictDetails = result as! NSDictionary
                let array : NSArray = self.dictDetails.allKeys
                let sortDescriptor : NSSortDescriptor = NSSortDescriptor(key: nil, ascending: false, selector: #selector(NSString.localizedCompare(_:)))
                self.arraySorted = array.sortedArrayUsingDescriptors([sortDescriptor])
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return dictDetails.allKeys.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dictDetails[self.arraySorted[section] as! String] as! NSArray).count
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader : UIView = UIView(frame: CGRectMake(0, 0, screenSize.width, 30))
        viewHeader.backgroundColor = UIColor.whiteColor()
        let labelDate :UILabel = UILabel(frame: CGRectMake(0, 0, screenSize.width, 30))
        labelDate.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        let dateStr = self.arraySorted[section] as! String
        let dateformatter : NSDateFormatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let date = dateformatter.dateFromString(dateStr)
        dateformatter.dateFormat = "EEEE dd MMMM"
        labelDate.text = "     \(dateformatter.stringFromDate(date!))"
        labelDate.textColor = UIColor.blackColor()
        viewHeader.addSubview(labelDate)
        return viewHeader
    }
   
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = LastOrderTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let dict : NSDictionary = (dictDetails[self.arraySorted[indexPath.section] as! String] as! NSArray).objectAtIndex(indexPath.row) as! NSDictionary
         (cell as! LastOrderTableViewCell).configureCell(dict)
        return cell!
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let dict : NSDictionary = (dictDetails[self.arraySorted[indexPath.section] as! String] as! NSArray).objectAtIndex(indexPath.row) as! NSDictionary
        let dateStr = self.arraySorted[indexPath.section] as! String
        let dateformatter : NSDateFormatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let date = dateformatter.dateFromString(dateStr)
        dateformatter.dateStyle = .FullStyle
        let bill : BillTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("BillVC") as! BillTableViewController
        bill.orderId = dict["id"] as! String
        bill.orderDate = dateformatter.stringFromDate(date!)
        self.navigationController?.pushViewController(bill, animated: true)
    }
   
}
