//
//  DriverHomeViewController.swift
//  Pickitup
//
//  Created by Bala on 11/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class DriverHomeViewController: UIViewController {
    @IBOutlet var mapViewObj : MKMapView!
    var locationManager : CLLocationManager!
    var isFirstTime : Bool!
    var arrayOrders : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ////UPDATE USER LOCATION
        locationManager = locationManager == nil ? CLLocationManager() : locationManager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        isFirstTime = true
        
        mapViewObj.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
        
        let sidemenu : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "SideMenuIcon"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(DriverHomeViewController.onSideMenuButtonPressed))
        self.navigationItem.leftBarButtonItem = sidemenu
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func onSideMenuButtonPressed (){
        self.sideMenuController()?.sideMenu?.toggleMenu()
    }
    
    
    //MARK:- GPS BUTTON ACTION
    @IBAction func onGpsbuttonPressed (sender : UIButton){
        mapViewObj.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
        
    }
    
    func fetchorders (lat : String , lng : String){
        SVProgressHUD.showWithStatus("Processing...")
        Orders.viewOrders(lat, lng: lng) { (success, result, error) -> Void in
            if(success){
                self.arrayOrders.removeAllObjects()
                self.arrayOrders.addObjectsFromArray(result as! [AnyObject])
                self.arrayOrders.enumerateObjectsUsingBlock({ (obj, idx, stop) -> Void in
                    let order : Orders = obj as! Orders
                    let info1 = BMAnnotation()
                    info1.coordinate = CLLocationCoordinate2DMake(Double(order.orderStoreLatitude)!, Double(order.orderStoreLongitude)!)
                    info1.distance = "\(order.orderDistance) km"
                    info1.imageName = "Red"
                    info1.tag = idx
                    info1.orderDetails = order
                    self.mapViewObj.addAnnotation(info1)
                })
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }

        }
    }
}

//MARK:- MAPVIEW DELEGATES
extension DriverHomeViewController : MKMapViewDelegate{
    
    ///MARK:- VIEW FOR ANNOTATION
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            var messagePinView = mapView.dequeueReusableAnnotationViewWithIdentifier("UserLocation")
            if ( messagePinView == nil ){
                messagePinView = MKAnnotationView(annotation: annotation, reuseIdentifier: "UserLocation")
                messagePinView?.canShowCallout = false
                messagePinView?.image = UIImage(named: "DriverCurrentLocation")
            }
            return messagePinView
        }
        
        var messagePinView = mapView.dequeueReusableAnnotationViewWithIdentifier("\((annotation as! BMAnnotation).tag)")
        if(messagePinView == nil){
            messagePinView = AnnotationPin(annotation: annotation, reuseIdentifier: "\((annotation as! BMAnnotation).tag)") as AnnotationPin
            messagePinView!.canShowCallout = false
            let arrayMessageView = (NSBundle.mainBundle().loadNibNamed("AnnotationView", owner: self, options: nil) as NSArray)
            let myAnnotation : AnnotationView = arrayMessageView[0] as! AnnotationView
            messagePinView?.frame = CGRectMake(0, 0, 55, 45)
            myAnnotation.CustomizeAnnotation(annotation as! BMAnnotation)
            messagePinView!.layer.anchorPoint = CGPointMake (0.5, 1.0)
            messagePinView!.addSubview(myAnnotation)
        }
        return messagePinView
        
    }
    
    
    //MARK:- DID SELECT ANNOTATION
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        if(view.annotation is MKUserLocation == false){
            let arrayView = (NSBundle.mainBundle().loadNibNamed("AnnotationPopUpView", owner: self, options: nil) as NSArray)
            let popUp : AnnotationPopUpView = arrayView[0] as! AnnotationPopUpView
            popUp.delegate = self
            let anno : BMAnnotation = view.annotation as! BMAnnotation
            popUp.customizeView(anno.orderDetails)
            KGModal.sharedInstance().showWithContentView(popUp, andAnimated: true)
            mapView.deselectAnnotation(view.annotation, animated: true)
        }
    }
}

//MARK:- LOCATION MANAGER DELEGATES
extension DriverHomeViewController : CLLocationManagerDelegate{
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let loc : CLLocation = (locations as NSArray).firstObject as! CLLocation
        locationManager.stopUpdatingLocation()
        if(isFirstTime == true){
            self.fetchorders("\(loc.coordinate.latitude)", lng: "\(loc.coordinate.longitude)")
            isFirstTime = false
        }
    }
}

//MARK:- ANNOTATION POP UP DELEGATE
extension  DriverHomeViewController : annotationPopUpDelegate{
    func goButtonPressed(order: Orders) {
        KGModal.sharedInstance().hideAnimated(true)
        let annotDetail : AnnotationDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AnnotationDetailVC") as! AnnotationDetailViewController
        annotDetail.orderDetails = order
        self.navigationController?.pushViewController(annotDetail, animated: true)
    }
}