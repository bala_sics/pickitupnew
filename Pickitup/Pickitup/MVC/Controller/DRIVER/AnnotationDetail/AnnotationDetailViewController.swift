//
//  AnnotationDetailViewController.swift
//  Pickitup
//
//  Created by Bala on 20/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class AnnotationDetailViewController: UIViewController {

    @IBOutlet var mapViewObj : MKMapView!
    @IBOutlet var viewTop : UIView!
    @IBOutlet var labelDestinationTime1 : UILabel!
    @IBOutlet var labelDestinationTime2 : UILabel!
    @IBOutlet var labelReadyTime : UILabel!
    var viewWellDone : UIView!
    @IBOutlet var heightSeparatorView : NSLayoutConstraint!
    var orderDetails : Orders!
    @IBOutlet var labelStoreName : UILabel!
    @IBOutlet var labelUserName : UILabel!

    var locationManager : CLLocationManager!
    var arrayCoordinates : [CLLocationCoordinate2D]!
    var isFirstTime : Bool!
    var arrayPolyLine1    : NSMutableArray = NSMutableArray()
    var arrayPolyLine2    : NSMutableArray = NSMutableArray()


    override func viewDidLoad() {
        super.viewDidLoad()

        ////UPDATE USER LOCATION
        locationManager = locationManager == nil ? CLLocationManager() : locationManager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        isFirstTime = true
        
        mapViewObj.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
        viewTop.backgroundColor = UIColor(red: 38.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 0.9)
        labelDestinationTime1.layer.cornerRadius = 5.0
        labelDestinationTime1.clipsToBounds = true
        labelDestinationTime2.layer.cornerRadius = 5.0
        labelDestinationTime2.clipsToBounds = true
        labelReadyTime.layer.cornerRadius = 5.0
        labelReadyTime.clipsToBounds = true
        heightSeparatorView.constant = 0.5
        
        loadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(AnnotationDetailViewController.calculateTime), object: nil)

    }
    
    //MARK:- LOAD DATA
    func loadData(){
        if(orderDetails.orderDeliveryTime != ""){
        let formatter : NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "hh:mm a"
        let date : NSDate = formatter.dateFromString(orderDetails.orderDeliveryTime)!
        formatter.dateFormat = "HH:mm"
        let dateStr : String = formatter.stringFromDate(date)
        labelReadyTime.text = CheckForNull(strToCheck: dateStr)
        }
        labelStoreName.text = "To \(orderDetails.orderStoreName)"
        labelUserName.text = "To \(orderDetails.orderUserName)"
        
        let info1 = BMAnnotation()
        info1.coordinate = CLLocationCoordinate2DMake(Double(orderDetails.orderStoreLatitude)!, Double(orderDetails.orderStoreLongitude)!)
        info1.distance = "\(orderDetails.orderDistance) km"
        info1.imageName = "Red"
        info1.tag = 0
        mapViewObj.addAnnotation(info1)
        
        let info2 = BMAnnotation()
        info2.coordinate = CLLocationCoordinate2DMake(Double(orderDetails.orderUserLatitude)!, Double(orderDetails.orderUserLongitude)!)
        info2.distance = "\(orderDetails.orderUserDistance) km"
        info2.imageName = "Pink"
        info1.tag = 1
        mapViewObj.addAnnotation(info2)
        calculateTime()
    }
    
    func calculateTime (){
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        let date : NSDate = dateFormatter.dateFromString("\(self.orderDetails.orderDeliveredDate) \(self.orderDetails.orderDeliveryTime)")!
        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
        let diff = NSCalendar.currentCalendar().components(.Minute, fromDate: NSDate(), toDate: date, options: []).minute
        labelDestinationTime1.text = diff < 1 ? "0" : "\(diff)"
        
        let date1 : NSDate = dateFormatter.dateFromString("\(self.orderDetails.orderedDate) \(self.orderDetails.orderedTime)")!
        let diff1 = NSCalendar.currentCalendar().components(.Minute, fromDate: NSDate(), toDate: date1, options: []).minute
        labelDestinationTime2.text = diff1 < 1 ? "0" : "\(diff1)"
        
        self.performSelector(#selector(AnnotationPopUpView.calculateTime), withObject: nil, afterDelay: 5.0)
    }

    
    //MARK:- FETCH ROUTES
    func drawRoute (source : CLLocationCoordinate2D , destination : CLLocationCoordinate2D , poly : NSMutableArray){
        let currentPlace: MKPlacemark = MKPlacemark(coordinate: source, addressDictionary: nil)
        let place: MKPlacemark = MKPlacemark(coordinate: destination, addressDictionary: nil)
        let request: MKDirectionsRequest = MKDirectionsRequest()
        request.source = MKMapItem(placemark: currentPlace)
        request.destination = MKMapItem(placemark: place)
        request.transportType = MKDirectionsTransportType.Any
        request.requestsAlternateRoutes = false
        let directions: MKDirections = MKDirections(request: request)
        directions.calculateDirectionsWithCompletionHandler() {
            (response, error) in
            if(error == nil && response != nil) {
                for route in response!.routes {
                    let r: MKRoute = route
                    poly.addObject(r.polyline)
                    self.mapViewObj.addOverlay(r.polyline, level: MKOverlayLevel.AboveRoads)
                }
            }
        }
    }

    //MARK:- GO BUTTON ACTION
    @IBAction func onGoButtonPressed (sender : UIButton){
        SVProgressHUD.showWithStatus("Processing...")
        Orders.acceptOrder(orderDetails.orderId) { (success, error) in
            if(success){
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
        
//        viewWellDone  = UIView(frame:  CGRectMake(0, screenSize.height , screenSize.width, screenSize.height))
//        viewWellDone.backgroundColor = UIColor.blackColor()
//        let imgvw : UIImageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
//        imgvw.image = UIImage(named: "WellDone")
//        viewWellDone.addSubview(imgvw)
//        AppDelegate.getAppdelegateInstance().window?.addSubview(viewWellDone)
//        UIView.animateWithDuration(0.4, animations: { () -> Void in
//            self.viewWellDone.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
//            }) { (completed) -> Void in
//                self.performSelector("dismissWellDoneView", withObject: nil, afterDelay: 3.0)
//        }
    }

    func dismissWellDoneView(){
        self.navigationController?.popToRootViewControllerAnimated(false)
        UIView.animateWithDuration(0.4, animations: { () -> Void in
            self.viewWellDone.frame = CGRectMake(0, screenSize.height , screenSize.width, screenSize.height)
            }) { (completed) -> Void in
                self.viewWellDone.removeFromSuperview()
        }
    }
}

//MARK:- MAPVIEW DELEGATES
extension AnnotationDetailViewController : MKMapViewDelegate{
    
    ///MARK:- VIEW FOR ANNOTATION
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            var messagePinView = mapView.dequeueReusableAnnotationViewWithIdentifier("UserLocation")
            if ( messagePinView == nil ){
                messagePinView = MKAnnotationView(annotation: annotation, reuseIdentifier: "UserLocation")
                messagePinView?.canShowCallout = false
                messagePinView?.image = UIImage(named: "DriverCurrentLocation")
            }
            return messagePinView
        }
        
        var messagePinView = mapView.dequeueReusableAnnotationViewWithIdentifier("\((annotation as! BMAnnotation).tag)")
        if(messagePinView == nil){
            messagePinView = AnnotationPin(annotation: annotation, reuseIdentifier: "\((annotation as! BMAnnotation).tag)") as AnnotationPin
            messagePinView!.canShowCallout = false
            let arrayMessageView = (NSBundle.mainBundle().loadNibNamed("AnnotationView", owner: self, options: nil) as NSArray)
            let myAnnotation : AnnotationView = arrayMessageView[0] as! AnnotationView
            messagePinView?.frame = CGRectMake(0, 0, 55, 45)
            myAnnotation.CustomizeAnnotation(annotation as! BMAnnotation)
            messagePinView!.layer.anchorPoint = CGPointMake (0.5, 1.0)
            messagePinView!.addSubview(myAnnotation)
        }
        return messagePinView
        
    }
    
    //MARK:- OVERLAY VIEW
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if(overlay.isKindOfClass(MKPolyline)){
            if arrayPolyLine1.containsObject((overlay as? MKPolyline)!) {
                let polyLineRenderer = MKPolylineRenderer(overlay: overlay)
                polyLineRenderer.strokeColor = UIColor(red: 31.0/255.0, green: 138.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                polyLineRenderer.lineWidth = 5
                
                return polyLineRenderer
            } else if arrayPolyLine2.containsObject((overlay as? MKPolyline)!) {
                print(overlay.description)
                let polyLineRenderer = MKPolylineRenderer(overlay: overlay)
                polyLineRenderer.strokeColor = UIColor(red: 13.0/255.0, green: 82.0/255.0, blue: 108.0/255.0, alpha: 1.0)
                polyLineRenderer.lineWidth = 5
                
                return polyLineRenderer
            }
        }
        return MKPolylineRenderer()
    }
}

//MARK:- LOCATION MANAGER DELEGATES
extension AnnotationDetailViewController : CLLocationManagerDelegate{
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc : CLLocation = (locations as NSArray).firstObject as! CLLocation
        locationManager.stopUpdatingLocation()
        if(isFirstTime == true){
            isFirstTime = false
            self.drawRoute(loc.coordinate, destination: CLLocationCoordinate2DMake(Double(orderDetails.orderStoreLatitude)!, Double(orderDetails.orderStoreLongitude)!), poly: arrayPolyLine1)
            self.drawRoute(CLLocationCoordinate2DMake(Double(orderDetails.orderStoreLatitude)!, Double(orderDetails.orderStoreLongitude)!), destination: CLLocationCoordinate2DMake(Double(orderDetails.orderUserLatitude)!, Double(orderDetails.orderUserLongitude)!), poly: arrayPolyLine2)
        }
    }
}

