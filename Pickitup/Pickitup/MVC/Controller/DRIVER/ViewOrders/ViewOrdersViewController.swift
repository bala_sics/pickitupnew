//
//  ViewOrdersViewController.swift
//  Pickitup
//
//  Created by Bala on 11/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewOrdersViewController: UIViewController {
    
    @IBOutlet var mapViewObj : MKMapView!
    @IBOutlet var viewTop : UIView!
    var locationManager : CLLocationManager!
    var detailsDict : [String:String]!
    var isFirstTime : Bool!
    var arrayPolyLine1    : NSMutableArray = NSMutableArray()
    var arrayPolyLine2    : NSMutableArray = NSMutableArray()
    @IBOutlet var labelTime : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isFirstTime = true
        mapViewObj.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
        viewTop.backgroundColor = UIColor(red: 38.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 0.9)
        fetchOrders()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
        NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(ViewOrdersViewController.calculateTimeDifference), object: nil)
    }
    
    func fetchOrders()  {
        SVProgressHUD.showWithStatus("Processing...")
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["driver_id" : Driver.currentPerson().userId]
        let url : String = base_Url + "driver_vieworders"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            if  (result["result"] as! String == "success"){
                self.detailsDict  = (result["details"] as! NSArray).objectAtIndex(0) as! [String : String]
                    ////UPDATE USER LOCATION
                    self.locationManager = self.locationManager == nil ? CLLocationManager() : self.locationManager
                    self.locationManager.delegate = self
                    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    self.locationManager.requestAlwaysAuthorization()
                    self.locationManager.startUpdatingLocation()
                    self.calculateTimeDifference()
                    self.addAnnotation()
                   SVProgressHUD.dismiss()
                
            }else{
                 SVProgressHUD.showErrorWithStatus("No order found")
            }
        }) { (operation, error) -> Void in
            SVProgressHUD.showErrorWithStatus(error.localizedDescription)
        }
    }
    
    //MARK:- DELIVERY TIME CALCULATION
    func calculateTimeDifference() {
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm a"
        let date : NSDate = dateFormatter.dateFromString("\(detailsDict["date"]!) \(detailsDict["delivered_time"]!)")!
        dateFormatter.timeZone = NSTimeZone.systemTimeZone()
        let diff = NSCalendar.currentCalendar().components(.Minute, fromDate: NSDate(), toDate: date, options: []).minute
        labelTime.text = diff < 1 ? "Order delivery will come in 0mn" : "Order delivery will come in \(diff)mn"
        self.performSelector(#selector(ViewOrdersViewController.calculateTimeDifference), withObject: nil, afterDelay: 5.0)
    }
    
    //MARK:- ADD ANNOTATIONS
    func addAnnotation()  {
        let info1 = BMAnnotation()
        info1.coordinate = CLLocationCoordinate2DMake(Double(detailsDict["Storelatitude"]!)!, Double(detailsDict["Storelongitude"]!)!)
        info1.distance = "\(detailsDict["distance_from_driver_store"]!) km"
        info1.imageName = "Red"
        info1.tag = 0
        mapViewObj.addAnnotation(info1)
        
        let info2 = BMAnnotation()
        info2.coordinate = CLLocationCoordinate2DMake(Double(detailsDict["Userlatitude"]!)!, Double(detailsDict["Userlongitude"]!)!)
        info2.distance = "\(detailsDict["distance_from_store_customer"]!) km"
        info2.imageName = "Pink"
        info1.tag = 1
        mapViewObj.addAnnotation(info2)
        
    }
    
    //MARK:- GPS BUTTON ACTION
    @IBAction func onGpsbuttonPressed (sender : UIButton){
        mapViewObj.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
        
    }
    
    //MARK:- CLOSE BUTTON ACTION
    @IBAction func onCloseButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- ORDER DETAILS BUTTON ACTION
    @IBAction func onOrderDetailsButtonPressed (sender : UIButton){
        if(detailsDict == nil) { return }
        let dateStr = detailsDict["date"]!
        let dateformatter : NSDateFormatter = NSDateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let date = dateformatter.dateFromString(dateStr)
        dateformatter.dateStyle = .FullStyle
        let bill : BillTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("BillVC") as! BillTableViewController
        bill.orderId = detailsDict["orderid"]!
        bill.orderDate = dateformatter.stringFromDate(date!)
        self.navigationController?.pushViewController(bill, animated: true)
        
    }
    
    //MARK:- FETCH ROUTES
    func drawRoute (source : CLLocationCoordinate2D , destination : CLLocationCoordinate2D , poly : NSMutableArray){
        let currentPlace: MKPlacemark = MKPlacemark(coordinate: source, addressDictionary: nil)
        let place: MKPlacemark = MKPlacemark(coordinate: destination, addressDictionary: nil)
        let request: MKDirectionsRequest = MKDirectionsRequest()
        request.source = MKMapItem(placemark: currentPlace)
        request.destination = MKMapItem(placemark: place)
        request.transportType = MKDirectionsTransportType.Any
        request.requestsAlternateRoutes = false
        let directions: MKDirections = MKDirections(request: request)
        directions.calculateDirectionsWithCompletionHandler() {
            (response, error) in
            if(error == nil && response != nil) {
                for route in response!.routes {
                    let r: MKRoute = route
                    poly.addObject(r.polyline)
                    self.mapViewObj.addOverlay(r.polyline, level: MKOverlayLevel.AboveRoads)
                }
            }
        }
    }
}

//MARK:- MAPVIEW DELEGATES
extension ViewOrdersViewController : MKMapViewDelegate{
    
    ///MARK:- VIEW FOR ANNOTATION
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) {
            var messagePinView = mapView.dequeueReusableAnnotationViewWithIdentifier("UserLocation")
            if ( messagePinView == nil ){
                messagePinView = MKAnnotationView(annotation: annotation, reuseIdentifier: "UserLocation")
                messagePinView?.canShowCallout = false
                messagePinView?.image = UIImage(named: "DriverCurrentLocation")
            }
            return messagePinView
        }
        
        var messagePinView = mapView.dequeueReusableAnnotationViewWithIdentifier("\((annotation as! BMAnnotation).tag)")
        if(messagePinView == nil){
            messagePinView = AnnotationPin(annotation: annotation, reuseIdentifier: "\((annotation as! BMAnnotation).tag)") as AnnotationPin
            messagePinView!.canShowCallout = false
            let arrayMessageView = (NSBundle.mainBundle().loadNibNamed("AnnotationView", owner: self, options: nil) as NSArray)
            let myAnnotation : AnnotationView = arrayMessageView[0] as! AnnotationView
            messagePinView?.frame = CGRectMake(0, 0, 55, 45)
            myAnnotation.CustomizeAnnotation(annotation as! BMAnnotation)
            messagePinView!.layer.anchorPoint = CGPointMake (0.5, 1.0)
            messagePinView!.addSubview(myAnnotation)
        }
        return messagePinView
        
    }
    
    //MARK:- OVERLAY VIEW
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        if(overlay.isKindOfClass(MKPolyline)){
            if arrayPolyLine1.containsObject((overlay as? MKPolyline)!) {
                let polyLineRenderer = MKPolylineRenderer(overlay: overlay)
                polyLineRenderer.strokeColor = UIColor(red: 31.0/255.0, green: 138.0/255.0, blue: 178.0/255.0, alpha: 1.0)
                polyLineRenderer.lineWidth = 5
                
                return polyLineRenderer
            } else if arrayPolyLine2.containsObject((overlay as? MKPolyline)!) {
                print(overlay.description)
                let polyLineRenderer = MKPolylineRenderer(overlay: overlay)
                polyLineRenderer.strokeColor = UIColor(red: 13.0/255.0, green: 82.0/255.0, blue: 108.0/255.0, alpha: 1.0)
                polyLineRenderer.lineWidth = 5
                
                return polyLineRenderer
            }
        }
        return MKPolylineRenderer()
    }
}

//MARK:- LOCATION MANAGER DELEGATES
extension ViewOrdersViewController : CLLocationManagerDelegate{
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc : CLLocation = (locations as NSArray).firstObject as! CLLocation
        locationManager.stopUpdatingLocation()
        if(isFirstTime == true){
            isFirstTime = false
            self.drawRoute(loc.coordinate, destination: CLLocationCoordinate2DMake(Double(detailsDict["Storelatitude"]!)!, Double(detailsDict["Storelongitude"]!)!), poly: arrayPolyLine1)
            
            self.drawRoute(CLLocationCoordinate2DMake(Double(detailsDict["Storelatitude"]!)!, Double(detailsDict["Storelongitude"]!)!), destination:CLLocationCoordinate2DMake(Double(detailsDict["Userlatitude"]!)!, Double(detailsDict["Userlongitude"]!)!), poly: arrayPolyLine2)
        }
    }
}
