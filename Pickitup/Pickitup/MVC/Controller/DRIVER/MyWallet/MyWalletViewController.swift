//
//  MyWalletViewController.swift
//  Pickitup
//
//  Created by Bala on 20/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class MyWalletViewController: UIViewController {

    @IBOutlet var imageviewProfile : UIImageView!
    @IBOutlet var heightSeparatorView : NSLayoutConstraint!
    @IBOutlet var labelSalary : UILabel!
    @IBOutlet var labelDriverName : UILabel!
    @IBOutlet var arrayImageView : [UIImageView]!
    @IBOutlet var labelRatingCount : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "My wallet"
        imageviewProfile.layer.cornerRadius = 75.0
        imageviewProfile.clipsToBounds = true
        heightSeparatorView.constant = 0.5

        fetchMySalary()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- FETCH SALARY
    func fetchMySalary()  {
        SVProgressHUD.showWithStatus("Processing...")
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["driver_id" : Driver.currentPerson().userId]
        let url : String = base_Url + "driver_salary"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            if  (result["result"] as! String == "success"){
                let detailsDict : NSDictionary = (result["details"] as! NSArray).objectAtIndex(0) as! NSDictionary
                self.labelSalary.text = "\(detailsDict["salary"] as! String)$"
                self.labelDriverName.text = detailsDict["name"] as? String
                self.labelRatingCount.text = "\(detailsDict["no_of_customers_rated"] as! String) Customers"

                self.calculateStarRating(Float(detailsDict["star_rating"] as! String)!, customersCount: Float(detailsDict["no_of_customers_rated"] as! String)!)
            }
            SVProgressHUD.dismiss()
        }) { (operation, error) -> Void in
            SVProgressHUD.showErrorWithStatus(error.localizedDescription)
        }
    }
    
    //MARK:- CALCULATE RATING
    func calculateStarRating(starCount : Float , customersCount : Float) {
        let count : Float = starCount == 0 || customersCount == 0 ? 0 : starCount/customersCount
        let roundValue : Int = Int(round(count))
        for imgVw in arrayImageView {
            imgVw.image = imgVw.tag < roundValue ? UIImage(named: "StarSelected") : UIImage(named: "StarUnselected")
        }
    }
}
