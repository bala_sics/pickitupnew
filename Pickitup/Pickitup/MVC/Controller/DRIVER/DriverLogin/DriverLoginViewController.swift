//
//  DriverLoginViewController.swift
//  Pickitup
//
//  Created by Bala on 11/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class DriverLoginViewController: UIViewController {

    @IBOutlet weak var textFieldUserName : UITextField!
    @IBOutlet weak var textFieldPassword : UITextField!
    @IBOutlet weak var labelBottom : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldPassword.setLeftPadding(10)
        textFieldUserName.setLeftPadding(10)
        let attrStr : NSMutableAttributedString = NSMutableAttributedString(string: "If you want to become a driver,please \nsubmit on the website. www.pickitup.com")
        attrStr.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 73.0/255.0, green: 187.0/255.0, blue: 191.0/255.0, alpha: 1.0), range: NSMakeRange(61, 17))
        labelBottom.attributedText = attrStr
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onForgotPasswordPressed (sender : UIButton){
        let forgot : ForgotPasswordViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ForgotVC") as! ForgotPasswordViewController
        forgot.hideLabelBottom = false
        self.navigationController?.pushViewController(forgot, animated: true)
    }
    
    @IBAction func onConnectButtonPressed (sender : UIButton){
        if(textFieldPassword.text == "" || textFieldUserName.text == ""){
            UIAlertView(title: "Warning", message: "Please fill all fields", delegate: nil, cancelButtonTitle: "Ok").show()
        }else {
            loginWithJson()
        }

    }
    
    func loginWithJson (){
        SVProgressHUD.showWithStatus("Processing...")
        Driver.driverLoginWithUserDetails(textFieldUserName.text!, password: textFieldPassword.text!) { (success, result, error) -> Void in
            if(success){
                SVProgressHUD.dismiss()
                AppDelegate.getAppdelegateInstance().setDriverHomePage()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
}

//MARK:- TEXTFIELD DELEGATE
extension DriverLoginViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == textFieldUserName){
            textFieldPassword.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
           // AppDelegate.getAppdelegateInstance().setHomePage()
        }
        return true
    }
}