//
//  DriverSideMenuTableViewController.swift
//  Pickitup
//
//  Created by Bala on 11/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class DriverSideMenuTableViewController: UITableViewController {
    @IBOutlet var imageViewProfile : UIImageView!
    @IBOutlet var viewFooter : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue:0/255.0, alpha: 0.7)
        imageViewProfile.layer.cornerRadius = 25.0
        imageViewProfile.clipsToBounds = true
        if(IS_IPHONE6){
            viewFooter.frame = CGRectMake(viewFooter.frame.origin.x, viewFooter.frame.origin.y, viewFooter.frame.size.width, 99)
        }else if(IS_IPHONE6PLUS){
            viewFooter.frame = CGRectMake(viewFooter.frame.origin.x, viewFooter.frame.origin.y, viewFooter.frame.size.width, 168)
        }else{
            self.tableView.tableFooterView = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if(self.respondsToSelector(Selector("setSeparatorInset:"))){
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
        if(cell.respondsToSelector(Selector("setLayoutMargins:"))){
            cell.layoutMargins =  UIEdgeInsetsMake(0, 0, 0, 0)
            ;
        }
        if(cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:"))){
            cell.preservesSuperviewLayoutMargins = false;
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let storybpard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        switch indexPath.row{
        case 1 :
            let viewOrder : ViewOrdersViewController = storybpard.instantiateViewControllerWithIdentifier("ViewOrdersVC") as! ViewOrdersViewController
            self.sideMenuController()?.setContentViewController(viewOrder)
            break
        case 2 :
            let notify : NotificationTableViewController = storybpard.instantiateViewControllerWithIdentifier("NotificationsVC") as! NotificationTableViewController
            self.sideMenuController()?.setContentViewController(notify)
            break
        case 3 :
            let lastOrder : LastOrdersTableViewController = storybpard.instantiateViewControllerWithIdentifier("LastOrderVC") as! LastOrdersTableViewController
            self.sideMenuController()?.setContentViewController(lastOrder)
            break
        case 4 :
            let wallet : MyWalletViewController = storybpard.instantiateViewControllerWithIdentifier("MyWalletVC") as! MyWalletViewController
            self.sideMenuController()?.setContentViewController(wallet)
            break
        case 5 :
            let credit : CreditCardTableViewController = storybpard.instantiateViewControllerWithIdentifier("CreditCardVC") as! CreditCardTableViewController
            self.sideMenuController()?.setContentViewController(credit)
            break
        case 6 :
            self.sideMenuController()?.sideMenu?.toggleMenu()
            Driver.driverLogout()
            AppDelegate.getAppdelegateInstance().setHomePage()
            break
        case 7 :
            let privacy : PrivacyPolicyViewController = storybpard.instantiateViewControllerWithIdentifier("PrivacyPolicyVC") as! PrivacyPolicyViewController
            privacy.navigationTitle = "About us"
            self.sideMenuController()?.setContentViewController(privacy)
            break
        case 8 :
            let share : ShareViewController = storybpard.instantiateViewControllerWithIdentifier("ShareVC") as! ShareViewController
            self.sideMenuController()?.setContentViewController(share)
            break
        case 9 :
            let contact : ContactUsTableViewController = storybpard.instantiateViewControllerWithIdentifier("ContactUsVC") as! ContactUsTableViewController
            self.sideMenuController()?.setContentViewController(contact)
            break
        case 10 :
            let privacy : PrivacyPolicyViewController = storybpard.instantiateViewControllerWithIdentifier("PrivacyPolicyVC") as! PrivacyPolicyViewController
            privacy.navigationTitle = "Privacy policy"
            self.sideMenuController()?.setContentViewController(privacy)
            break
        case 11 :
            let privacy : PrivacyPolicyViewController = storybpard.instantiateViewControllerWithIdentifier("PrivacyPolicyVC") as! PrivacyPolicyViewController
            privacy.navigationTitle = "Terms of use"
            self.sideMenuController()?.setContentViewController(privacy)
            break

        default :
            self.sideMenuController()?.sideMenu?.toggleMenu()
            break
        }
    }


}
