//
//  NotificationTableViewController.swift
//  Pickitup
//
//  Created by Bala on 20/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class NotificationTableViewController: UITableViewController {

    var dictDetails : NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Notifications"
        
        let imageVw : UIImageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
        imageVw.image = UIImage(named: "Background")
        self.tableView.backgroundView = imageVw
        fetchNotifications()

    }

    func fetchNotifications() {
        SVProgressHUD.showWithStatus("Processing...")
        Notifications.fetchAllNotifications { (success, result, error) in
            if success{
                self.dictDetails = result as! NSDictionary
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.dictDetails.allKeys.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dictDetails[self.dictDetails.allKeys[section] as! String] as! NSArray).count
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader : UIView = UIView(frame: CGRectMake(0, 0, screenSize.width, 30))
        viewHeader.backgroundColor = UIColor(red: 38.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 0.9)
        let labelDate :UILabel = UILabel(frame: CGRectMake(0, 0, screenSize.width, 30))
        labelDate.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        labelDate.text = "     \(self.dictDetails.allKeys[section])"
        labelDate.textColor = UIColor.whiteColor()
        viewHeader.addSubview(labelDate)
        
        let viewSeparator : UIView = UIView(frame: CGRectMake(0, 29, screenSize.width, 0.5))
        viewSeparator.backgroundColor = UIColor.lightGrayColor()
        viewHeader.addSubview(viewSeparator)

        return viewHeader
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let dict : NSDictionary = (dictDetails[self.dictDetails.allKeys[indexPath.section] as! String] as! NSArray).objectAtIndex(indexPath.row) as! NSDictionary
        let label : UILabel = cell?.viewWithTag(100) as! UILabel
        label.text = dict["notification"] as? String
        
        return cell!
    }

}
