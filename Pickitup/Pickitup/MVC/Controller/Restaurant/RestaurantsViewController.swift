//
//  RestaurantsViewController.swift
//  Pickitup
//
//  Created by Bala on 29/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class RestaurantsViewController: UIViewController {

    @IBOutlet var viewSearch : UIView!
    @IBOutlet var viewNavigation : UIView!
    @IBOutlet var textfieldSearch : UITextField!
    @IBOutlet var tableViewRestaurants : UITableView!
    var tapGestureSearch : BMTapGestureRecognizer!
    @IBOutlet var arrayViews : [UIView]!
    var arrayCategories : [String] = ["Meat","Veggie","Kosher","Dairy","Glutten free"]
    var arraySelectedCategories : NSMutableArray = NSMutableArray()
    var arrayRestaurants : NSMutableArray = NSMutableArray()
    var isSearching : Bool = false
    var arraySearch : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearch.layer.cornerRadius = 3.0
        viewSearch.layer.borderWidth = 1.0
        viewSearch.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewSearch.clipsToBounds = true
        
        viewNavigation.layer.cornerRadius = 1.0
        viewNavigation.layer.borderWidth = 1.0
        viewNavigation.layer.borderColor = UIColor(red: 37.0/255.0, green: 191.0/255.0, blue: 197.0/255.0, alpha: 1.0).CGColor
        viewNavigation.clipsToBounds = true

        
        
        for viewObj in arrayViews{
            let gesture : BMTapGestureRecognizer = BMTapGestureRecognizer(target: self, action: #selector(RestaurantsViewController.onCategorySelected(_:)))
            viewObj.addGestureRecognizer(gesture)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
        fetchRestaurants("")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
    }
    
    //MARK:- FETCH STORES
    func fetchRestaurants(category : String){
        SVProgressHUD.showWithStatus("Processing...")
        Stores.fetchAllRestaurants(category) { (success, result, error) -> Void in
            if(success){
                self.arrayRestaurants.removeAllObjects()
                self.arrayRestaurants.addObjectsFromArray(result as! [AnyObject])
                self.tableViewRestaurants.reloadData()
                SVProgressHUD.dismiss()
            }else{
                self.arrayRestaurants.removeAllObjects()
                self.tableViewRestaurants.reloadData()
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }

    //MARK:- CATEGORY ACTION
    func onCategorySelected (gesture : BMTapGestureRecognizer){
        let viewObj = gesture.view
        let buttonMarker : UIButton = viewObj?.viewWithTag(100) as! UIButton
        buttonMarker.selected = !buttonMarker.selected
        if(arraySelectedCategories.containsObject(arrayCategories[viewObj!.tag])){
            arraySelectedCategories.removeObject(arrayCategories[viewObj!.tag])
        }else{
            arraySelectedCategories.addObject(arrayCategories[viewObj!.tag])
        }
        fetchRestaurants(arraySelectedCategories.componentsJoinedByString(","))
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- BUTTON ACTION
    //MARK:- BACK BUTTON ACTION
    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- VIEW STORE BUTTON ACTION
    @IBAction func onViewStoreButtonPressed (sender : UIButton){
        let store : StoreServiceViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreServiceVC") as! StoreServiceViewController
        self.navigationController?.pushViewController(store, animated: true)
    }
    
    //MARK:- TAP GESTURE ACTION
    func onTapGestureSearchTapped(){
        textfieldSearch.resignFirstResponder()
        isSearching = false
        if(tapGestureSearch != nil) { self.navigationController?.view.removeGestureRecognizer(tapGestureSearch)}
    }

    func searchRestaurant(str : String){
        let predicate : NSPredicate = NSPredicate(format: "storeName contains[CD] %@", str)
        let array = arrayRestaurants.filteredArrayUsingPredicate(predicate)
        arraySearch.removeAllObjects()
        arraySearch.addObjectsFromArray(array)
        tableViewRestaurants.reloadData()
    }

}

// MARK: - TABLE VIEW DELEGATE
extension RestaurantsViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching == true ? arraySearch.count : arrayRestaurants.count
    }
     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 310
    }
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = RestaurantsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        
        let stor : Stores = isSearching == true ? arraySearch[indexPath.row] as! Stores : arrayRestaurants[indexPath.row] as! Stores
        (cell as! RestaurantsTableViewCell).configureCell(stor)
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item : ItemDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ItemDetailVC") as! ItemDetailsViewController
        item.selectedStore = isSearching == true ? arraySearch[indexPath.row] as! Stores : arrayRestaurants[indexPath.row] as! Stores
        self.navigationController?.pushViewController(item, animated: true)
    }
}

//MARK:- TEXTFIELD DELEGATE
extension RestaurantsViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        isSearching = true
        tapGestureSearch = BMTapGestureRecognizer(target: self, action: #selector(RestaurantsViewController.onTapGestureSearchTapped))
        self.navigationController?.view.addGestureRecognizer(tapGestureSearch)
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        isSearching = false
        textfieldSearch.text = ""
        tableViewRestaurants.reloadData()
        if(tapGestureSearch != nil) { self.navigationController?.view.removeGestureRecognizer(tapGestureSearch)}
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        searchRestaurant(textField.text! + string)
        return true
    }

}