//
//  MainNavigationController.swift
//  Pickitup
//
//  Created by Bala on 29/12/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MainNavigationController: ENSideMenuNavigationController, ENSideMenuDelegate,UIGestureRecognizerDelegate {
    
    var tapGesture : BMTapGestureRecognizer!
    var sideMenuControllerObj : UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: sideMenuControllerObj, menuPosition:.Left)
        sideMenu?.delegate = self //optional
        sideMenu?.menuWidth = IS_IPHONE6 || IS_IPHONE6PLUS ? 250 : 220 //optional, default is 160
       // sideMenu?.bouncingEnabled = false
        sideMenu?.allowPanGesture = false
        // make navigation bar showing over side menu
        view.bringSubviewToFront(navigationBar)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sideMenuDidOpen() {
        if(tapGesture == nil){
            tapGesture = BMTapGestureRecognizer(target: self, action: #selector(MainNavigationController.tappedOnView(_:)))
            tapGesture.delegate = self
            self.view.addGestureRecognizer(tapGesture)
        }
    }
    
    func sideMenuDidClose() {
        if(tapGesture != nil){
            self.view.removeGestureRecognizer(tapGesture)
            tapGesture = nil
        }
    }
    
    func tappedOnView(gesture : BMTapGestureRecognizer){
        self.sideMenu?.hideSideMenu()
    }

    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if(NSStringFromClass(touch.view!.classForCoder) == "UITableViewCellContentView"){
            if(touch.view!.tag == 100){
            return false
            }
        }
        return true
    }
}

