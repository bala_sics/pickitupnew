//
//  StoreItemsTableViewController.swift
//  Pickitup
//
//  Created by Bala on 08/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class StoreItemsTableViewController: UITableViewController {
    var arrayStores : NSMutableArray = NSMutableArray()
    var selectedCategory : String!
    var isSearching : Bool = false
    var arraySearch : NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        fetchStores()
    }

    //MARK:- FETCH STORES
    func fetchStores(){
        SVProgressHUD.showWithStatus("Processing...")
        Stores.fetchAllStores(selectedCategory) { (success, result, error) -> Void in
            if(success){
                self.arrayStores.removeAllObjects()
                self.arrayStores.addObjectsFromArray(result as! [AnyObject])
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    func searchStore(str : String){
        let predicate : NSPredicate = NSPredicate(format: "storeName contains[CD] %@", str)
        let array = arrayStores.filteredArrayUsingPredicate(predicate)
        arraySearch.removeAllObjects()
        arraySearch.addObjectsFromArray(array)
        self.tableView.reloadData()
    }
    //MARK:- TABLEVIEW DELEGATES
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching == true ? arraySearch.count : arrayStores.count
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 310
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = RestaurantsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let stor : Stores = isSearching == true ? arraySearch[indexPath.row] as! Stores : arrayStores[indexPath.row] as! Stores
        (cell as! RestaurantsTableViewCell).configureCell(stor)
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item : ItemDetailsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ItemDetailVC") as! ItemDetailsViewController
        item.selectedStore = isSearching == true ? arraySearch[indexPath.row] as! Stores : arrayStores[indexPath.row] as! Stores
       AppDelegate.getAppdelegateInstance().mainNavigation.pushViewController(item, animated: true)
    }

}
