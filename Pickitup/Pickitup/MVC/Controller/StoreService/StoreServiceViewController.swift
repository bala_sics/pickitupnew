//
//  StoreServiceViewController.swift
//  Pickitup
//
//  Created by Bala on 02/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class StoreServiceViewController: UIViewController {

    @IBOutlet var viewSearch : UIView!
    @IBOutlet var textfieldSearch : UITextField!
    var tapGestureSearch : BMTapGestureRecognizer!
    @IBOutlet var viewNavigation : UIView!

    var pageControl : ADPageControl!
    
    var arrayVC : [StoreItemsTableViewController]!
    var currentIndex : Int32!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearch.layer.cornerRadius = 3.0
        viewSearch.layer.borderWidth = 1.0
        viewSearch.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewSearch.clipsToBounds = true
        
        viewNavigation.layer.cornerRadius = 1.0
        viewNavigation.layer.borderWidth = 1.0
        viewNavigation.layer.borderColor = UIColor(red: 37.0/255.0, green: 191.0/255.0, blue: 197.0/255.0, alpha: 1.0).CGColor
        viewNavigation.clipsToBounds = true

        let stor : StoreItemsTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsVC") as! StoreItemsTableViewController
        stor.selectedCategory = "Grocery store"

        let stor1 : StoreItemsTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsVC") as! StoreItemsTableViewController
        stor1.selectedCategory = "Laundry"

        let stor2 : StoreItemsTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsVC") as! StoreItemsTableViewController
        stor2.selectedCategory = "Flowers"

        let stor3 : StoreItemsTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsVC") as! StoreItemsTableViewController
        stor3.selectedCategory = "High tech"

        let stor4 : StoreItemsTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("StoreItemsVC") as! StoreItemsTableViewController
        stor4.selectedCategory = "Para-pharmacy"

        arrayVC = [stor,stor1,stor2,stor3,stor4]
        
        setupPageControl()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
        
    }
    
    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func setupPageControl (){
        //page 0
        let pageModel0 = ADPageModel()
        pageModel0.strPageTitle = "Grocery store"
        pageModel0.iPageNumber = 0
        pageModel0.viewController = arrayVC[0]
        
        let pageModel1 = ADPageModel()
        pageModel1.strPageTitle = "Laundry"
        pageModel1.iPageNumber = 1
        pageModel1.bShouldLazyLoad = true
        
        let pageModel2 = ADPageModel()
        pageModel2.strPageTitle = "Flowers"
        pageModel2.iPageNumber = 2
        pageModel2.bShouldLazyLoad = true
        
        let pageModel3 = ADPageModel()
        pageModel3.strPageTitle = "High tech"
        pageModel3.iPageNumber = 3
        pageModel3.bShouldLazyLoad = true
        
        let pageModel4 = ADPageModel()
        pageModel4.strPageTitle = "Para-pharmacy"
        pageModel4.iPageNumber = 4
        pageModel4.bShouldLazyLoad = true
        
        pageControl = ADPageControl()
        pageControl.delegateADPageControl = self
        pageControl.arrPageModel = NSMutableArray(objects: pageModel0,pageModel1,pageModel2,pageModel3,pageModel4)

        /**** 3. Customize parameters (Optinal, as all have default value set) ****/
        
        pageControl.iFirstVisiblePageNumber = 2;
        pageControl.iTitleViewHeight = 32;
        pageControl.iPageIndicatorHeight = 2;
        pageControl.fontTitleTabText =  UIFont(name: "Myriad Hebrew", size: 13.0)
        
        pageControl.bEnablePagesEndBounceEffect = false;
        pageControl.bEnableTitlesEndBounceEffect = false;
        
        pageControl.colorTabText = UIColor.darkGrayColor()
        pageControl.colorTitleBarBackground = UIColor.whiteColor()
        pageControl.colorPageIndicator = UIColor(red: 37.0/255.0, green: 191.0/255.0, blue: 197.0/255.0, alpha: 1.0)
        pageControl.colorPageOverscrollBackground = UIColor.lightGrayColor()
        
        pageControl.bShowMoreTabAvailableIndicator = false;
        
        
        
        /**** 3. Add as subview ****/
        
        pageControl.view.frame = CGRectMake(0, 94, self.view.bounds.size.width, self.view.bounds.size.height - 94)
        self.view.addSubview(pageControl.view)

    }
    
    //MARK:- VIEW RESTAURANT BUTTON ACTION
    @IBAction func onViewRestaurantButtonPressed (sender : UIButton){
        let restaurant : RestaurantsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("RestaurantsVC") as! RestaurantsViewController
        self.navigationController?.pushViewController(restaurant, animated: true)
    }
    
    
    func onTapGestureSearchTapped(){
        textfieldSearch.resignFirstResponder()
        arrayVC[Int(currentIndex)].isSearching = false
        if(tapGestureSearch != nil) { self.navigationController?.view.removeGestureRecognizer(tapGestureSearch)}
    }
}

//MARK:- TEXTFIELD DELEGATE
extension StoreServiceViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        arrayVC[Int(currentIndex)].isSearching = true
        tapGestureSearch = BMTapGestureRecognizer(target: self, action: #selector(StoreServiceViewController.onTapGestureSearchTapped))
        self.navigationController?.view.addGestureRecognizer(tapGestureSearch)
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
         arrayVC[Int(currentIndex)].isSearching = false
        arrayVC[Int(currentIndex)].tableView.reloadData()
        textField.text = ""
        if(tapGestureSearch != nil) { self.navigationController?.view.removeGestureRecognizer(tapGestureSearch)}
        textField.resignFirstResponder()
        return true
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        arrayVC[Int(currentIndex)].searchStore(textField.text! + string)
        return true
    }
}



extension StoreServiceViewController : ADPageControlDelegate{
    func adPageControlGetViewControllerForPageModel(pageModel: ADPageModel!) -> UIViewController! {
        return arrayVC[Int(pageModel.iPageNumber)]
    }
    func adPageControlCurrentVisiblePageIndex(iCurrentVisiblePage: Int32) {
        currentIndex = iCurrentVisiblePage
    }
}