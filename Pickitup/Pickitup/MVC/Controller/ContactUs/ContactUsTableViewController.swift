//
//  ContactUsTableViewController.swift
//  Pickitup
//
//  Created by Bala on 07/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class ContactUsTableViewController: UITableViewController {

    @IBOutlet var arrayButtons : [UIButton]!
    @IBOutlet var textviewDescription : GCPlaceholderTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationItem.title = "CONTACT US"
        
        textviewDescription.layer.cornerRadius = 3.0
        textviewDescription.layer.borderColor = UIColor.whiteColor().CGColor
        textviewDescription.layer.borderWidth = 0.5
        textviewDescription.placeholder = "Description..."
        textviewDescription.placeholderColor = UIColor.whiteColor()
        textviewDescription.textColor = UIColor.whiteColor()
        ((arrayButtons as NSArray).objectAtIndex(1) as! UIButton).selected = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onCheckmarkButtonsPressed (sender : UIButton){
        (arrayButtons as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            (obj as! UIButton).selected = !(obj as! UIButton).selected
        }
    }
}

//MARK:- TEXTVIEW DELEGATES
extension ContactUsTableViewController : UITextViewDelegate{
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        textviewDescription.textColor = UIColor.whiteColor()
        if(text == "\n")
        {
            textView.resignFirstResponder()
        }
        
        return true
    }

}