//
//  DeliveryTimeViewController.swift
//  Pickitup
//
//  Created by Bala on 11/03/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

protocol deliveryTimeDelegate {
    func didFinishSelectingDeliveryTime (delvryDay : String , delvryTime : String)
}
class DeliveryTimeViewController: UIViewController {

    @IBOutlet var arrayButtons : [UIButton]!
    @IBOutlet var labelTime1 : UILabel!
    @IBOutlet var labelTime2 : UILabel!
    @IBOutlet var toolbar : UIToolbar!
    @IBOutlet var timePicker : UIDatePicker!
    @IBOutlet var textFIeldTime : UITextField!
    var selectedDay : String = "Today"
    var dictTime : NSDictionary!
    var delegate : deliveryTimeDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayButtons[0].selected = true
        textFIeldTime.inputView = timePicker
        textFIeldTime.inputAccessoryView = toolbar
        fetchTime()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
        
    }
    
    func fetchTime (){
        if(Cart.getStoreId() == "") { return }
        SVProgressHUD.showWithStatus("Processing...")
        Cart.fetchAvailableTime(Cart.getStoreId()) { (success, result, error) -> Void in
            if(success){
                self.dictTime = result as! NSDictionary
                let today : NSDictionary = self.dictTime["Today"] as! NSDictionary
                self.labelTime1.text = today["time1"] as? String
                self.labelTime2.text = today["time2"] as? String
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    

    //MARK:- BUTTON ACTIONS
    //MARK:- CLOSE BUTTON ACTION
    @IBAction func onCloseButtonPressed (Sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- DAYS BUTTON ACTION
    @IBAction func onDayChanged(sender : UIButton){
        selectedDay = sender.tag == 0 ? "Today" : "Tomorrow"
        let today : NSDictionary = self.dictTime[selectedDay] as! NSDictionary
        self.labelTime1.text = today["time1"] as? String
        self.labelTime2.text = today["time2"] as? String
        for btn in arrayButtons{
            btn.selected = btn.tag == sender.tag
        }
    }

    //MARK:- BOTTOM DONE BUTTON ACTION
    @IBAction func onDoneButtonPressed (sender : UIButton){
        if(Cart.getStoreId() == "") {
            self.navigationController?.popViewControllerAnimated(true)
            return
        }

        SVProgressHUD.showWithStatus("Processing...")
        Cart.CheckAvailability(Cart.getStoreId(), day: selectedDay, time: textFIeldTime.text!) { (success, error) -> Void in
            if(success){
                SVProgressHUD.dismiss()
                self.delegate.didFinishSelectingDeliveryTime(self.selectedDay, delvryTime: self.textFIeldTime.text!)
                self.navigationController?.popViewControllerAnimated(true)

            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    //MARK:- TIME PICKER ACTION
    @IBAction func onTimePickerValueChanged (sender : UIDatePicker){
         let formatter : NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "hh:mm a"
        textFIeldTime.text = formatter.stringFromDate(sender.date)
    }
    
    //MARK:- TOOLBAR DONE BUTTON ACTION
    @IBAction func onDoneToolbarButtonPressed (){
        textFIeldTime.resignFirstResponder()
    }
}

//MARK:- TEXTFIELD DELEGATE
extension DeliveryTimeViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        let formatter : NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "hh:mm a"
        textFIeldTime.text = formatter.stringFromDate(timePicker.date)
    }
}
