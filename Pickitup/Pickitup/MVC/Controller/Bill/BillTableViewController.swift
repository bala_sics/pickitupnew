//
//  BillTableViewController.swift
//  Pickitup
//
//  Created by Bala on 21/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class BillTableViewController: UITableViewController {

    var orderId : String!
    var orderDate : String!
    var arrayBill : NSMutableArray = NSMutableArray()
    @IBOutlet var labelDate : UILabel!
    @IBOutlet var labelSubtotal : UILabel!
    @IBOutlet var labelShippingCost : UILabel!
    @IBOutlet var labelDeliveryTip : UILabel!
    @IBOutlet var labelTotal : UILabel!
    @IBOutlet var arrayImageView : [UIImageView]!
    @IBOutlet var labelRank : UILabel!
    @IBOutlet var viewStarRating : UIView!
    
    var selectedStar : NSInteger!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Bill"
        viewStarRating.hidden = Driver.currentPerson().userId != nil
        //if Driver.currentPerson().userId != nil { self.tableView.tableFooterView = nil }
        fetchBillDetails()
        for imagevW in arrayImageView{
            let gesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BillTableViewController.imageViewTapped(_:)))
            gesture.numberOfTapsRequired = 1
            imagevW.addGestureRecognizer(gesture)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- STAR RATING ACTION
    func imageViewTapped (sender : UITapGestureRecognizer){
            selectedStar = sender.view!.tag + 1
           labelRank.text = "You have been ranked \(selectedStar) stars"

            (arrayImageView as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
                let imageViewObj : UIImageView = obj as! UIImageView
                imageViewObj.userInteractionEnabled = false
                imageViewObj.image = UIImage(named: imageViewObj.tag < self.selectedStar ? "StarSelected" : "StarUnselected")
        }
        rateDriverWithJson(selectedStar)
    }
    
    func setRating(count : NSString)  {
        labelRank.text = "You have been ranked \(count.integerValue) stars"
            (arrayImageView as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
                let imageViewObj : UIImageView = obj as! UIImageView
                imageViewObj.userInteractionEnabled =  count.integerValue == 0
                imageViewObj.image = UIImage(named: imageViewObj.tag < count.integerValue ? "StarSelected" : "StarUnselected")
            }
    }
    
    //MARK:- RATE DRIVER WITH JSON
    func rateDriverWithJson(count : NSInteger) {
        SVProgressHUD.showWithStatus("Processing...")
        Bill.rateDriver(orderId, rating: "\(count)", completion: { (success, error) in
            if(success){
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        })
    }

    //MARK:- FETCH BILL DETAILS
    func fetchBillDetails()  {
        SVProgressHUD.showWithStatus("Processing...")
        Bill.billDetails(orderId, userid: Driver.currentPerson().userId == nil ? Person.currentPerson()
            .userId: "") { (success, result, rating, error) in
            if(success){
                self.arrayBill.removeAllObjects()
                self.arrayBill.addObjectsFromArray(result as! [AnyObject])
                self.tableView.reloadData()
                self.setRating(rating)
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    //MARK:- TABLEVIEW DELEGATES
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayBill.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")
        
        let billobj : Bill = arrayBill[indexPath.row] as! Bill
        let labelQuantity : UILabel = cell?.viewWithTag(10) as! UILabel
        labelQuantity.text = "x\(billobj.billQuantity)"
        let labelName : UILabel = cell?.viewWithTag(11) as! UILabel
        labelName.text = billobj.billItemName
        let labelPrice : UILabel = cell?.viewWithTag(12) as! UILabel
        labelPrice.text = "\(billobj.billItemPrice)$"
        
        labelDate.text = "\(orderDate), \(billobj.billShopName)"
        labelSubtotal.text = "\(billobj.billSubTotal)$"
        labelShippingCost.text  = "\(billobj.billShippingCost)$"
        labelDeliveryTip.text  = "\(billobj.billTip)$"
        
        let total = Float(billobj.billSubTotal)! + Float(billobj.billShippingCost)! + Float(billobj.billTip)!
         labelTotal.text = String(format: "TOTAL: %.2f$", total)
        
        return cell!
    }
}
