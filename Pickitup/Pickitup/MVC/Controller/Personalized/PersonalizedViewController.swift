//
//  PersonalizedViewController.swift
//  Pickitup
//
//  Created by Bala on 11/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class PersonalizedViewController: UIViewController {

    @IBOutlet var tableViewObj : UITableView!
    var selectedIndex : NSIndexPath!
    var itemId : String!
    var dictDetails : NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchProducts()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchProducts()  {
        SVProgressHUD.showWithStatus("Processing...")
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["dishid" : itemId]
        let url : String = base_Url + "drinks_iphone"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            if (result["result"] as! String == "success"){
                self.dictDetails = result["details"] as! NSDictionary
                self.tableViewObj.reloadData()
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus("No items found")
            }
        }) { (operation, error) -> Void in
            SVProgressHUD.showErrorWithStatus(error.localizedDescription)
        }

    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
    }
    
    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onAddButtonPressed (sender : UIButton){
        let dict : NSDictionary = (self.dictDetails[self.dictDetails.allKeys[selectedIndex.section] as! String] as! NSArray).objectAtIndex(selectedIndex.row) as! NSDictionary
        self.addToCart("1", storeId: dict["storeid"] as! String, itemid: dict["id"] as! String)
    }
}

//MARK:- TABLE VIEW DELEGATES
extension PersonalizedViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.dictDetails.allKeys.count
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader : UIView = UIView(frame: CGRectMake(0, 0, screenSize.width, 44))
        viewHeader.backgroundColor = UIColor.whiteColor()
        
       let imageViewBackground = UIImageView(frame: CGRectMake(0, -0.7, screenSize.width, 45))
        imageViewBackground.image = UIImage(named: "BillPageBorder")
        viewHeader.addSubview(imageViewBackground)
        
        let labelItemName = UILabel(frame: CGRectMake(20, 0, screenSize.width - 40, 44))
        labelItemName.textColor = UIColor.blackColor()
        labelItemName.font = UIFont(name: "Myriad Hebrew", size: 17.0)
        labelItemName.text = self.dictDetails.allKeys[section] as? String
        viewHeader.addSubview(labelItemName)

        return viewHeader
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dictDetails[self.dictDetails.allKeys[section] as! String]!.count
            
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = PersonalizedTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        let dict : NSDictionary = self.dictDetails[self.dictDetails.allKeys[indexPath.section] as! String]?.objectAtIndex(indexPath.row) as! NSDictionary
        
        (cell as! PersonalizedTableViewCell).labelItemName.text = dict["name"] as? String
//        (cell as! PersonalizedTableViewCell).labelItemName.font = indexPath.row % 4 == 0 ? UIFont(name: "Myriad Hebrew", size: 17.0) : UIFont(name: "Myriad Hebrew", size: 15.0)
       // (cell as! PersonalizedTableViewCell).imageViewBackground.image = indexPath.row % 4 == 0 ? UIImage(named: "BillPageBorder") : nil
        if(selectedIndex != nil){
             (cell as! PersonalizedTableViewCell).viewBackground.backgroundColor = selectedIndex == indexPath  ? UIColor(red: 37.0/255.0, green: 191.0/255.0, blue: 197.0/255.0, alpha: 1.0) : UIColor.whiteColor()
            (cell as! PersonalizedTableViewCell).labelItemName.textColor = selectedIndex == indexPath  ?  UIColor.whiteColor() : UIColor.blackColor()
        }
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        if(indexPath.row % 4 == 0) { return }
        selectedIndex = indexPath
         tableViewObj.reloadData()
    }
    
    //MARK:- ADD TO CART WITH JSON
    func addToCart (quantity : String ,storeId : String , itemid : String){
        SVProgressHUD.showWithStatus("Processing...")
        Cart.addToCart(storeId, itemId: "", quantity: "", subItemId: itemid, subItemQuantity: quantity) { (success, result, error) in
            if(success){
                SVProgressHUD.dismiss()
                self.navigationController?.popViewControllerAnimated(true)
            }else{
                if(error?.localizedDescription == "Please try again"){
                    Cart.SharedInstance().totalPrice = "0"
                    Cart.SharedInstance().shippingCost = "0"
                    Cart.SharedInstance().arrayCartItems.removeAllObjects()
                    SVProgressHUD.dismiss()
                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            }

        }
   }

}