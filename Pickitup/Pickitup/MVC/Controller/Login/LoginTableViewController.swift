//
//  LoginTableViewController.swift
//  Pickitup
//
//  Created by Bala on 29/12/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginTableViewController: UITableViewController {
    
    @IBOutlet weak var textFieldUserName : UITextField!
    @IBOutlet weak var textFieldPassword : UITextField!
    var arrayHeight : [CGFloat]!
    @IBOutlet weak var buttonCreateAccount : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageVw : UIImageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
        imageVw.image = UIImage(named: "Background")
        self.tableView.backgroundView = imageVw
        
        textFieldPassword.setLeftPadding(10)
        textFieldUserName.setLeftPadding(10)
        buttonCreateAccount.layer.borderColor = UIColor.whiteColor().CGColor
        buttonCreateAccount.layer.borderWidth = 1.0

        if(IS_IPHONE6){
             arrayHeight = [55,55,50,82,81]
        }else if(IS_IPHONE6PLUS){
             arrayHeight = [55,55,50,82,85]
        }else{
            arrayHeight = [55,55,50,82,76]
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- FACEBOOK CONNECT BUTTON ACTION
    @IBAction func onFacebookConnectButtonPressed (sender : UIButton){
        let login : FBSDKLoginManager = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.Browser
        login.logOut()
        login.logInWithReadPermissions(["public_profile", "email"], fromViewController: self) { (result, error) -> Void in
            if(error == nil && result.isCancelled == false){
                SVProgressHUD.showWithStatus("Processing...")
                let request = FBSDKGraphRequest(graphPath:"me", parameters:["fields" : "id, name, email"])
                request.startWithCompletionHandler {
                    (connection, result, error) in
                    if error != nil {
                        SVProgressHUD.dismiss()
                    }
                    else if let userData = result as? [String:AnyObject] {
                        print("\(userData)")
                        self.facebookLoginWithJson(userData)
                    }
                }
            }else{
                //SVProgressHUD.showErrorWithStatus("Login Failed")
            }
        }
    }
    
    //MARK:- FORGOT PASSWORD BUTTON ACTION
    @IBAction func onForgotPasswordPressed (sender : UIButton){
        let forgot : ForgotPasswordViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ForgotVC") as! ForgotPasswordViewController
        forgot.hideLabelBottom = true
        self.navigationController?.pushViewController(forgot, animated: true)
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return arrayHeight[indexPath.row]
    }
    
    //MARK:- FACEBOOK LOGIN WITH JSON
    func facebookLoginWithJson (details : NSDictionary){
        Person.facebookLoginWithUserDetails(CheckForNull(strToCheck: details.checkValueForKey(key: "name") as? String), email: CheckForNull(strToCheck: details.checkValueForKey(key: "email") as? String), fbId: CheckForNull(strToCheck: details.checkValueForKey(key: "id") as? String)) { (success, result, error) -> Void in
            if(success){
                SVProgressHUD.dismiss()
                AppDelegate.getAppdelegateInstance().setHomePage()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    //MARK:- LOGIN WITH JSON
    func loginWithJson (){
        SVProgressHUD.showWithStatus("Processing...")
        Person.loginWithUserDetails(textFieldUserName.text!, password: textFieldPassword.text!) { (success, result, error) -> Void in
            if(success){
                SVProgressHUD.dismiss()
//                if(Person.currentPerson().verificationStatus == "Verified"){
                    AppDelegate.getAppdelegateInstance().setHomePage()
//                }else{
//                    let mobile : VerificationTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VerificationVC") as! VerificationTableViewController
//                    self.navigationController?.pushViewController(mobile, animated: true)
//                }
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
}

//MARK:- TEXTFIELD DELEGATE
extension LoginTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == textFieldUserName){
            textFieldPassword.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
            
            if(textFieldPassword.text == "" || textFieldUserName.text == ""){
                UIAlertView(title: "Warning", message: "Please fill all fields", delegate: nil, cancelButtonTitle: "Ok").show()
            }else {
                loginWithJson()
            }
            
        }
        return true
    }
}