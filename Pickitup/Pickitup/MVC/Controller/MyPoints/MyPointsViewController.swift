//
//  MyPointsViewController.swift
//  Pickitup
//
//  Created by Bala on 20/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class MyPointsViewController: UIViewController {

    @IBOutlet var imageviewProfile : UIImageView!
    @IBOutlet var labelBottom : UILabel!
    @IBOutlet var labelPoints : UILabel!
    @IBOutlet var labelUsername : UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "My Points"
        imageviewProfile.layer.cornerRadius = 75.0
        imageviewProfile.clipsToBounds = true
        
        labelBottom.text = "Get free delivery at 2000 points. \nYou earn 50 points for each order."
        fetchPoints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func fetchPoints()  {
        SVProgressHUD.showWithStatus("Processing...")
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["user_id" : Person.currentPerson().userId]
        let url : String = base_Url + "my_points"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            if  (result["result"] as! String == "success"){
                let detailsDict : NSDictionary = (result["details"] as! NSArray).objectAtIndex(0) as! NSDictionary
                self.labelPoints.text = "\(detailsDict["points"] as! String) points"
                self.labelUsername.text = detailsDict["username"] as? String
            }else{
                
            }
            SVProgressHUD.dismiss()
        }) { (operation, error) -> Void in
            SVProgressHUD.showErrorWithStatus(error.localizedDescription)
        }
        

    }

}
