//
//  CreditCardTableViewController.swift
//  Pickitup
//
//  Created by Bala on 08/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class CreditCardTableViewController: UITableViewController {

    var buttonRemove : UIButton!
    var arraySelectedButtons : NSMutableArray = NSMutableArray()
    var arraySelectedCards : NSMutableArray = NSMutableArray()
    var arrayCards : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonRemove = UIButton(type: UIButtonType.Custom)
        buttonRemove.frame = CGRectMake(0, screenSize.height, screenSize.width, 50)
        buttonRemove.backgroundColor = UIColor(red: 242.0/255.0, green: 63.0/255.0, blue: 62.0/255.0, alpha: 1.0)
        buttonRemove.setTitle("Remove", forState: UIControlState.Normal)
        buttonRemove.titleLabel?.font = UIFont(name: "Myriad Hebrew", size: 18.0)
        buttonRemove.addTarget(self, action: #selector(CreditCardTableViewController.onRemoveButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        fetchCards()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.arraySelectedButtons.enumerateObjectsUsingBlock { (obj, idx, Stop) -> Void in
            (obj as! UIButton).selected = false
            (obj as! UIButton).backgroundColor = UIColor.darkGrayColor()
        }
        self.tableView.reloadData()
        self.arraySelectedCards.removeAllObjects()
        UIView.animateWithDuration(0.4, animations: { () -> Void in
            self.buttonRemove.frame = CGRectMake(0, screenSize.height, screenSize.width, 50)
            }, completion: { (success) -> Void in
                self.buttonRemove.removeFromSuperview()
        })
    }
    
    func fetchCards (){
        SVProgressHUD.showWithStatus("Processing...")
        CreditCard.fetchCards { (success, result, error) -> Void in
            if(success){
                self.arrayCards.removeAllObjects()
                self.arrayCards.addObjectsFromArray(result as! [AnyObject])
                self.tableView.reloadData()
                SVProgressHUD.dismiss()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCards.count
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = CreditCardTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }

        let card : CreditCard = arrayCards[indexPath.row] as! CreditCard
        (cell as! CreditCardTableViewCell).buttonDelete.tag = indexPath.row
        (cell as! CreditCardTableViewCell).buttonDelete.addTarget(self, action: #selector(CreditCardTableViewController.onDeleteButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        let index1 = card.creditCardNumber.endIndex.advancedBy(-3)
        let number = card.creditCardNumber.substringFromIndex(index1)
        let index2 = card.creditCardName.endIndex.advancedBy(-card.creditCardName.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)/2)
        let name = card.creditCardName.substringToIndex(index2)

         (cell as! CreditCardTableViewCell).labelCard.text = "  \(name)***  #############\(number)"
        return cell!
        
    }
    
    func onDeleteButtonPressed (sender : UIButton){
        let card : CreditCard = arrayCards[sender.tag] as! CreditCard
        if(sender.selected){
            sender.backgroundColor = UIColor.darkGrayColor()
            arraySelectedButtons.removeObject(sender)
            arraySelectedCards.removeObject(card.creditCardId)
        }else{
            sender.backgroundColor = UIColor(red: 242.0/255.0, green: 63.0/255.0, blue: 62.0/255.0, alpha: 1.0)
            arraySelectedButtons.addObject(sender)
            arraySelectedCards.addObject("\(card.creditCardId)")
        }
        sender.selected = !sender.selected
        if(arraySelectedCards.count == 0){
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                self.buttonRemove.frame = CGRectMake(0, screenSize.height, screenSize.width, 50)
                }, completion: { (success) -> Void in
                    self.buttonRemove.removeFromSuperview()
            })
        }else{
            AppDelegate.getAppdelegateInstance().window!.addSubview(buttonRemove)
            UIView.animateWithDuration(0.4, animations: { () -> Void in
                self.buttonRemove.frame = CGRectMake(0, screenSize.height - 50, screenSize.width, 50)
                }, completion: { (success) -> Void in
            })

        }
    }
    
    func onRemoveButtonPressed (){
        SVProgressHUD.showWithStatus("Processing...")
        CreditCard.deleteCards(arraySelectedCards.componentsJoinedByString(",")) { (success, error) -> Void in
            if(success){
                self.fetchCards()
                self.arraySelectedButtons.enumerateObjectsUsingBlock { (obj, idx, Stop) -> Void in
                    (obj as! UIButton).selected = false
                    (obj as! UIButton).backgroundColor = UIColor.darkGrayColor()
                }
                self.arraySelectedCards.removeAllObjects()
                UIView.animateWithDuration(0.4, animations: { () -> Void in
                    self.buttonRemove.frame = CGRectMake(0, screenSize.height, screenSize.width, 50)
                    }, completion: { (success) -> Void in
                        self.buttonRemove.removeFromSuperview()
                })

            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
        
    }
}
