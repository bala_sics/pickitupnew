//
//  VerificationTableViewController.swift
//  Pickitup
//
//  Created by Bala on 29/12/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class VerificationTableViewController: UITableViewController {
    
    @IBOutlet weak var textFieldCode : UITextField!
    @IBOutlet weak var toolBar : UIToolbar!
    @IBOutlet weak var buttonNext : UIButton!
    @IBOutlet weak var LabelText : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageVw : UIImageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
        imageVw.image = UIImage(named: "Background")
        self.tableView.backgroundView = imageVw
        
        textFieldCode.inputAccessoryView = toolBar
        
        self.title = "Pickitup"
        self.navigationItem.hidesBackButton = true

        buttonNext.layer.borderColor = UIColor.whiteColor().CGColor
        buttonNext.layer.borderWidth = 1.0
        LabelText.text = "We just sent a code to your email \n Please enter the code below"

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- SEND ME AGAIN BUTTON ACTION
    @IBAction func onSendMeAgainButtonPressed (sender : UIButton){
        SVProgressHUD.showWithStatus("Sending verification Code...")
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["email" : Person.currentPerson().email , "user_id" : Person.currentPerson().userId]
        let url : String = base_Url + "resend_email_varification"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? SVProgressHUD.showSuccessWithStatus("Email sent") : SVProgressHUD.showErrorWithStatus("Please try again")
            }) { (operation, error) -> Void in
                SVProgressHUD.showErrorWithStatus(error.localizedDescription)
        }
    }
    
    //MARK:- DONE TOOLBAR BUTTON ACTION
    @IBAction func onDoneButtonPressed (){
        textFieldCode.resignFirstResponder()
    }
    
    //MARK:- CANCEL BUTTON ACTION
    @IBAction func onCancelButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- NEXT BUTTON ACTION
    @IBAction func onNextButtonPressed (sender : UIButton){
        if(textFieldCode.text == ""){
            UIAlertView(title: "Warning", message: "Please enter the valid verification code", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
            SVProgressHUD.showWithStatus("Verifying...")
            let manager  = AFHTTPRequestOperationManager()
            manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
            let params : NSDictionary = ["email" : Person.currentPerson().email , "verification_code" : textFieldCode.text!]
            let url : String = base_Url + "email_varification"
            manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
                let result : NSDictionary = responseObject as! NSDictionary
                (result["result"] as! String == "success") ? AppDelegate.getAppdelegateInstance().setHomePage() : SVProgressHUD.showErrorWithStatus("Invalid verification code")
                }) { (operation, error) -> Void in
                    SVProgressHUD.showErrorWithStatus(error.localizedDescription)
            }
        }
    }
}

//MARK:- TEXTFIELD DELEGATE
extension VerificationTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}