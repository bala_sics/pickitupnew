//
//  MobileNumberTableViewController.swift
//  Pickitup
//
//  Created by Bala on 29/12/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class MobileNumberTableViewController: UITableViewController {
    
    @IBOutlet weak var textFieldPhone : UITextField!
    @IBOutlet weak var toolBar : UIToolbar!
     @IBOutlet weak var buttonNext : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageVw : UIImageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
        imageVw.image = UIImage(named: "Background")
        self.tableView.backgroundView = imageVw
        
        textFieldPhone.inputAccessoryView = toolBar
        textFieldPhone.setLeftPadding(10)
        
        self.title = "Pickitup"
        self.navigationItem.hidesBackButton = true
        
        buttonNext.layer.borderColor = UIColor.whiteColor().CGColor
        buttonNext.layer.borderWidth = 1.0


        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- DONE TOOLBAR BUTTON ACTION
    @IBAction func onDoneButtonPressed (){
        textFieldPhone.resignFirstResponder()
    }
    
    //MARK:- CANCEL BUTTON ACTION
    @IBAction func onCancelButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
}

//MARK:- TEXTFIELD DELEGATE
extension MobileNumberTableViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}