//
//  ScanContainerViewController.swift
//  Pickitup
//
//  Created by Bala on 10/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class ScanContainerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
        
    }


    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
  
}

