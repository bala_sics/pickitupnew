//
//  ScanCardTableViewController.swift
//  Pickitup
//
//  Created by Bala on 10/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation
import CoreMedia
import CoreVideo
import MobileCoreServices

class ScanCardTableViewController: UITableViewController {

    @IBOutlet var arrayTextFields : [UITextField]!
    @IBOutlet var toolbar : UIToolbar!
    var activeTextfield : UITextField!
    var arrayCardsType : [String] = ["American Express","Visa","MasterCard","Discover","DinersClub","JCB"]

    override func viewDidLoad() {
        super.viewDidLoad()
         CardIOUtilities.preload()
        (arrayTextFields as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            (obj as! UITextField).setLeftPadding(10)
            if(obj.tag != 4 && obj.tag != 5 ){
                (obj as! UITextField).inputAccessoryView = self.toolbar
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onScanCardButtonPressed (sender : UIButton){
        let cardIOVC = CardIOPaymentViewController(paymentDelegate: self)
        cardIOVC.modalPresentationStyle = .FormSheet
        presentViewController(cardIOVC, animated: true, completion: nil)
    }
    
    @IBAction func onSaveThisCardButtonPressed (sender : UIButton){
        if(!verifyFields()){
            UIAlertView(title: "Warning", message: "Please fill all the fields", delegate: nil, cancelButtonTitle: "Ok").show()
        }else if(!Luhn.validateString(arrayTextFields[0].text!)){
            UIAlertView(title: "Warning", message: "Please enter the valid card number", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
            if(activeTextfield != nil) { activeTextfield.resignFirstResponder() }
            saveCardWithJson()
        }

    }
    
    //MARK:- SAVE CREDIT CARD WITH JSON
    func saveCardWithJson (){
        SVProgressHUD.showWithStatus("Processing...")
        let type : OLCreditCardType = Luhn.typeFromString(arrayTextFields[0].text!)
        let cardType : String = type.hashValue < arrayCardsType.count ? arrayCardsType[type.hashValue] : "Invalid"

        CreditCard.addCreditCard(arrayTextFields[0].text!, expmonth: arrayTextFields[1].text!, expyear: arrayTextFields[2].text!, name: arrayTextFields[4].text!, lastName: arrayTextFields[5].text!, ZipcCode: "", securitycode: arrayTextFields[3].text!, cardType: cardType) { (success, result, error) -> Void in
            if(success){
                SVProgressHUD.showSuccessWithStatus("Saved successfully")
                self.navigationController?.popViewControllerAnimated(true)
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }

    func verifyFields () -> Bool{
        var success : Bool = true
        (arrayTextFields as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            if((obj as! UITextField).text == ""){
                success = false
            }
        }
        return success
    }

    @IBAction func onDoneButtonPressed (sender : UIButton){
        activeTextfield.resignFirstResponder()
    }
}
extension ScanCardTableViewController : CardIOPaymentViewControllerDelegate {
    func userDidCancelPaymentViewController(paymentViewController: CardIOPaymentViewController!) {
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func userDidProvideCreditCardInfo(cardInfo: CardIOCreditCardInfo!, inPaymentViewController paymentViewController: CardIOPaymentViewController!) {
        if let info = cardInfo {
            arrayTextFields[0].text = info.cardNumber
            arrayTextFields[1].text = "\(info.expiryMonth)"
            arrayTextFields[2].text = "\(info.expiryYear)"
            arrayTextFields[3].text = info.cvv
        }
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }  
}

extension ScanCardTableViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextfield = textField
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}