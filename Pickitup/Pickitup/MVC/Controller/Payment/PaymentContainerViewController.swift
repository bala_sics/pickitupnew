//
//  PaymentContainerViewController.swift
//  Pickitup
//
//  Created by Bala on 10/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class PaymentContainerViewController: UIViewController {
    var dictDetails : NSMutableDictionary!
    var infrmtionOrder : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
        
    }

    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
  
    //MARK:- PROCEED TO PAYMENT BUTTON ACTION
    @IBAction func onProceedToPaymentButtonPressed (){
        let paymnt : PaymentTableViewController = self.childViewControllers[0] as! PaymentTableViewController
        if(verifyFields() == false){
            UIAlertView(title: "Warning", message: "Please fill your personal information", delegate: nil, cancelButtonTitle: "Ok").show()
        }
//        else if(paymnt.selectedCreditCardId == ""){
//            UIAlertView(title: "Warning", message: "Please select any credit card for payment", delegate: nil, cancelButtonTitle: "Ok").show()
//        }
        else{
            paymnt.onPayPalButtonPressed(UIButton())
          //  placeOrderWithJson()
        }
    }
    
    func placeOrderWithJson (){
        SVProgressHUD.showWithStatus("Processing")
        let paymnt : PaymentTableViewController = self.childViewControllers[0] as! PaymentTableViewController
        dictDetails.setValue(paymnt.selectedCreditCardId, forKey: "creditcardId")
        dictDetails.setValue(paymnt.arrayTextFields[0].text!, forKey: "name")
        dictDetails.setValue(paymnt.arrayTextFields[1].text!, forKey: "streetname")
        dictDetails.setValue(paymnt.arrayTextFields[3].text!, forKey: "apartmentNo")
        dictDetails.setValue(paymnt.arrayTextFields[4].text!, forKey: "Floor")
        dictDetails.setValue(paymnt.arrayTextFields[5].text!, forKey: "City")
        dictDetails.setValue(paymnt.arrayTextFields[2].text!, forKey: "N")
        dictDetails.setValue(paymnt.arrayTextFields[6].text!, forKey: "State")
        dictDetails.setValue(paymnt.arrayTextFields[7].text!, forKey: "country")
        dictDetails.setValue(paymnt.arrayTextFields[8].text!, forKey: "ZipCode")
        dictDetails.setValue(Cart.getStoreId(), forKey: "storeId")
        dictDetails.setValue(infrmtionOrder, forKey: "information")


        Cart.proceedToPayment(dictDetails, completion: { (success, error) -> Void in
            if(success){
                SVProgressHUD.dismiss()
                Cart.SharedInstance().arrayCartItems.removeAllObjects()
                Cart.SharedInstance().totalPrice = "0"
                Cart.SharedInstance().shippingCost = "0"
                self.navigationController?.popToRootViewControllerAnimated(true)
                
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        })
    }
    
    //MARK:- VERIFYING TEXTFIELD IS EMPTY OR NOT
    func verifyFields() -> Bool
    {
        let paymnt : PaymentTableViewController = self.childViewControllers[0] as! PaymentTableViewController
        var success : Bool = true
        (paymnt.arrayTextFields as NSArray).enumerateObjectsUsingBlock({ (textfield , idx, stop) -> Void in
            let textfieldObj = textfield as! UITextField
            if (textfieldObj.text!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0)
            {
                success = false
            }
        })
        return success
    }
}
