//
//  PaymentTableViewController.swift
//  Pickitup
//
//  Created by Bala on 02/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class PaymentTableViewController: UITableViewController,PayPalPaymentDelegate {
    
    @IBOutlet var arrayTextFields : [UITextField]!
    @IBOutlet var toolbar : UIToolbar!
    var activeTextfield : UITextField!
    @IBOutlet var arrayTableViewCells : [UITableViewCell]!
    var arrayCellsToLoad :NSMutableArray = NSMutableArray()
    var buttonProceedToPayment : UIButton!
    var arrayCards : NSMutableArray = NSMutableArray()
    var selectedCardButton : UIButton = UIButton()
    var selectedCreditCardId : String = ""
    
    var payPalConfig = PayPalConfiguration() // default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (arrayTextFields as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            (obj as! UITextField).setLeftPadding(10)
            if(idx == 2 || idx == 8 ){
                (obj as! UITextField).inputAccessoryView = self.toolbar
            }
        }
        //
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = true;
        payPalConfig.merchantName = "Pickitup"
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0]
        payPalConfig.payPalShippingAddressOption = .PayPal;

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        PayPalMobile.preconnectWithEnvironment(PayPalEnvironmentNoNetwork)
        fetchCards()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- FETCH SAVED CARDS
    func fetchCards (){
        SVProgressHUD.showWithStatus("Processing...")
        self.arrayCellsToLoad.removeAllObjects()
        self.arrayCellsToLoad.addObjectsFromArray(self.arrayTableViewCells)

        CreditCard.fetchCards { (success, result, error) -> Void in
            if(success){
                self.arrayCards.removeAllObjects()
                self.arrayCards.addObjectsFromArray(result as! [AnyObject])
                self.addCreditCardCells()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK:- ADD CREDIT CARD CELL
    func addCreditCardCells (){
        self.arrayCards.enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let cell : CreditCardTableViewCell = CreditCardTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            cell.contentView.backgroundColor = UIColor.clearColor()
            cell.backgroundColor = UIColor.clearColor()
            cell .buttonDelete.addTarget(self, action: #selector(PaymentTableViewController.onDeleteButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.buttonDelete.tag = idx
            cell.buttonDelete.backgroundColor = UIColor.whiteColor()
            cell.buttonDelete.layer.borderWidth = 1.0
            cell.buttonDelete.layer.borderColor = UIColor.lightGrayColor().CGColor
            cell.labelCard.layer.cornerRadius = 3.0
            cell.labelCard.layer.borderWidth = 1.0
            cell.labelCard.layer.borderColor = UIColor.lightGrayColor().CGColor
            
            let card : CreditCard = obj as! CreditCard
            let index1 = card.creditCardNumber.endIndex.advancedBy(-3)
            let number = card.creditCardNumber.substringFromIndex(index1)
            let index2 = card.creditCardName.endIndex.advancedBy(-card.creditCardName.lengthOfBytesUsingEncoding(NSUTF8StringEncoding)/2)
            let name = card.creditCardName.substringToIndex(index2)
            cell.labelCard.text = "  \(name)***  #############\(number)"

            self.arrayCellsToLoad.addObject(cell)
        }
        self.tableView.reloadData()
        SVProgressHUD.dismiss()
    }

    //MARK:- BUTTON ACTIONS
    //MARK:- DELETE BUTTON ACTION IN CREDIT CARD CELL
    func onDeleteButtonPressed (sender : UIButton){
        selectedCardButton.selected = false
        selectedCardButton.backgroundColor = UIColor.whiteColor()
        selectedCardButton = sender
        let card : CreditCard = arrayCards[sender.tag] as! CreditCard
        selectedCreditCardId = card.creditCardId
        if(sender.selected){
            sender.backgroundColor = UIColor.whiteColor()
        }else{
            sender.backgroundColor = UIColor.lightGrayColor()
        }
        sender.selected = !sender.selected
    }

    //MARK:- DONE BUTTON ACTION
    @IBAction func onDoneButtonPressed (sender : UIButton){
        activeTextfield.resignFirstResponder()
    }
    
    //MARK:- VERIFYING TEXTFIELD IS EMPTY OR NOT
    func verifyFields() -> Bool
    {
        var success : Bool = true
        (arrayTextFields as NSArray).enumerateObjectsUsingBlock({ (textfield , idx, stop) -> Void in
            let textfieldObj = textfield as! UITextField
            if (textfieldObj.text!.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0 )
            {
                success = false
            }
        })
        return success
    }

    
    //MARK:- PAYPAL BUTTON ACTION
    @IBAction func onPayPalButtonPressed (sender : UIButton){
        if(verifyFields() == false){
            UIAlertView(title: "Warning", message: "Please fill your personal information", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
        let items : NSMutableArray = NSMutableArray()
        Cart.SharedInstance().arrayCartItems.enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let cart : Cart = obj as! Cart
            let item : PayPalItem = PayPalItem(name: cart.cartItemName, withQuantity: UInt(cart.cartItemQuantity)!, withPrice: NSDecimalNumber(string: cart.cartItemPrice), withCurrency: "USD", withSku: cart.cartItemId)
            items.addObject(item)
        }
        
        let subtotal = PayPalItem.totalPriceForItems(items as [AnyObject])
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Pickitup", intent: .Sale)
        
        payment.items = items as [AnyObject]
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController : PayPalPaymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)!
            presentViewController(paymentViewController, animated: true, completion: nil)
        }
        else {
//            print("Payment not processalbe: \(payment)")
            SVProgressHUD.showErrorWithStatus("Payment not processable")
        }
        }

    }
    
    @IBAction func onAddCreditCardButtonPressed(sender : UIButton){
        let scan : ScanContainerViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ScanVC") as! ScanContainerViewController
        self.navigationController?.pushViewController(scan, animated: true)
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  indexPath.row == 6 ? 80 : indexPath.row == 7 ? 60 : indexPath.row == 8 ? 20 : 40
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCellsToLoad.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return arrayCellsToLoad[indexPath.row] as! UITableViewCell
    }
   
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        paymentViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController, didCompletePayment completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismissViewControllerAnimated(true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            (self.parentViewController as! PaymentContainerViewController).placeOrderWithJson()
        })
    }


}
extension PaymentTableViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextfield = textField
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}