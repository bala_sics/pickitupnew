//
//  FollowMyDriverViewController.swift
//  Pickitup
//
//  Created by Bala on 07/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
import MapKit

class FollowMyDriverViewController: UIViewController {
    @IBOutlet var mapViewObj : MKMapView!
    @IBOutlet weak var viewDot : UIView!
    @IBOutlet weak var labelTime : UILabel!
    @IBOutlet weak var viewTop : UIView!
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var viewNoOrder : UIView!
    @IBOutlet weak var labelName : UILabel!
    var arrayDrivers : NSMutableArray = NSMutableArray()
    var isFirstTime : Bool = true
    var annotation : BMAnnotation!
    var selectedIndex : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        mapViewObj.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
        viewDot.layer.cornerRadius = 15.0
        labelTime.layer.cornerRadius = 5.0
        labelTime.clipsToBounds = true
        viewTop.backgroundColor = UIColor(red: 38.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 0.9)
        viewBottom.backgroundColor = UIColor(red: 38.0/255.0, green: 37.0/255.0, blue: 37.0/255.0, alpha: 0.9)
        fetchOrderDetails()
        
        let rightButton : UIBarButtonItem = UIBarButtonItem(title: "Order from", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(FollowMyDriverViewController.selectOrderButtonAction))
        self.navigationItem.rightBarButtonItem = rightButton
        
    }

    //MARK:- GPS BUTTON ACTION
    @IBAction func onGpsbuttonPressed (sender : UIButton){
        mapViewObj.setUserTrackingMode(MKUserTrackingMode.Follow, animated: true);
   }
    
    //MARK:- COMPLETE ORDER BUTTON ACTION
    @IBAction func onCompleteOrderButtonPressed (sender : UIButton){
        if(arrayDrivers.count == 0) { return }
        SVProgressHUD.showWithStatus("Processing...")
        let driver : DriverDetails = arrayDrivers[selectedIndex] as! DriverDetails
        Orders.completeOrder(driver.driverid, orderid: driver.driverOrderId) { (success, error) in
            if(success){
                SVProgressHUD.dismiss()
                 self.mapViewObj.removeAnnotation(self.annotation)
                self.labelTime.text = "00:00"
                self.arrayDrivers.removeAllObjects()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchOrderDetails()  {
        if(isFirstTime) { SVProgressHUD.showWithStatus("Processing...") }
        DriverDetails.myDriverDetails { (success, result, error) in
            if(success){
                self.arrayDrivers.removeAllObjects()
                self.arrayDrivers.addObjectsFromArray(result as! [AnyObject])
                self.addAnnotation()
                self.viewNoOrder.hidden = true
                self.isFirstTime = false
                SVProgressHUD.dismiss()
            }else{
                
                if(self.isFirstTime) { SVProgressHUD.showErrorWithStatus(error?.localizedDescription) }
                if(error?.localizedDescription == "No drivers found"){
                self.viewNoOrder.hidden = false
                self.isFirstTime = false
                }
            }
            self.fetchOrderDetails()
        }
    }

    func addAnnotation(){
        let driver : DriverDetails = arrayDrivers[selectedIndex] as! DriverDetails
        let annotation1 = BMAnnotation()
        annotation1.coordinate = CLLocationCoordinate2DMake(Double(driver.driverLat)!, Double(driver.driverLong)!)
        annotation1.imageName = "Red"
        self.mapViewObj.addAnnotation(annotation1)
        labelName.text = driver.driverName
        if(annotation != nil) {self.mapViewObj.removeAnnotation(annotation)}
        annotation = annotation1
        
        if(driver.driverDeliveredTime != ""){
            let formatter : NSDateFormatter = NSDateFormatter()
            formatter.dateFormat = "hh:mm a"
            let date : NSDate = formatter.dateFromString(driver.driverDeliveredTime)!
            formatter.dateFormat = "HH:mm"
            let dateStr : String = formatter.stringFromDate(date)
            labelTime.text = CheckForNull(strToCheck: dateStr)
        }

    }
    
    func selectOrderButtonAction(){
        if arrayDrivers.count != 0 {
            let dropDownView : BMOrderDropdown = BMOrderDropdown()
            dropDownView.arrayToLoad = arrayDrivers
            dropDownView.delegate = self
            dropDownView.showDropDown(dropDownFrame: CGRectMake(self.view.frame.size.width - 150, 44, 150, 0), heightOfDropDown: 88.0)
        }
    }
}

extension FollowMyDriverViewController : MKMapViewDelegate{
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is BMAnnotation) {
            return nil
        }
        
        let reuseId = "Annotation"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = true
        }
        else {
            anView!.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let cpa = annotation as! BMAnnotation
        anView!.image = UIImage(named:cpa.imageName)
        
        return anView
    }
}

extension FollowMyDriverViewController : bmOrderDropDownDelegate{
    func selectedValueFromBMDropDowm(index: Int) {
        selectedIndex = index
    }
}
