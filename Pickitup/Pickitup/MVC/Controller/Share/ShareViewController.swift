//
//  ShareViewController.swift
//  Pickitup
//
//  Created by Bala on 07/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
import FBSDKShareKit
import MessageUI

class ShareViewController: UIViewController,UIDocumentInteractionControllerDelegate {

    var instagram = MGInstagram()
    var mailComposer : MFMailComposeViewController!
    var kClientId = "969326877147-svafkv95d7nuuqt889k30dtirmq5fmdt.apps.googleusercontent.com"; // Get this from https://console.developers.google.com
    var kShareURL = "http://www.srishtis.com/";
    var documentController: UIDocumentInteractionController!
    var shareConfiguration : ShareConfiguration!
    
    func gppinit()  {
        shareConfiguration = ShareConfiguration.sharedInstance()
        GPPShare.sharedInstance().delegate = self

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        gppinit()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

         self.navigationItem.title = "SHARE"
        ////Twitter Login
        FHSTwitterEngine.sharedEngine().permanentlySetConsumerKey("aCUwPORfep0WbAk1AcGo10h9l", andSecret: "6UoosrHr4Uq2os50DwHyb5UB1xyUu0vtkIuyn1EK55cqmOGpTW")
        FHSTwitterEngine.sharedEngine().delegate = self
        FHSTwitterEngine.sharedEngine().loadAccessToken()

        mailComposer  = MFMailComposeViewController()
        
        let signIn = GPPSignIn.sharedInstance();
        signIn.shouldFetchGooglePlusUser = true;
        signIn.clientID = kClientId;
        signIn.shouldFetchGoogleUserEmail = true;
        signIn.shouldFetchGoogleUserID = true;
        signIn.scopes = [kGTLAuthScopePlusLogin];
        signIn.trySilentAuthentication();
        signIn.delegate = self;
        shareConfiguration.useNativeSharebox = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    //MARK:- BUTTON ACTIONS
    //MARK:- CANCEL BUTTON ACTION
    @IBAction func onCancelButtonPressed (sender : UIButton){
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    //MARK:- FACEBOOK SHARE
    @IBAction func onFacebookShareButtonPressed (sender : UIButton){
        let content = FBSDKShareLinkContent()
        
        content.contentURL = NSURL(string: "http://stackoverflow.com/questions/7545254")

        let dialog : FBSDKShareDialog = FBSDKShareDialog()
        dialog.fromViewController = self
        dialog.shareContent = content
        dialog.mode = .Native
        if dialog.canShow() == false {
            dialog.mode = .Browser
        }
        dialog.show()
    }
    
    //MARK:- TWITTER SHARE
    @IBAction func onTwitterShareButtonPressed (sender : UIButton){
        
        let vc : UIViewController = FHSTwitterEngine.sharedEngine().loginControllerWithCompletionHandler { (success) in
            if(success){
                FHSTwitterEngine.sharedEngine().postTweet("Posted via pickitup")

            }else{
                UIAlertView(title: "Error", message: "Twitter login failed", delegate:nil, cancelButtonTitle: "ok").show()
            }
        }
        self.presentViewController(vc, animated: true, completion: nil)
                
    }
    
    //MARK:- LINKEDIN SHARE
    @IBAction func onLinkedinShareButtonPresssed (sender : UIButton){
        let permissions : NSArray = NSArray(objects: LISDK_BASIC_PROFILE_PERMISSION,LISDK_W_SHARE_PERMISSION)
        LISDKSessionManager.createSessionWithAuth(permissions as [AnyObject], state: nil, showGoToAppStoreDialog: true, successBlock: { (returnState) in
            if (LISDKSessionManager.hasValidSession() == true){
                let url : NSString =  NSString(format: "https://api.linkedin.com/v1/people/~/shares")
                let payload : NSString = NSString(format: "{\"comment\":\"Posted via pickitup\",\"visibility\":{ \"code\":\"anyone\" }}")
                LISDKAPIHelper.sharedInstance().postRequest(url as String, stringBody: payload as String, success: { (response) in
                    UIAlertView(title: "LinkedIn", message: "Shared successfully", delegate: nil, cancelButtonTitle: "Ok").show()

                    }, error: { (error) in
                        UIAlertView(title: "Error", message: "Sharing failed", delegate: nil, cancelButtonTitle: "Ok").show()

                })
            }
            }) { (error) in
                UIAlertView(title: "Error", message: "Sharing failed", delegate: nil, cancelButtonTitle: "Ok").show()
        }
        
    }
    
    //MARK:- INSTAGRAM SHARE
    @IBAction func onInstagramShareButtonPressed (sender : UIButton){
        if MGInstagram.isAppInstalled() == true {
            instagram.postImage(UIImage(named: "ScreenShot"), withCaption: "Posted via pickitup", inView: self.view)
        }else{
            UIAlertView(title: "Error", message: "Please install instagram app", delegate: nil, cancelButtonTitle: "Ok").show()
        }
    }
    
    //MARK:- MAIL SHARE
    @IBAction func onMailShareButtonPressed (sender : UIButton){
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Try pickitup app")
       // mailComposer.setToRecipients(["bala@gmail.com"])
        mailComposer.addAttachmentData(UIImageJPEGRepresentation(UIImage(named: "ScreenShot")!, 0.7)!, mimeType: "image/jpeg", fileName: "pickitup")
        self.presentViewController(mailComposer, animated: true, completion: nil)
    }
    
    //MARK:- GOOGLE PLUS SHARE
    @IBAction func onGoogleShareButtonPressed (sender : UIButton){
        let signIn = GPPSignIn.sharedInstance();
        signIn.authenticate();

    }
    
    //MARK:- WHATSAPP SHARE
    @IBAction func onWhatsappShareButtonPressed (sender : UIButton){
        let urlStringEncoded = kShareURL.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
        let url  = NSURL(string: "whatsapp://send?text=\(urlStringEncoded!)")
        if UIApplication.sharedApplication().canOpenURL(url!) {
            UIApplication.sharedApplication().openURL(url!)
        }
    }
    
    //MARK:- SHOW ALERT
    func showAlert(message : String)  {
        UIAlertView(title: "Result", message: message, delegate: nil, cancelButtonTitle: "Ok").show()
    }
    
    func shareBuilder()  -> GPPShareBuilder{
        
        self.view.endEditing(true)
        let sharebuilder : GPPShareBuilder = GPPShare.sharedInstance().nativeShareDialog()
        sharebuilder.setURLToShare(NSURL(string: kShareURL))
        sharebuilder.setPrefillText("Try pickitup app")
        return sharebuilder

    }
}

//MARK:- TWITTER DELEGATE
extension ShareViewController : FHSTwitterEngineAccessTokenDelegate{
    
    func loadAccessToken() -> String! {
        return NSUserDefaults.standardUserDefaults().objectForKey("SavedAccessHTTPBody") == nil ? "" :  NSUserDefaults.standardUserDefaults().objectForKey("SavedAccessHTTPBody") as! String
    }
    func storeAccessToken(accessToken: String!) {
        NSUserDefaults.standardUserDefaults().setObject(accessToken, forKey: "SavedAccessHTTPBody")
    }
}

//MARK:- MAIL COMPOSER DELEGATE
extension ShareViewController : MFMailComposeViewControllerDelegate{
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        switch (result)
        {
        case MFMailComposeResultCancelled:
            self.dismissViewControllerAnimated(true, completion: nil)
            break;
            
        case MFMailComposeResultSaved:
            showAlert("Mail saved")
            break;
            
        case MFMailComposeResultSent:
            showAlert("Mail sent")
            break;
            
        case MFMailComposeResultFailed:
            showAlert("Mail sending failed")
            break;
            
        default:
            showAlert("Mail not sent")
            break;
        }

    }
}

extension ShareViewController : GPPSignInDelegate{
    
    func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
        let share : GPPShareBuilder = self.shareBuilder()
        if share.open() == false {
           SVProgressHUD.showErrorWithStatus("Error in sharing..")
        }
      
    }
}

extension ShareViewController : GPPShareDelegate{
    
    func finishedSharing(shared: Bool) {
        if shared == false {
            SVProgressHUD.showErrorWithStatus("Please try again")
        }
    }
//    func finishedSharingWithError(error: NSError!) {
//        print(error.localizedDescription)
//    }
    
}