//
//  InformationViewController.swift
//  Pickitup
//
//  Created by Bala on 09/03/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

protocol informationDelegate {
    func didFinishAddingInformation(infm : String)
    
}

class InformationViewController: UIViewController {

    @IBOutlet weak var textViewInformation : GCPlaceholderTextView!
    var delegate : informationDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewInformation.placeholder = "Enter your text here..."
        textViewInformation.placeholderColor = UIColor.blackColor()
        textViewInformation.textColor = UIColor.blackColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onBackButtonPressed (sender : UIButton){
        delegate.didFinishAddingInformation(textViewInformation.text)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
        
    }

}

//MARK:- TEXTVIEW DELEGATES
extension InformationViewController : UITextViewDelegate{
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        textViewInformation.textColor = UIColor.blackColor()
        if(text == "\n")
        {
            textView.resignFirstResponder()
        }
        
        return true
    }
    
}