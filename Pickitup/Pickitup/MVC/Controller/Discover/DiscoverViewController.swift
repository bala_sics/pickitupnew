//
//  DiscoverViewController.swift
//  Pickitup
//
//  Created by Bala on 05/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class DiscoverViewController: UIViewController {

    @IBOutlet var viewCart : UIView!
    @IBOutlet var tableViewItems : UITableView!
    @IBOutlet var labelCart : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DiscoverViewController.onCartViewTapped))
        tapGesture.numberOfTapsRequired = 1
        viewCart.addGestureRecognizer(tapGesture)
        
         labelCart.text = NSString(format: "%.2f$", Float(Cart.SharedInstance().totalPrice)!) as String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
        
    }

    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- CART ACTION
    func onCartViewTapped (){
        let cart : CartViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CartVC") as! CartViewController
        self.navigationController?.pushViewController(cart, animated: true)
    }
    
}
//MARK:- TABLE VIEW DELEGATES
extension DiscoverViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cart.SharedInstance().arrayCartItems.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 310
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        if(cell == nil){
            cell = DiscoverTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        (cell as! DiscoverTableViewCell).delegate = self
        let cart : Cart = Cart.SharedInstance().arrayCartItems[indexPath.row] as! Cart
        (cell as! DiscoverTableViewCell).labelItemType.text = "   \(cart.cartStoreName)".uppercaseString
        (cell as! DiscoverTableViewCell).imageViewItem.sd_setImageWithURL(NSURL(string: store_ImagePath + cart.cartStoreImage), placeholderImage:  UIImage(named: "dummy5"))
        (cell as! DiscoverTableViewCell).labelItemName.text = cart.cartItemType == "maindish" ? cart.cartItemName : cart.cartSubItemName
        (cell as! DiscoverTableViewCell).labelItemPrice.text = cart.cartItemType == "maindish" ? NSString(format: "%.2f $", Float(cart.cartItemPrice)!) as String :  NSString(format: "%.2f $", Float(cart.cartSubItemPrice)!) as String
        (cell as! DiscoverTableViewCell).cartObj = Cart.SharedInstance().arrayCartItems[indexPath.row] as! Cart
        let predicate = NSPredicate(format: "cartItemId == %@",cart.cartItemId)
        let array = Cart.SharedInstance().arrayCartItems.filteredArrayUsingPredicate(predicate)
        if(array.count > 0){
            let cart : Cart = array[0] as! Cart
            (cell as! DiscoverTableViewCell).labelQuantity.text = cart.cartItemType == "maindish" ? "x\(cart.cartItemQuantity)" : "x\(cart.cartSubItemQuantity)"
            (cell as! DiscoverTableViewCell).viewQuantiy.hidden = false
        }
        return cell!
    }
    
  }

extension DiscoverViewController : discoverCelldelegate{
    
    func updateCartAmount(amount: Float) {
        let total : Float = Float(Cart.SharedInstance().totalPrice)! + amount
        labelCart.text = NSString(format: "%.2f$", total) as String
    }
}
