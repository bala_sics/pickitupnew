//
//  ItemDetailsViewController.swift
//  Pickitup
//
//  Created by Bala on 02/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class ItemDetailsViewController: UIViewController {
    
    @IBOutlet var viewCart : UIView!
    @IBOutlet var tableViewItems : UITableView!
    @IBOutlet var imageViewStore : UIImageView!
    @IBOutlet var labelCart : UILabel!
    var imageViewHeader : UIImageView!
    var labelHeader : UILabel!
    var heightForHeader : CGFloat!
    var viewHeader : UIView!
    var buttonDiscover : UIButton!
    var buttonBack : UIButton!
    var arrayTableViewCells : NSMutableArray = NSMutableArray()
    var arrayHeights : NSMutableArray = NSMutableArray()
    
    var arrayMenu : NSMutableArray = NSMutableArray()
    var selectedStore : Stores!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ItemDetailsViewController.onCartViewTapped))
        tapGesture.numberOfTapsRequired = 1
        viewCart.addGestureRecognizer(tapGesture)
        heightForHeader = 55
        imageViewStore.sd_setImageWithURL(NSURL(string:store_ImagePath + selectedStore.storeImageName), placeholderImage: UIImage(named: "dummy5"))
      
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
        loadData()
        labelCart.text = NSString(format: "%.2f$", Float(Cart.SharedInstance().totalPrice)!) as String
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData (){
        arrayHeights.removeAllObjects()
        arrayTableViewCells.removeAllObjects()
        let cell1 : UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        cell1.selectionStyle = UITableViewCellSelectionStyle.None
        let viewType : UIView = UIView(frame: CGRectMake(0, 0, screenSize.width, 30))
        viewType.backgroundColor = UIColor.whiteColor()
        cell1.contentView.addSubview(viewType)
        
        let imageViewType : UIImageView = UIImageView(image: UIImage(named: "KosherSelected"))
        imageViewType.frame = CGRectMake(0, 0, 15, 15)
        viewType.addSubview(imageViewType)
        
        let labelType : UILabel = UILabel(frame: CGRectMake(18, 3, 200, 15))
        labelType.text = "Kosher"
        labelType.textColor = UIColor.blackColor()
        labelType.font = UIFont(name: "Myriad Hebrew", size: 12.0)
        labelType.textAlignment = NSTextAlignment.Center
        labelType.sizeToFit()
        viewType.addSubview(labelType)
        viewType.frame = CGRectMake((screenSize.width/2) - ((labelType.frame.width + 18)/2), 0, labelType.frame.width + 18, 15)
        viewType.hidden = true
        
        let labelDescription : UILabel = UILabel(frame: CGRectMake(5, 18, screenSize.width - 10, 15))
        labelDescription.text = selectedStore.storeDescription == "" ? "No description" : selectedStore.storeDescription
        labelDescription.textColor = UIColor.lightGrayColor()
        labelDescription.font = UIFont(name: "Myriad Hebrew", size: 13.0)
        labelDescription.textAlignment = NSTextAlignment.Center
        labelDescription.lineBreakMode = NSLineBreakMode.ByWordWrapping
        labelDescription.numberOfLines = 0
        labelDescription.sizeToFit()
        labelDescription.frame = CGRectMake(5, 5 , screenSize.width - 10, labelDescription.frame.height)
        cell1.contentView.addSubview(labelDescription)
        
        arrayHeights.addObject(labelDescription.frame.height + 30)
        arrayTableViewCells.addObject(cell1)
        
        fetchMenu()

    }
    
    func fetchMenu (){
        SVProgressHUD.showWithStatus("Processing...")
        Menu.fetchMenu(selectedStore.storeId) { (success, result, error) -> Void in
            if(success){
                self.arrayMenu.removeAllObjects()
                self.arrayMenu.addObjectsFromArray(result as! [AnyObject])
                self.createTableViewCells()
            }else{
                SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
            }
        }
    }
    
    //MARK:- BUTTON ACTIONS
    //MARK:- DISCOVER BUTTON ACTION
    func onDiscoverButtonPressed (){
        if(Cart.SharedInstance().arrayCartItems.count == 0){
            UIAlertView(title: "Warning", message: "Please add any item to cart", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
        let discover : DiscoverViewController = self.storyboard?.instantiateViewControllerWithIdentifier("DiscoverVC") as! DiscoverViewController
        self.navigationController?.pushViewController(discover, animated: true)
        }
    }
    
    //MARK:- CART ACTION
    func onCartViewTapped (){
        if(Cart.SharedInstance().arrayCartItems.count == 0){
            UIAlertView(title: "Warning", message: "Please add any item to cart", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
        let cart : CartViewController = self.storyboard?.instantiateViewControllerWithIdentifier("CartVC") as! CartViewController
        self.navigationController?.pushViewController(cart, animated: true)
        }
    }
    
    //MARK:- BACK BUTTON ACTION
    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func createTableViewCells (){
        arrayMenu.enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let menuObj : Menu = obj as! Menu
            self.createHeaderTableViewCells(menuObj.menuTitle)
            menuObj.menuItemDetails.enumerateObjectsUsingBlock({ (item, idx, stop) -> Void in
                let cell2 = ItemsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
                cell2.delegate = self
                cell2.itemObj = item as! Items
                cell2.storeId = self.selectedStore.storeId
                cell2.labelItemName.text = (item as! Items).itemName
                cell2.labelDescription.text = (item as! Items).itemDescription
                cell2.labelPrice.text = "\((item as! Items).itemRate)$"
                cell2.delegate = self
                let predicate = NSPredicate(format: "cartItemId == %@",(item as! Items).itemId)
                let array = Cart.SharedInstance().arrayCartItems.filteredArrayUsingPredicate(predicate)
                if(array.count > 0){
                    let cart : Cart = array[0] as! Cart
                    cell2.labelQuantity.text = "x\(cart.cartItemQuantity)"
                    cell2.viewQuantiy.hidden = false
                }
                self.arrayTableViewCells.addObject(cell2)
                self.arrayHeights.addObject(70)
            })
        }
        self.tableViewItems.reloadData()
        SVProgressHUD.dismiss()

    }
    
    func createHeaderTableViewCells (titleStr : String){
        let cell1 : UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        cell1.selectionStyle = UITableViewCellSelectionStyle.None
        let viewBorder : UIView = UIView(frame: CGRectMake(-10, 0, screenSize.width + 20, 40))
        viewBorder.backgroundColor = UIColor.whiteColor()
        viewBorder.layer.borderWidth = 0.5
        viewBorder.layer.borderColor = UIColor.lightGrayColor().CGColor
        cell1.contentView.addSubview(viewBorder)
        
        let labelTitle : UILabel = UILabel(frame: CGRectMake(20, 0, screenSize.width - 40,40))
        labelTitle.text = titleStr
        labelTitle.textColor = UIColor.blackColor()
        labelTitle.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        viewBorder.addSubview(labelTitle)
        arrayHeights.addObject(40)
        arrayTableViewCells.addObject(cell1)

    }

}

//MARK:- TABLE VIEW DELEGATES
extension ItemDetailsViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(viewHeader != nil) {return viewHeader}
        viewHeader = UIView(frame: CGRectMake(0, 0, screenSize.width, 55))
        viewHeader.backgroundColor = UIColor.whiteColor()
        
        imageViewHeader  = UIImageView(frame:CGRectMake(screenSize.width/2 - 25, -25, 50, 50))
       // imageViewHeader.frame = CGRectMake(screenSize.width/2 - 25, -25, 50, 50)
        imageViewHeader.sd_setImageWithURL(NSURL(string:store_ImagePath + selectedStore.storeLogo), placeholderImage: UIImage(named: "dummy5"))
        imageViewHeader.layer.cornerRadius = 25.0
        imageViewHeader.clipsToBounds = true
        imageViewHeader.autoresizingMask = [UIViewAutoresizing.FlexibleLeftMargin,UIViewAutoresizing.FlexibleRightMargin]
        viewHeader.addSubview(imageViewHeader)
        
        labelHeader = UILabel(frame: CGRectMake(screenSize.width/2 - 100, 25, 200, 30))
        labelHeader.text = selectedStore.storeName
        labelHeader.textColor = UIColor.blackColor()
        labelHeader.font = UIFont(name: "Myriad Hebrew", size: 15.0)
        labelHeader.textAlignment = NSTextAlignment.Center
        viewHeader.addSubview(labelHeader)
        
        buttonDiscover = UIButton(type: UIButtonType.Custom)
        buttonDiscover.frame = CGRectMake(screenSize.width - 70, 25, 50, 30)
        buttonDiscover.setTitle("Discover", forState: UIControlState.Normal)
        buttonDiscover.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        buttonDiscover.titleLabel?.font = UIFont(name: "Myriad Hebrew", size: 13.0)
        buttonDiscover.addTarget(self, action: #selector(ItemDetailsViewController.onDiscoverButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        viewHeader.addSubview(buttonDiscover)
        
        buttonBack = UIButton(type: UIButtonType.Custom)
        buttonBack.frame = CGRectMake(10, 25, 50, 30)
        buttonBack.setTitle("Back", forState: UIControlState.Normal)
        buttonBack.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        buttonBack.titleLabel?.font = UIFont(name: "Myriad Hebrew", size: 13.0)
        buttonBack.addTarget(self, action: #selector(ItemDetailsViewController.onBackButtonPressed(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        buttonBack.hidden = true
        viewHeader.addSubview(buttonBack)
        
        return viewHeader
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return heightForHeader
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTableViewCells.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return arrayHeights[indexPath.row] as! CGFloat
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        return arrayTableViewCells[indexPath.row] as! UITableViewCell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
         scrollView.contentOffset.y = max(0,scrollView.contentOffset.y)
            let y = min(20 ,(scrollView.contentOffset.y * 0.28125) - 25)
            let width = max(25, (50 - (scrollView.contentOffset.y * 25/160)))
            imageViewHeader.frame = CGRectMake(screenSize.width/2 - width/2, y, width, width)
            imageViewHeader.layer.cornerRadius = width/2
            labelHeader.frame = CGRectMake(screenSize.width/2 -  100, CGRectGetMaxY(imageViewHeader.frame) - 5, 200, 30)
            buttonDiscover.frame = CGRectMake(screenSize.width - 70, CGRectGetMaxY(imageViewHeader.frame) - 5, 50, 30)
            buttonBack.frame = CGRectMake(10, CGRectGetMaxY(imageViewHeader.frame) - 5, 50, 30)
            buttonBack.hidden = y != 20
            viewHeader.frame = CGRectMake(0, 0, screenSize.width, CGRectGetMaxY(labelHeader.frame))
            heightForHeader = CGRectGetMaxY(labelHeader.frame)
            tableViewItems.reloadData()
    }
    
}

extension ItemDetailsViewController : itemCelldelegate{
    func selectedRowatIndex(index: Int, itemObj: Items)
    {
        let personalized : PersonalizedViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PersonalizedVC") as! PersonalizedViewController
        personalized.itemId = itemObj.itemId
        self.navigationController?.pushViewController(personalized, animated: true)
    }
    
    func updateCartAmount(amount: Float) {
        
        let total : Float = Float(Cart.SharedInstance().totalPrice)! + amount
        labelCart.text = NSString(format: "%.2f$", total) as String
    }
}
