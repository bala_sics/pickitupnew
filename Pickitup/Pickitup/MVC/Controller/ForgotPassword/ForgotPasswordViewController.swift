//
//  ForgotPasswordViewController.swift
//  Pickitup
//
//  Created by Bala on 11/01/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var textFieldEmail : UITextField!
    @IBOutlet weak var labelBottom : UILabel!
    var hideLabelBottom : Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldEmail.setLeftPadding(10)
        let attrStr : NSMutableAttributedString = NSMutableAttributedString(string: "If you want to become a driver,please \nsubmit on the website. www.pickitup.com")
        attrStr.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 73.0/255.0, green: 187.0/255.0, blue: 191.0/255.0, alpha: 1.0), range: NSMakeRange(61, 17))
        labelBottom.attributedText = attrStr
        self.title = "Pickitup"
        self.navigationItem.hidesBackButton = true
        labelBottom.hidden = hideLabelBottom
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSendPasswordButtonPressed (sender : UIButton){
        if(textFieldEmail.text == ""){
            UIAlertView(title: "Warning", message: "Please enter your email", delegate: nil, cancelButtonTitle: "Ok").show()
        }else if(!IsValidEmail(textFieldEmail.text!)){
            UIAlertView(title: "Warning", message: "Please enter the valid email", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
            textFieldEmail.resignFirstResponder()
            SVProgressHUD.showWithStatus("Sending mail...")
            Person.sendRecoveryPassword(textFieldEmail.text!, type: Person.currentPerson().userId == nil ? "forgotpassword" : "driver_forgotpassword", completion: { (success, error) -> Void in
                if(success){
                    SVProgressHUD.showSuccessWithStatus("Email sent successfully")
                    self.navigationController?.popViewControllerAnimated(true)
                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            })
        }
    }

    //MARK:- EMAIL VALIDATION
    func IsValidEmail(email: NSString) -> Bool
    {
        let stricterFilterString:NSString = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailRegex:NSString = stricterFilterString
        let emailTest:NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluateWithObject(email)
    }

}

//MARK:- TEXTFIELD DELEGATE
extension ForgotPasswordViewController : UITextFieldDelegate{
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}