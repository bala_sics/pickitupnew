//
//  RegistrationTableViewController.swift
//  Pickitup
//
//  Created by Bala on 29/12/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class RegistrationTableViewController: UITableViewController {
    
    @IBOutlet weak var textFieldUserName : UITextField!
    @IBOutlet weak var textFieldPassword : UITextField!
    @IBOutlet weak var textFieldEmail : UITextField!
    @IBOutlet weak var buttonNext : UIButton!
    var activeTextfield : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageVw : UIImageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
        imageVw.image = UIImage(named: "Background")
        self.tableView.backgroundView = imageVw
        
        textFieldPassword.setLeftPadding(10)
        textFieldUserName.setLeftPadding(10)
        textFieldEmail.setLeftPadding(10)
        
        buttonNext.layer.borderColor = UIColor.whiteColor().CGColor
        buttonNext.layer.borderWidth = 1.0
        
        self.title = "Pickitup"
        self.navigationItem.hidesBackButton = true

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- CANCEL BUTTON ACTION
    @IBAction func onCancelButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK:- NEXT BUTTON ACTION
    @IBAction func onNextButtonPressed (sender : UIButton){
        if(textFieldEmail.text == "" || textFieldPassword.text == "" || textFieldUserName.text == ""){
            UIAlertView(title: "Warning", message: "Please fill all fields", delegate: nil, cancelButtonTitle: "Ok").show()
        }else if(!IsValidEmail(textFieldEmail.text!)){
             UIAlertView(title: "Warning", message: "Please enter the valid email", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
            if(activeTextfield != nil) { activeTextfield.resignFirstResponder()}
            SVProgressHUD.showWithStatus("Processing...")
            Person.registerWithUserDetails(textFieldEmail.text!, password: textFieldPassword.text!, username: textFieldUserName.text!, completion: { (success, result, error) -> Void in
                if(success){
                    SVProgressHUD.dismiss()
//                    let mobile : VerificationTableViewController = self.storyboard?.instantiateViewControllerWithIdentifier("VerificationVC") as! VerificationTableViewController
//                    self.navigationController?.pushViewController(mobile, animated: true)
                    AppDelegate.getAppdelegateInstance().setHomePage()

                }else{
                    SVProgressHUD.showErrorWithStatus(error?.localizedDescription)
                }
            })
        }
    }
    
    
    //MARK:- EMAIL VALIDATION
    func IsValidEmail(email: NSString) -> Bool
    {
        let stricterFilterString:NSString = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailRegex:NSString = stricterFilterString
        let emailTest:NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluateWithObject(email)
    }

}

//MARK:- TEXTFIELD DELEGATE
extension RegistrationTableViewController : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeTextfield = textField
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}