//
//  SideMenuTableViewController.swift
//  Pickitup
//
//  Created by Bala on 30/12/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import UIKit

class SideMenuTableViewController: UITableViewController {
    
    @IBOutlet var imageViewProfile : UIImageView!
    @IBOutlet var viewFooter : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue:0/255.0, alpha: 0.7)
        imageViewProfile.layer.cornerRadius = 25.0
        imageViewProfile.clipsToBounds = true
        if(IS_IPHONE6){
            viewFooter.frame = CGRectMake(viewFooter.frame.origin.x, viewFooter.frame.origin.y, viewFooter.frame.size.width, 99)
        }else if(IS_IPHONE6PLUS){
            viewFooter.frame = CGRectMake(viewFooter.frame.origin.x, viewFooter.frame.origin.y, viewFooter.frame.size.width, 168)
        }else{
            self.tableView.tableFooterView = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if(self.respondsToSelector(Selector("setSeparatorInset:"))){
            cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }
        if(cell.respondsToSelector(Selector("setLayoutMargins:"))){
            cell.layoutMargins =  UIEdgeInsetsMake(0, 0, 0, 0)
            ;
        }
        if(cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:"))){
            cell.preservesSuperviewLayoutMargins = false;
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let storybpard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        switch indexPath.row{
        case 1 :
            let follow : FollowMyDriverViewController = storybpard.instantiateViewControllerWithIdentifier("FollowMyDriverVC") as! FollowMyDriverViewController
            self.sideMenuController()?.setContentViewController(follow)
            break
        case 2 :
            let notify : NotificationTableViewController = storybpard.instantiateViewControllerWithIdentifier("NotificationsVC") as! NotificationTableViewController
            self.sideMenuController()?.setContentViewController(notify)
            break
        case 3 :
            let lastOrder : LastOrdersTableViewController = storybpard.instantiateViewControllerWithIdentifier("LastOrderVC") as! LastOrdersTableViewController
            self.sideMenuController()?.setContentViewController(lastOrder)
            break
        case 4 :
            let points : MyPointsViewController = storybpard.instantiateViewControllerWithIdentifier("MyPointsVC") as! MyPointsViewController
            self.sideMenuController()?.setContentViewController(points)
            break

        case 5 :
            let credit : CreditCardTableViewController = storybpard.instantiateViewControllerWithIdentifier("CreditCardVC") as! CreditCardTableViewController
            self.sideMenuController()?.setContentViewController(credit)
            break
        case 6 :
            let privacy : PrivacyPolicyViewController = storybpard.instantiateViewControllerWithIdentifier("PrivacyPolicyVC") as! PrivacyPolicyViewController
            privacy.navigationTitle = "About us"
            self.sideMenuController()?.setContentViewController(privacy)
            break
        case 7 :
            let driverLogin : DriverLoginViewController = storybpard.instantiateViewControllerWithIdentifier("DriverLoginVC") as! DriverLoginViewController
            self.sideMenuController()?.setContentViewController(driverLogin)
            break
        case 8 :
            let share : ShareViewController = storybpard.instantiateViewControllerWithIdentifier("ShareVC") as! ShareViewController
            self.sideMenuController()?.setContentViewController(share)
            break
        case 9 :
            let contact : ContactUsTableViewController = storybpard.instantiateViewControllerWithIdentifier("ContactUsVC") as! ContactUsTableViewController
            self.sideMenuController()?.setContentViewController(contact)
            break
        case 10 :
            let privacy : PrivacyPolicyViewController = storybpard.instantiateViewControllerWithIdentifier("PrivacyPolicyVC") as! PrivacyPolicyViewController
            privacy.navigationTitle = "Privacy Policy"
            self.sideMenuController()?.setContentViewController(privacy)
            break
        case 11 :
            let privacy : PrivacyPolicyViewController = storybpard.instantiateViewControllerWithIdentifier("PrivacyPolicyVC") as! PrivacyPolicyViewController
            privacy.navigationTitle = "Terms of use"
            self.sideMenuController()?.setContentViewController(privacy)
            break

        default :
            self.sideMenuController()?.sideMenu?.toggleMenu()
            break
        }
    }
}

