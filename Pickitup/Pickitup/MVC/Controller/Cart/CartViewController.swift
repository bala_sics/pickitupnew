//
//  CartViewController.swift
//  Pickitup
//
//  Created by Bala on 02/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit



class CartViewController: UIViewController {
    
    @IBOutlet var buttonPlus : UIButton!
    @IBOutlet var buttonMinus : UIButton!
    @IBOutlet var labelTip : UILabel!
    @IBOutlet var buttonPlusDate : UIButton!
    @IBOutlet var labelSubTotal : UILabel!
    @IBOutlet var labelShippingCost : UILabel!
    @IBOutlet var labelTotal : UILabel!
    @IBOutlet var labelDeliveryTime : UILabel!
    var deliveryDay : String!
    var deliveryTime : String!
    var informationAboutTheOrder : String = ""


    override func viewDidLoad() {
        super.viewDidLoad()    
        buttonPlus.layer.cornerRadius = 15.0
        buttonPlus.clipsToBounds = true
        buttonPlusDate.layer.cornerRadius = 15.0
        buttonPlusDate.clipsToBounds = true
        buttonMinus.layer.cornerRadius = 15.0
        buttonMinus.clipsToBounds = true
        labelSubTotal.text = NSString(format: "%.2f$", Float(Cart.SharedInstance().totalPrice)!) as String
        labelShippingCost.text = NSString(format: "%.2f$", Float(Cart.SharedInstance().shippingCost)!) as String
        labelTotal.text = NSString(format: "TOTAL: %.2f$", Float(Cart.SharedInstance().shippingCost)! + Float(Cart.SharedInstance().totalPrice)!) as String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
    }

    @IBAction func onBackButtonPressed (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onProceedToPaymentButtonPressed (sender : UIButton){
      if(labelDeliveryTime.text == ""){
            UIAlertView(title: "Warning", message: "Please select delivery time", delegate: nil, cancelButtonTitle: "Ok").show()
        }else{
        let payment : PaymentContainerViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PaymentContainerVC") as! PaymentContainerViewController
            payment.dictDetails = NSMutableDictionary()
            payment.dictDetails.setValue(Person.currentPerson().userId, forKey: "userid")
            payment.dictDetails.setValue(Cart.SharedInstance().arrayCartItems.valueForKey("cartId").componentsJoinedByString(","), forKey: "cartId")
            payment.dictDetails.setValue(labelShippingCost.text?.stringByReplacingOccurrencesOfString("$", withString: ""), forKey: "shippingcost")
            payment.dictDetails.setValue(labelTip.text?.stringByReplacingOccurrencesOfString("$", withString: ""), forKey: "tip")
            payment.dictDetails.setValue(Cart.SharedInstance().totalPrice, forKey: "cartamount")
            payment.dictDetails.setValue(deliveryDay, forKey: "deliveryDay")
            payment.dictDetails.setValue(deliveryTime, forKey: "deliveryTime")
          payment.infrmtionOrder = informationAboutTheOrder
        self.navigationController?.pushViewController(payment, animated: true)
        }
    }
    
    @IBAction func onInformationButtonPressed (sender : UIButton){
        let infm : InformationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("InformationVC") as! InformationViewController
        infm.delegate = self
        self.navigationController?.pushViewController(infm, animated: true)
    }
    
    @IBAction func onButtonPlusButtonPressed (sender : UIButton){
        var tip : CGFloat = CGFloat((labelTip.text!.stringByReplacingOccurrencesOfString("$", withString: "") as NSString).floatValue)
        tip = tip + 1.00
        labelTotal.text = NSString(format: "TOTAL: %.2f$",  Float(Cart.SharedInstance().shippingCost)! + Float(Cart.SharedInstance().totalPrice)! + Float(tip)) as String
        labelTip.text = NSString(format: "%.2f$", tip) as String
    }
    
    @IBAction func onMinusButtonPressed (Sender : UIButton){
        var tip : CGFloat = CGFloat((labelTip.text!.stringByReplacingOccurrencesOfString("$", withString: "") as NSString).floatValue)
        if(tip != 0){
        tip = tip - 1.00
        labelTotal.text = NSString(format: "TOTAL: %.2f$",  Float(Cart.SharedInstance().shippingCost)! + Float(Cart.SharedInstance().totalPrice)! + Float(tip)) as String
        labelTip.text = NSString(format: "%.2f$", tip) as String
        }
    }
    
    @IBAction func onDeliveryTimeButtonPressed (sender : UIButton){
        let delivery : DeliveryTimeViewController = self.storyboard?.instantiateViewControllerWithIdentifier("DeliveryVC") as! DeliveryTimeViewController
        delivery.delegate = self
        self.navigationController?.pushViewController(delivery, animated: true)
    }

    // MARK: - Table view data source
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cart.SharedInstance().arrayCartItems.count
    }
    
     func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cel")
        if(cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cel")
        }
        let dict : Cart = Cart.SharedInstance().arrayCartItems[indexPath.row] as! Cart
        let labelCount : UILabel = cell?.viewWithTag(1) as! UILabel
        labelCount.text = dict.cartItemType == "maindish" ? "x\(dict.cartItemQuantity)" : "x\(dict.cartSubItemQuantity)"
        let labelName : UILabel = cell?.viewWithTag(2) as! UILabel
        labelName.text = dict.cartItemType == "maindish" ? dict.cartItemName : dict.cartSubItemName
        let labelPrice : UILabel = cell?.viewWithTag(3) as! UILabel
        labelPrice.text = NSString(format: "%.2f$", Float(dict.cartItemTotalPrice)!) as String
        return cell!
    }

}

extension CartViewController : deliveryTimeDelegate{
    func didFinishSelectingDeliveryTime(delvryDay: String, delvryTime: String) {
        labelDeliveryTime.text = "\(delvryDay) \(delvryTime)"
        deliveryDay = delvryDay
        deliveryTime = delvryTime
    }
}

extension CartViewController : informationDelegate{
    func didFinishAddingInformation(infm: String) {
        informationAboutTheOrder = infm
    }
}