//
//  Notifications.swift
//  Pickitup
//
//  Created by Bala on 21/05/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class Notifications: NSObject {

    //MARK:- FETCH NOTIFICATIONS
    class func fetchAllNotifications(completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["userid" : "79"] //Person.currentPerson().userId]
        let url : String = base_Url + "all_notifications"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,result["details"] as! NSDictionary ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Notifications found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
        }) { (operation, error) -> Void in
            completion(false,nil,error)
        }
        
    }

}
