//
//  Driver.swift
//  Pickitup
//
//  Created by Bala on 19/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
var _driver : Driver?

class Driver: NSObject {
    var name : String!
     var lastName : String!
    var userId : String!
    var email : String!
    var password : String!

    
    //MARK:- STORE USERDETAILS
    class func initWithDictionary(dict : NSDictionary) -> AnyObject
    {
        _driver = Driver()
        _driver?.name = CheckForNull(strToCheck: dict.checkValueForKey(key: "name") as? String)
        _driver?.lastName = CheckForNull(strToCheck: dict.checkValueForKey(key: "last_name") as? String)
        _driver?.userId =  CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)
        _driver?.email =  CheckForNull(strToCheck: dict.checkValueForKey(key: "email") as? String)
        _driver?.password =  CheckForNull(strToCheck: dict.checkValueForKey(key: "password") as? String)
        return _driver!
    }
    
    //MARK:- CURRENT PERSON SINGLETON
    class func currentPerson() -> Driver
    {
        if (_driver == nil)
        {
            _driver = Driver()
        }
        return _driver!
    }
    
    //MARK:- LOGOUT
    class func driverLogout(){
        _driver = nil
    }
    
    class func userWithDetails (userDetails : NSDictionary) -> Driver{
        
        if (_driver == nil) {
            _driver = Driver.initWithDictionary(userDetails) as? Driver
        }
        return _driver!
    }
    
    //MARK:- LOGIN
    class func driverLoginWithUserDetails(username : String, password : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["username" : username , "password" : password, "device_token" : NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! String , "device_type":"iPhone"]
        let url : String = base_Url + "driver_login"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            _driver = nil
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Driver.userWithDetails(result["details"] as! NSDictionary),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Invalid User", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }


}
