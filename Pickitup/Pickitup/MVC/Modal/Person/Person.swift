//
//  Person.swift
//  LoginSwift
//
//  Created by Srishti Innovative on 11/01/15.
//  Copyright (c) 2015 BALA. All rights reserved.
//

import UIKit
var _person : Person?

class Person: NSObject {
    var username : String!
    var userId : String!
    var email : String!
    var password : String!
    var verificationStatus : String!
    var facebookId : String!
    
    //MARK:- STORE USERDETAILS
    class func initWithDictionary(dict : NSDictionary) -> AnyObject
    {
        _person = Person()
        _person?.username = CheckForNull(strToCheck: dict.checkValueForKey(key: "username") as? String)
        _person?.userId =  CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)
        _person?.email =  CheckForNull(strToCheck: dict.checkValueForKey(key: "email") as? String)
        _person?.password =  CheckForNull(strToCheck: dict.checkValueForKey(key: "password") as? String)
        _person?.verificationStatus =  CheckForNull(strToCheck: dict.checkValueForKey(key: "varification_status") as? String)
        _person?.facebookId =  CheckForNull(strToCheck: dict.checkValueForKey(key: "fb_id") as? String)
        return _person!
    }
    
    //MARK:- CURRENT PERSON SINGLETON
    class func currentPerson() -> Person
    {
        if (_person == nil)
        {
            _person = Person()
        }
        return _person!
    }
    
    //MARK:- LOGOUT
    class func logout(){
        _person = nil
    }
    
    class func userWithDetails (userDetails : NSDictionary) -> Person{
        
        if (_person == nil) {
            Person.saveLoggedUserdetails(userDetails)
            _person = Person.initWithDictionary(userDetails) as? Person
        }
        return _person!
    }
    
    //MARK:- LOGIN
    class func loginWithUserDetails(username : String, password : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["username" : username , "password" : password ,"device_token" : NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! String , "device_type":"iPhone"]
        let url : String = base_Url + "login"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            _person = nil
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Person.userWithDetails(result["details"] as! NSDictionary),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Invalid User", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
    
    //MARK:- FACEBOOK LOGIN
    class func facebookLoginWithUserDetails(username : String, email : String , fbId : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["user_name" : username , "email" : email , "fb_id" : fbId , "device_token" : NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! String , "device_type":"iPhone"]
        let url : String = base_Url + "facebook_login"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            _person = nil
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Person.userWithDetails(result["details"] as! NSDictionary),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Invalid User", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
    
    //MARK:- REGISTRATION
    class func registerWithUserDetails(email : String, password : String , username : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["username" : username , "password" : password ,"email" : email , "device_token" : NSUserDefaults.standardUserDefaults().objectForKey("DeviceToken") as! String , "device_type":"iPhone"]
        
        let url : String = base_Url + "registration"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            _person = nil
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Person.userWithDetails(result["details"] as! NSDictionary),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "User already exist", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
    
    //MARK:- FORGOT PASSWORD
    class func sendRecoveryPassword(email : String , type : String, completion : (Bool ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["email" : email]
        let url : String = base_Url + type
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,error)
        }
    }
    
    //MARK:- MAKE FILE NAME
    func makeFileName() -> NSString{
        let dateFormatter : NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyMMddHHmmssSSS"
        let dateString = dateFormatter.stringFromDate(NSDate())
        let randomValue  = arc4random() % 1000
        let fileName : NSString = "B\(dateString)\(randomValue).jpg"
        return fileName
    }
    
    //MARK:- UPDATE STORED USER DATA
    class func updateLoggedUser (userdetails : NSDictionary){
        saveLoggedUserdetails(userdetails)
        _person = Person.initWithDictionary(userdetails) as? Person
    }
    
    //MARK:- STORE USER DATA
    class func saveLoggedUserdetails(dictDetails : NSDictionary){
        let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(dictDetails)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: "LoggedUserDetails")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
}

