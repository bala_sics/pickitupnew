//
//  Orders.swift
//  Pickitup
//
//  Created by Bala on 22/03/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class Orders: NSObject {
    
    var orderAddress : String!
    var orderCity : String!
    var orderCountry : String!
    var orderDeliveryTime : String!
    var orderUserEmail : String!
    var orderLastName : String!
    var orderStoreLatitude : String!
    var orderStoreLongitude : String!
    var orderUserName : String!
    var orderTotalItems : String!
    var orderStoreName : String!
    var orderStoreLogoImage : String!
    var orderStoreImage : String!
    var orderDistance : String!
    var orderId : String!
    var orderStoreOwnerName : String!
    var orderStoreOwnerEmail : String!
    var orderedTime : String!
    var orderUserLatitude : String!
    var orderUserLongitude : String!
    var orderUserDistance : String!
    var orderDeliveredDate : String!
     var orderedDate : String!
    
    init(dict : NSDictionary) {
        super.init()
        self.orderDistance = CheckForNull(strToCheck: dict.checkValueForKey(key: "distance") as? String)
        self.orderId = CheckForNull(strToCheck: dict.checkValueForKey(key: "order_id") as? String)
        self.orderTotalItems = CheckForNull(strToCheck: dict.checkValueForKey(key: "no_of_items") as? String)
        self.orderDeliveryTime = CheckForNull(strToCheck: dict.checkValueForKey(key: "deliveredTime") as? String)
        self.orderCountry = CheckForNull(strToCheck: dict.checkValueForKey(key: "country") as? String)
        self.orderStoreLogoImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "logo") as? String)
        self.orderStoreImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
        self.orderLastName = CheckForNull(strToCheck: dict.checkValueForKey(key: "last_name") as? String)
        self.orderAddress = CheckForNull(strToCheck: dict.checkValueForKey(key: "address") as? String)
        self.orderStoreLatitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "latitude") as? String)
        self.orderStoreLongitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "longitude") as? String)
        self.orderUserName = CheckForNull(strToCheck: dict.checkValueForKey(key: "username") as? String)
        self.orderUserEmail = CheckForNull(strToCheck: dict.checkValueForKey(key: "useremail") as? String)
        self.orderStoreName = CheckForNull(strToCheck: dict.checkValueForKey(key: "shop_name") as? String)
        self.orderStoreOwnerName = CheckForNull(strToCheck: dict.checkValueForKey(key: "name") as? String)
        self.orderStoreOwnerEmail = CheckForNull(strToCheck: dict.checkValueForKey(key: "email") as? String)
        self.orderedTime = CheckForNull(strToCheck: dict.checkValueForKey(key: "orderedTime") as? String)
        self.orderUserLatitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "userlatitude") as? String)
        self.orderUserLongitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "userlongitude") as? String)
        self.orderUserDistance = CheckForNull(strToCheck: dict.checkValueForKey(key: "userdistance") as? String)
        self.orderDeliveredDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "deliveredDate") as? String)
        self.orderedDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "orderedDate") as? String)
    }
    
    //MARK:- VIEW ORDERS
    class func viewOrders(lat : String , lng : String , completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["driver_id" : Driver.currentPerson().userId , "latitude" : lat , "longitude" : lng]
        let url : String = base_Url + "payed_orders"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Orders.storeOrder(result["details"] as! NSArray),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No orders found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
    
    //MARK:- STORE ORDERS
    class func storeOrder(arrayOrder : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        arrayOrder.enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let order : Orders = Orders(dict: obj as! NSDictionary)
            array.addObject(order)
        }
        return array
    }

    //MARK:- ACCEPT ORDER
    class func acceptOrder(orderid : String , completion : (Bool ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["driver_id" : Driver.currentPerson().userId , "order_id" : orderid ]
        let url : String = base_Url + "accept_order_driver"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please complete the pending orders and try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,error)
        }
    }
    
    //MARK:- COMPLETE ORDER
    class func completeOrder(driverid : String ,orderid : String , completion : (Bool ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["driver_id" : driverid , "order_id" : orderid ]
        let url : String = base_Url + "complete_order_driver"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,error)
        }
    }
    
    //MARK:- MY LAST ORDERS
    class func myLastOrders(completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["user_id" : Person.currentPerson().userId]
        let url : String = base_Url + "mylast_order"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,result["details"] as! NSDictionary ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Orders found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
        }) { (operation, error) -> Void in
            completion(false,nil,error)
        }
        
    }
    
    //MARK:- DRIVER LAST ORDERS
    class func driverLastOrders(completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["driver_id" : Driver.currentPerson().userId]
        let url : String = base_Url + "driver_last_order_iphone"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,result["details"] as! NSDictionary ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Orders found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
        }) { (operation, error) -> Void in
            completion(false,nil,error)
        }
        
    }

}

class DriverDetails: NSObject {
    
    var driverid : String!
    var driverName : String!
    var driverLat : String!
    var driverLong : String!
    var driverStoreId : String!
    var driverStoreName : String!
    var driverDeliveredTime : String!
    var driverOrderId : String!
    
    init(dict : NSDictionary) {
        super.init()
        self.driverid = CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)
        self.driverName = CheckForNull(strToCheck: dict.checkValueForKey(key: "name") as? String)
        self.driverLat = CheckForNull(strToCheck: dict.checkValueForKey(key: "latitude") as? String)
        self.driverLong = CheckForNull(strToCheck: dict.checkValueForKey(key: "longitude") as? String)
        self.driverStoreId = CheckForNull(strToCheck: dict.checkValueForKey(key: "store_id") as? String)
        self.driverStoreName = CheckForNull(strToCheck: dict.checkValueForKey(key: "store_name") as? String)
        self.driverDeliveredTime = CheckForNull(strToCheck: dict.checkValueForKey(key: "delivered_time") as? String)
        self.driverOrderId = CheckForNull(strToCheck: dict.checkValueForKey(key: "orderid") as? String)
    }
    
    //MARK:- MY DRIVER DETAILS
    class func myDriverDetails(completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["user_id" : Person.currentPerson().userId]
        let url : String = base_Url + "driver_details"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true, DriverDetails.storeDriverDetails (result["details"] as! NSArray) ,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No drivers found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
        }) { (operation, error) -> Void in
            completion(false,nil,error)
        }
        
    }
    
    //MARK:- STORE ORDERS
    class func storeDriverDetails(arraydetails : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        arraydetails.enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let driver : DriverDetails = DriverDetails(dict: obj as! NSDictionary)
            array.addObject(driver)
        }
        return array
    }

}

class Bill : NSObject{
    
    var billItemName : String!
    var billItemPrice : String!
    var billQuantity : String!
    var billShippingCost : String!
    var billShopName : String!
    var billSubTotal : String!
    var billTip : String!
    
    init(dict : NSDictionary) {
      super.init()
        self.billItemName = CheckForNull(strToCheck: dict.checkValueForKey(key: "itemname") as? String)
        let price = dict["itemprice"] as! NSString
        self.billItemPrice = "\(price)"
        self.billQuantity = CheckForNull(strToCheck: dict.checkValueForKey(key: "quantity") as? String)
        self.billShippingCost = CheckForNull(strToCheck: dict.checkValueForKey(key: "shippingcost") as? String)
        self.billShopName = CheckForNull(strToCheck: dict.checkValueForKey(key: "shopname") as? String)
        let sub = dict["subtotal"] as! NSString
        self.billSubTotal = "\(sub)"
        self.billTip = CheckForNull(strToCheck: dict.checkValueForKey(key: "tip") as? String)
    }
    
    //MARK:- BILL DETAILS
    class func billDetails(orderid :String , userid : String , completion : (Bool ,AnyObject? ,String!,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["user_id" : userid , "orderid" : orderid]
        let url : String = base_Url + "bill_details"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Bill.storeBill(result["details"] as! NSArray),result["rating"] as! String ,nil) : completion(false,nil,"",NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No items found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
        }) { (operation, error) -> Void in
            completion(false,nil,"",error)
        }
        
    }
    
        //MARK:- STORE ORDERS
        class func storeBill(arraydetails : NSArray) -> NSArray
        {
            let array = NSMutableArray()
            arraydetails.enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
                let order : Bill = Bill(dict: obj as! NSDictionary)
                array.addObject(order)
            }
            return array
        }


    //MARK:- RATE DRIVER
    class func rateDriver(orderid :String ,rating : String, completion : (Bool ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["user_id" : Person.currentPerson().userId , "order_id" : orderid , "rating" : rating]
        let url : String = base_Url + "driver_rating"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
        }) { (operation, error) -> Void in
            completion(false,error)
        }
        
    }

}