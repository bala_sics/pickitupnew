//
//  CreditCard.swift
//  Pickitup
//
//  Created by Bala on 03/03/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class CreditCard: NSObject {
    
    var creditCardNumber : String!
    var creditCardExpMonth : String!
    var creditCardExpYear : String!
    var creditCardSecurityCode : String!
    var creditCardName : String!
    var creditCardLastName : String!
    var creditCardZipcode : String!
    var creditCardId : String!
    
    class func initWithDictionary (dict : NSDictionary) -> CreditCard{
        let card : CreditCard = CreditCard()
        card.creditCardNumber = CheckForNull(strToCheck: dict.checkValueForKey(key: "cardnum") as? String)
        card.creditCardExpMonth = CheckForNull(strToCheck: dict.checkValueForKey(key: "expmonth") as? String)
        card.creditCardExpYear = CheckForNull(strToCheck: dict.checkValueForKey(key: "expyear") as? String)
        card.creditCardSecurityCode = CheckForNull(strToCheck: dict.checkValueForKey(key: "securitycode") as? String)
        card.creditCardName = CheckForNull(strToCheck: dict.checkValueForKey(key: "name") as? String)
        card.creditCardLastName = CheckForNull(strToCheck: dict.checkValueForKey(key: "lastName") as? String)
        card.creditCardZipcode = CheckForNull(strToCheck: dict.checkValueForKey(key: "ZipCode") as? String)
        card.creditCardId = CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)

        return card
    }

    //MARK:- ADD CREDIT CART
    class func addCreditCard(number : String , expmonth : String , expyear : String , name : String , lastName : String , ZipcCode : String , securitycode : String , cardType : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["cardnum" : number , "userid" : Person.currentPerson().userId , "expmonth" : expmonth , "expyear" : expyear , "name" : name , "lastName" : lastName , "ZipCode" : ZipcCode , "securitycode" : securitycode , "cardtype" : cardType]
        let url : String = base_Url + "add_creditcard"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,result,nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: CheckForNull(strToCheck: result.checkValueForKey(key: "details") as? String), forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
    
    //MARK:- STORE DATA
    class func storeCard(arrayCard : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        arrayCard.enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let card : CreditCard = CreditCard.initWithDictionary(obj as! NSDictionary)
            array.addObject(card)
        }
        return array
    }
    
    //MARK:- VIEW CART
    class func fetchCards(completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["userid" : Person.currentPerson().userId]
        let url : String = base_Url + "view_creditcard"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,CreditCard.storeCard(result["details"] as! NSArray
                ),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No saved cards", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
    
    //MARK:- DELETE CART
    class func deleteCards(cardId : String , completion : (Bool,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["card_id" : cardId]
        let url : String = base_Url + "delete_creditcard"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,error)
        }
    }
}
