//
//  Cart.swift
//  Pickitup
//
//  Created by Bala on 01/03/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

var _cart : Cart?

class Cart: NSObject {
    
    var arrayCartItems : NSMutableArray = NSMutableArray()
    var totalPrice : String!
    var shippingCost : String!
    
    var cartStoreId : String!
    var cartStoreName : String!
    var cartStoreImage : String!
    var cartStoreDescription : String!
    var cartItemId : String!
    var cartItemName : String!
    var cartItemDescription : String!
    var cartItemPrice : String!
    var cartItemQuantity : String!
    var cartItemTotalPrice : String!
    var cartId : String!
    
    var cartSubItemId : String!
    var cartSubItemName : String!
    var cartSubItemQuantity : String!
    var cartSubItemPrice : String!
    var cartItemType : String!
    
    class func initWithDictionary (dict : NSDictionary) -> Cart{
        let cart : Cart = Cart()
        cart.cartStoreId = CheckForNull(strToCheck: dict.checkValueForKey(key: "storeid") as? String)
        cart.cartStoreName = CheckForNull(strToCheck: dict.checkValueForKey(key: "storename") as? String)
        cart.cartStoreImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "storeimage") as? String)
        cart.cartStoreDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "storedescription") as? String)
        cart.cartItemId = CheckForNull(strToCheck: dict.checkValueForKey(key: "item_id") as? String)
        cart.cartItemName = CheckForNull(strToCheck: dict.checkValueForKey(key: "item_name") as? String)
        cart.cartItemDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "item_description") as? String)
        cart.cartItemPrice = CheckForNull(strToCheck: dict.checkValueForKey(key: "item_price") as? String)
        cart.cartItemQuantity = CheckForNull(strToCheck: dict.checkValueForKey(key: "item_quantity") as? String)
        let price = dict["totalprice"]!
        cart.cartItemTotalPrice = "\(price)"
        
        cart.cartId = CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)
        
        cart.cartSubItemId = CheckForNull(strToCheck: dict.checkValueForKey(key: "subdish_id") as? String)
        cart.cartSubItemName = CheckForNull(strToCheck: dict.checkValueForKey(key: "subdish_name") as? String)
        cart.cartSubItemQuantity = CheckForNull(strToCheck: dict.checkValueForKey(key: "subdish_quantity") as? String)
        cart.cartSubItemPrice = CheckForNull(strToCheck: dict.checkValueForKey(key: "subdish_price") as? String)
        cart.cartItemType = CheckForNull(strToCheck: dict.checkValueForKey(key: "status") as? String)

        return cart
    }

    //MARK:- SINGLETON FOR CART
    class func SharedInstance() -> Cart
    {
        if (_cart == nil)
        {
            _cart = Cart()
        }
        return _cart!
    }
    
    //MARK:- GETTING CURRENT STOREID
   class func getStoreId () -> String{
        var storeId : String = ""
        if(Cart.SharedInstance().arrayCartItems.count > 0){
            let cart : Cart = Cart.SharedInstance().arrayCartItems[0] as! Cart
            storeId = cart.cartStoreId
        }
        return storeId
    }


    //MARK:- ADD TO CART
    class func addToCart(storeId : String , itemId : String , quantity : String , subItemId : String , subItemQuantity : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["store_id" : storeId , "user_id" : Person.currentPerson().userId , "item_id" : itemId , "quantity" : quantity , "subdish_cat_id" : subItemId , "subdish_quantity" : subItemQuantity]
        let url : String = base_Url + "add_to_cart"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Cart.storeData(result),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
    
    class func storeData (data : NSDictionary) -> NSArray{
        Cart.SharedInstance().arrayCartItems.removeAllObjects()
        (data["details"] as! NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let cart : Cart = Cart.initWithDictionary(obj as! NSDictionary)
            Cart.SharedInstance().arrayCartItems.addObject(cart)
        }
        Cart.SharedInstance().totalPrice = data["total"] as! String
        return Cart.SharedInstance().arrayCartItems
    }
    
    //MARK:- VIEW CART
    class func viewCart(completion : (Bool ,AnyObject? ,NSError?) -> Void){
        Cart.SharedInstance().totalPrice = "0"
        Cart.SharedInstance().shippingCost = "0"
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["user_id" : Person.currentPerson().userId]
        let url : String = base_Url + "view_cart"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Cart.storeData(result
                ),nil) : completion(false,nil,nil)
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
    
    //MARK:- FETCH AVAILABLE TIME
    class func fetchAvailableTime(storeId : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["storeid_id" : storeId]
        let url : String = base_Url + "storetime"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,result["details"],nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }

    //MARK:- CHECK AVAILABILITY
    class func CheckAvailability(storeId : String , day : String , time : String, completion : (Bool ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["storeid_id" : storeId , "day" : day , "store_time" : time ]
        let url : String = base_Url + "store_available"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Delivery time not available", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,error)
        }
    }


    //MARK:- FETCH AVAILABLE TIME
    class func proceedToPayment(params : NSDictionary, completion : (Bool ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let url : String = base_Url + "payment_details"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,nil) : completion(false,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "Please try again", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,error)
        }
    }

}
