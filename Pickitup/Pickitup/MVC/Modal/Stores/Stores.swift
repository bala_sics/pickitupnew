//
//  Stores.swift
//  Pickitup
//
//  Created by Bala on 20/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class Stores: NSObject {
    var storeEmail : String!
    var storeId : String!
    var storeImageName : String!
    var storeFirstName : String!
    var storeLastName : String!
    var storeLatitude : String!
    var storeLongitude : String!
    var storeName : String!
    var storeLogo : String!
    var type : String!
    var storeDescription : String!
    var storeDistance : String!
    
    class func initWithDictionary (dict : NSDictionary) -> Stores{
        let store : Stores = Stores()
        store.storeEmail = CheckForNull(strToCheck: dict.checkValueForKey(key: "email") as? String)
        store.storeId = CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)
        store.storeImageName = CheckForNull(strToCheck: dict.checkValueForKey(key: "store_image") as? String)
        store.storeFirstName = CheckForNull(strToCheck: dict.checkValueForKey(key: "name") as? String)
        store.storeLastName = CheckForNull(strToCheck: dict.checkValueForKey(key: "last_name") as? String)
        store.storeLatitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "lat") as? String)
        store.storeLongitude = CheckForNull(strToCheck: dict.checkValueForKey(key: "longi") as? String)
        store.storeName = CheckForNull(strToCheck: dict.checkValueForKey(key: "shop_name") as? String)
        store.storeLogo = CheckForNull(strToCheck: dict.checkValueForKey(key: "logo") as? String)
        store.type = CheckForNull(strToCheck: dict.checkValueForKey(key: "type") as? String)
        store.storeDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "shop_description") as? String)
        store.storeDistance = CheckForNull(strToCheck: dict.checkValueForKey(key: "distance") as? String)
        return store
    }

    //MARK:- FETCH NEARBY STORES
    class func fetchNearbyStores(lat : String, long : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["user_id" : Person.currentPerson().userId , "lat" : lat , "long" : long]
        let url : String = base_Url + "stores"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Stores.storeNearByStores(result["details"] as! NSArray),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No stores", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }

    //MARK:- STORE DATA
    class func storeNearByStores(arrayPost : NSArray) -> NSArray
    {
        let array = NSMutableArray()
        arrayPost.enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let store : Stores = Stores.initWithDictionary(obj as! NSDictionary)
            array.addObject(store)
        }
        return array
    }

    //MARK:- FETCH NEARBY STORES
    class func searchStore(storeName : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["shop_name" : storeName]
        let url : String = base_Url + "searchstores"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Stores.storeNearByStores(result["details"] as! NSArray),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No stores", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }

    //MARK:- FETCH ALL STORES
    class func fetchAllStores(category : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["category" : category , "user_id" : Person.currentPerson().userId]
        let url : String = base_Url + "allstores"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            Cart.SharedInstance().totalPrice = "0"
            Cart.SharedInstance().shippingCost = "0"
            Cart.SharedInstance().arrayCartItems.removeAllObjects()
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Stores.storeNearByStores(result["details"] as! NSArray),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No stores", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
    
    //MARK:- FETCH ALL RESTAURANTS
    class func fetchAllRestaurants(category : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["category" : category , "user_id" : Person.currentPerson().userId]
        let url : String = base_Url + "allrestaurant"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            Cart.SharedInstance().totalPrice = "0"
            Cart.SharedInstance().shippingCost = "0"
            Cart.SharedInstance().arrayCartItems.removeAllObjects()
            let result : NSDictionary = responseObject as! NSDictionary
            (result["result"] as! String == "success") ? completion(true,Stores.storeNearByStores(result["details"] as! NSArray),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No Restaurants", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
                completion(false,nil,error)
        }
    }
}
