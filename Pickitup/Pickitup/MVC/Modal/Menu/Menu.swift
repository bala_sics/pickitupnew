//
//  Menu.swift
//  Pickitup
//
//  Created by Bala on 26/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit

class Menu: NSObject {

    var menuTitle : String!
    var menuItemDetails : NSMutableArray = NSMutableArray()
    
    //MARK:- FETCH ALL ITEMS
    class func fetchMenu(storeId : String, completion : (Bool ,AnyObject? ,NSError?) -> Void){
        let manager  = AFHTTPRequestOperationManager()
        manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
        let params : NSDictionary = ["store_id" : storeId , "user_id" : Person.currentPerson().userId]
        let url : String = base_Url + "menudetailsall"
        manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
            let result : NSDictionary = responseObject as! NSDictionary
          (result["result"] as! String == "success") ? completion(true,Menu.storeMenuItems(result["details"] as! NSDictionary),nil) : completion(false,nil,NSError(domain: "", code: 1, userInfo: NSDictionary(object: "No items found", forKey: NSLocalizedDescriptionKey) as [NSObject : AnyObject]))
            }) { (operation, error) -> Void in
               completion(false,nil,error)
        }
    }
    
    class func storeMenuItems (details : NSDictionary) -> NSArray{
        let array = NSMutableArray()
        (details.allKeys as NSArray).enumerateObjectsUsingBlock { (obj, idx, stop) -> Void in
            let menu : Menu = Menu()
            menu.menuTitle = obj as! String
            for dict in (details[obj as! String] as! NSArray){
                let item = Items.initWithDictionary(dict as! NSDictionary)
                menu.menuItemDetails.addObject(item)
            }
            array.addObject(menu)
        }
            return array
    }
}

class Items: NSObject {
    
    var itemDescription : String!
    var itemImage : String!
    var itemName : String!
    var itemRate : String!
    var itemId : String!
    
    class func initWithDictionary (dict : NSDictionary) -> Items{
        let item : Items = Items()
        item.itemDescription = CheckForNull(strToCheck: dict.checkValueForKey(key: "description") as? String)
        item.itemImage = CheckForNull(strToCheck: dict.checkValueForKey(key: "image") as? String)
        item.itemName = CheckForNull(strToCheck: dict.checkValueForKey(key: "name") as? String)
        item.itemRate = CheckForNull(strToCheck: dict.checkValueForKey(key: "rate") as? String)
        item.itemId = CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)

        return item
    }

    
}