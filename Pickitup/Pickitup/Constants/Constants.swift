//
//  Constants.swift
//  Pickitup
//
//  Created by Bala on 29/12/2015.
//  Copyright © 2015 Bala. All rights reserved.
//

import Foundation
import UIKit

//SCREEN SIZE
let screenSize = UIScreen.mainScreen().bounds

//IOS VERSION
let Device = UIDevice.currentDevice()
private let iosVersion = NSString(string: Device.systemVersion).doubleValue
let IS_iOS8 : Bool = iosVersion >= 8

let IS_iOS7 : Bool = iosVersion >= 7 && iosVersion < 8

//CHECK USER DEVICE
let IS_IPHONE4 : Bool = screenSize.height == 480
let IS_IPHONE5 : Bool = screenSize.height == 568
let IS_IPHONE6 : Bool = screenSize.height == 667
let IS_IPHONE6PLUS : Bool = screenSize.height == 736

//CHECK NIL VALUE
func CheckForNull(strToCheck strToCheck : String?) -> String{
    if let strText = strToCheck {
        return strText
    }
    return "nil"
}


//BASEURL
let base_Url = "http://sicsglobal.com/WEBTEAM/WebT1/pickitup/index.php/json/"
let store_ImagePath = "http://sicsglobal.com/WEBTEAM/WebT1/pickitup/assets/uploads/"