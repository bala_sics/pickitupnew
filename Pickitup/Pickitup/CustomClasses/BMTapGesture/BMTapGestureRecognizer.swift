//
//  BMTapGestureRecognizer.swift
//  Pickitup
//
//  Created by Bala on 08/02/2016.
//  Copyright © 2016 Bala. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass

class BMTapGestureRecognizer: UIGestureRecognizer {

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent) {
        if (self.state == UIGestureRecognizerState.Possible) {
            self.state = UIGestureRecognizerState.Recognized;
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent) {
        self.state = UIGestureRecognizerState.Failed;
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent) {
         self.state = UIGestureRecognizerState.Failed;
    }
}
