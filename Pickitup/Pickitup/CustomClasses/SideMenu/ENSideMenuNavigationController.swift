//
//  RootNavigationViewController.swift
//  SwiftSideMenu
//
//  Created by Evgeny Nazarov on 29.09.14.
//  Copyright (c) 2014 Evgeny Nazarov. All rights reserved.
//

import UIKit
import CoreLocation

public class ENSideMenuNavigationController: UINavigationController, ENSideMenuProtocol,UINavigationControllerDelegate {
    
    public var sideMenu : ENSideMenu?
    public var sideMenuAnimationType : ENSideMenuAnimation = .Default
    var locationManager : CLLocationManager!
    
    
    // MARK: - Life cycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        locationManager = locationManager == nil ? CLLocationManager() : locationManager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    public init( menuViewController: UIViewController, contentViewController: UIViewController?) {
        super.init(nibName: nil, bundle: nil)
        
        if (contentViewController != nil) {
            self.viewControllers = [contentViewController!]
        }
        
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: menuViewController, menuPosition:.Left)
        view.bringSubviewToFront(navigationBar)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        if(viewController is ShareViewController == false && viewController is ContactUsTableViewController == false && viewController is MyWalletViewController == false && viewController is NotificationTableViewController == false && viewController is PrivacyPolicyViewController == false && viewController is MyPointsViewController == false && viewController is BillTableViewController == false){
            viewController.title = "Pickitup"
        }
        viewController.navigationItem.hidesBackButton = true
        if(viewController is HomeViewController == false && viewController is ShareViewController == false && viewController is ForgotPasswordViewController == false && viewController is DriverHomeViewController == false ){
            let backButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "BackButton"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(ENSideMenuNavigationController.onBackButtonPressed))
            viewController.navigationItem.leftBarButtonItem = backButton
        }
    }
    
    func onBackButtonPressed (){
        self.popViewControllerAnimated(true)
    }
    
    // MARK: - Navigation
    public func setContentViewController(contentViewController: UIViewController) {
        self.sideMenu?.toggleMenu()
        switch sideMenuAnimationType {
        case .None:
            self.viewControllers = [contentViewController]
            break
        default:
            //            contentViewController.navigationItem.hidesBackButton = true
            //            self.setViewControllers([contentViewController], animated: true)
            gotoDestination(contentViewController)
            break
        }
    }
    
    func gotoDestination(destViewController:UIViewController){
        let currentViewController: UIViewController = AppDelegate.getAppdelegateInstance().mainNavigation.topViewController!
        if(String.fromCString(class_getName(destViewController.dynamicType)) == String.fromCString(class_getName(currentViewController.dynamicType))!){
            
        }else{
            let controllers : NSMutableArray = NSMutableArray(array: AppDelegate.getAppdelegateInstance().mainNavigation.viewControllers)
            controllers.addObject(destViewController)
            self.setViewControllers(controllers as! [UIViewController], animated: true)
        }
        
    }
    
    
}
//MARK:- LOCATION MANAGER DELEGATES
extension ENSideMenuNavigationController : CLLocationManagerDelegate{
    public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let loc : CLLocation = (locations as NSArray).firstObject as! CLLocation
        if(Person.currentPerson().userId != nil){
            let manager  = AFHTTPRequestOperationManager()
            manager.responseSerializer.acceptableContentTypes = NSSet(objects: "text/html","application/json") as Set<NSObject>
            let params : NSDictionary = ["user_id" : Driver.currentPerson().userId == nil ? Person.currentPerson().userId :  Driver.currentPerson().userId, "lat" : "\(loc.coordinate.latitude)" , "longi" : "\(loc.coordinate.longitude)"]
            let url : String = Driver.currentPerson().userId == nil ? base_Url +  "user_location_update" : base_Url +  "driver_lat_long"
            manager.POST(url, parameters: params as AnyObject!, success: { (operation, responseObject) -> Void in
                // let result : NSDictionary = responseObject as! NSDictionary
                }) { (operation, error) -> Void in
            }
            
        }
    }
}
